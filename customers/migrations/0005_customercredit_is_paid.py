# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2018-09-13 09:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0004_auto_20180913_1333'),
    ]

    operations = [
        migrations.AddField(
            model_name='customercredit',
            name='is_paid',
            field=models.BooleanField(default=False),
        ),
    ]
