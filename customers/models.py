from __future__ import unicode_literals
from django.db import models
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from main.models import BaseModel
from main.models import STATE
from versatileimagefield.fields import VersatileImageField
 

DOC_CATEGORY = (
    ('license', 'Drivers License'),
    ('voters_card', 'Voters Card'),
    ('passport', 'Passport'),
    ('ration_card', 'Ration Card'),
)

class Customer(BaseModel):
    shop = models.ForeignKey("main.Shop")
    name = models.CharField(max_length=128)
    age = models.CharField(max_length=128)
    address = models.TextField()
    phone = models.CharField(max_length=128,null=True,blank=True)
    email = models.EmailField(null=True,blank=True)
    state = models.CharField(max_length=128,choices=STATE,default="Kerala")    
    
    first_time_credit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    first_time_debit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    credit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    debit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    document_category = models.CharField(max_length=128, choices=DOC_CATEGORY,default="voters_card")
    document_no = models.CharField(max_length=128,null=True,blank=True)
    photo = VersatileImageField('Photo',upload_to="customer/photo/",blank=True,null=True)
    document = VersatileImageField(blank=True,null=True)
    signature = VersatileImageField(blank=True,null=True)
    
    is_deleted = models.BooleanField(default=False)
    is_system_generated = models.BooleanField(default=False)
     
    class Meta:
        db_table = 'customers_customer'
        verbose_name = _('customer')
        verbose_name_plural = _('customers')
        ordering = ('auto_id',)        
        unique_together = (("shop","name","address"),)  
  
    def __unicode__(self): 
        return "%s - %s" %(self.name,self.address)


class CustomerCredit(models.Model):
    shop = models.ForeignKey("main.Shop")
    sale = models.ForeignKey("sales.Sale",limit_choices_to={'is_deleted': False},blank=True,null=True)
    checkin = models.ForeignKey("bookings.Checkin",limit_choices_to={'is_deleted': False},blank=True,null=True)
    customer = models.ForeignKey("customers.Customer",limit_choices_to={'is_deleted': False},blank=True,null=True)
    amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    is_paid = models.BooleanField(default=False)

    class meta:
        db_table = 'customers_customer_credit'
        verbose_name = _('customer credit')
        verbose_name_plural = _('customer credits')

    def __unicode__(self):
        return self.amount