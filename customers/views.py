from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from customers.models import Customer,CustomerCredit
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, shop_required, check_account_balance,permissions_required,ajax_required
from customers.forms import CustomerForm
from main.functions import generate_form_errors, get_auto_id, get_a_id
import datetime
from django.db.models import Q
from dal import autocomplete
from sales.models import Sale
from customers.functions import update_customer_credit_debit,\
    get_customer_credit, get_customer_debit
from users.functions import get_current_shop
from django.db.models import Sum


class CustomerAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)

        items = Customer.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) | 
                                 Q(name__istartswith=self.q) | 
                                 Q(phone__istartswith=self.q) | 
                                 Q(email__istartswith=self.q) | 
                                 Q(address__istartswith=self.q)
                                )
    
        return items
    
    
@check_mode
@login_required
@shop_required
def dashboard(request):
    return HttpResponseRedirect(reverse('customers:customers'))


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_customer'])
def create(request):    
    current_shop = get_current_shop(request)
    if request.method == 'POST':
        form = CustomerForm(request.POST,request.FILES)
        
        if form.is_valid(): 
            
            auto_id = get_auto_id(Customer)
            a_id = get_a_id(Customer,request)
            
            #create customer
            
            first_time_credit = form.cleaned_data['first_time_credit']
            first_time_debit = form.cleaned_data['first_time_debit']
            data = form.save(commit=False)
            data.a_id = a_id
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.shop = current_shop
            data.save()    
            
            #update credit debit
            update_customer_credit_debit(data.pk,"credit",first_time_credit)
            update_customer_credit_debit(data.pk,"debit",first_time_debit)

            return HttpResponseRedirect(reverse('customers:customer',kwargs={'pk':data.pk}))             
        
        else:            
            form = CustomerForm(initial={'shop':current_shop})
            context = {
                "title" : "Create Customer ",
                "form" : form,
                "url" : reverse('customers:create'),

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,

            }
            return render(request,'customers/entry.html',context)
    
    else:
        form = CustomerForm(initial={'shop':current_shop})
        context = {
            "title" : "Create Customer ",
            "form" : form,
            "url" : reverse('customers:create'),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,

        }
        return render(request,'customers/entry.html',context)


@check_mode
@login_required
@shop_required 
@permissions_required(['can_view_customer'])
def customers(request):
    current_shop = get_current_shop(request)
    instances = Customer.objects.filter(is_deleted=False,is_system_generated=False,shop=current_shop)
    title = "Customers"
    
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(name__icontains=query) | Q(email__icontains=query) | Q(phone__icontains=query) | Q(address__icontains=query))
        title = "Customers - %s" %query    
        
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'customers/customers.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_customer'])
def customer(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Customer.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    context = {
        "instance" : instance,
        "title" : "Customer : " + instance.name,
        "single_page" : True,
        "sales" : Sale.objects.filter(customer=instance,is_deleted=False,shop=current_shop),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'customers/customer.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_customer'])
def edit(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Customer.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    old_first_time_credit = instance.first_time_credit
    if request.method == 'POST':
            
        response_data = {}
        form = CustomerForm(request.POST,request.FILES,instance=instance)
        
        if form.is_valid():  
            
            customer = Customer.objects.get(pk=pk)
            previous_first_time_credit = customer.first_time_credit
            previous_first_time_debit = customer.first_time_debit  
                   
            #update customer
            first_time_credit = form.cleaned_data['first_time_credit']
            first_time_debit = form.cleaned_data['first_time_debit']
            
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()    
            
            #restore previos credit debit
            update_customer_credit_debit(pk,"debit",previous_first_time_credit)            
            update_customer_credit_debit(pk,"credit",previous_first_time_debit) 
            
            #update credit debit
            update_customer_credit_debit(pk,"credit",first_time_credit)
            update_customer_credit_debit(pk,"debit",first_time_debit) 

            return HttpResponseRedirect(reverse('customers:customer',kwargs={'pk':data.pk}))              
            
        else:
            form = CustomerForm(instance=instance,initial={'shop':current_shop})        
            context = {
                "form" : form,
                "title" : "Edit Customer : " + instance.name,
                "instance" : instance,
                "url" : reverse('customers:edit',kwargs={'pk':instance.pk}),
                "redirect" : True,

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "purchases" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
            }
            return render(request, 'customers/entry.html', context)

    else: 

        form = CustomerForm(instance=instance,initial={'shop':current_shop})
        
        context = {
            "form" : form,
            "title" : "Edit Customer : " + instance.name,
            "instance" : instance,
            "url" : reverse('customers:edit',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "purchases" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'customers/entry.html', context)

@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_customer'])
def delete(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Customer.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    Customer.objects.filter(pk=pk,shop=current_shop).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Customer Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('customers:customers')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_customer'])
def delete_selected_customers(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Customer.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
            Customer.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Customer(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('customers:customers')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
def get_credit(request):
    pk = request.GET.get('id')
    current_shop = get_current_shop(request)
    instance = Customer.objects.get(pk=pk,is_deleted=False)
    other_expense = 0
    if CustomerCredit.objects.filter(customer=instance,sale__isnull=False,is_paid=False).exists():
        other_expense = CustomerCredit.objects.filter(customer=instance,sale__isnull=False,is_paid=False).aggregate(amount=Sum('amount')).get('amount',0)
    print other_expense
    if instance:
        if instance.is_system_generated == True:
            response_data = {
                "status" : "true",
                'credit' : float(0.00), 
                'debit' : float(0.00), 
                'other_balance' : float(0.00)
            }
        else:
            response_data = {
                "status" : "true",
                'credit' : float(instance.credit), 
                'debit' : float(instance.debit),
                'other_balance' : float(other_expense) 
            }
    else:
        response_data = {
            "status" : "false",
            "message" : "Credit Error"
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
def check_document(request):
    pk = request.GET.get('id')
    current_shop = get_current_shop(request)
    instance = Customer.objects.get(pk=pk,is_deleted=False)
    if instance:
        if instance.document_no and instance.photo and instance.document :
            response_data = {
                "status" : "true",  
                "is_comleted" : "true"              
            }
        else:
            response_data = {
                "status" : "true",
                "is_comleted" : "false" 
            }
    else:
        response_data = {
            "status" : "false",
            "message" : "Customer Error"
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')  



