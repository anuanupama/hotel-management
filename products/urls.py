from django.conf.urls import url, include
from django.contrib import admin
import views
from products.views import CategoryAutocomplete, ProductAutocomplete,BrandAutocomplete,SubcategoryAutocomplete,AssetAutocomplete


urlpatterns = [
    url(r'^subcategory-autocomplete/$',SubcategoryAutocomplete.as_view(create_field='name'),name='subcategory_autocomplete'),
    url(r'^category-autocomplete/$',CategoryAutocomplete.as_view(create_field='name'),name='category_autocomplete'),
    url(r'^brand-autocomplete/$',BrandAutocomplete.as_view(create_field='name'),name='brand_autocomplete'),
    url(r'^product-autocomplete/$',ProductAutocomplete.as_view(),name='product_autocomplete'),
    url(r'^asset-autocomplete/$',AssetAutocomplete.as_view(create_field='name'),name='asset_autocomplete'),
    
    url(r'^$', views.dashboard,name='dashboard'),

    url(r'^get-asset/$',views.get_asset,name='get_asset'), 
    
    url(r'^product/create/$',views.create,name='create'),
    url(r'^product/create-new-product/$',views.create_new_product,name='create_new_product'),
    url(r'^product/views/$',views.products,name='products'),
    url(r'^product/edit/(?P<pk>.*)/$',views.edit,name='edit'),
    url(r'^product/view/(?P<pk>.*)/$',views.product,name='product'),
    url(r'^product/delete/(?P<pk>.*)/$',views.delete,name='delete'),
    url(r'^product/delete-selected/$', views.delete_selected_products, name='delete_selected_products'),

    url(r'^get-product/$',views.get_product,name='get_product'),
    url(r'^get-product-sub-categories/$', views.get_product_sub_categories, name='get_product_sub_categories'),
    url(r'^get-product-units/$', views.get_product_units, name='get_product_units'),
    url(r'^get-returnable-product/$',views.get_returnable_product,name='get_returnable_product'),
    url(r'^get-unit-price/$', views.get_unit_price, name='get_unit_price'),

    url(r'^get-expiry/$', views.get_expiry, name='get_expiry'),
    url(r'^get-purchase-items/$', views.get_purchase_items, name='get_purchase_items'),
    
    url(r'^upload-product-list/$', views.upload_product_list, name='upload_product_list'),
    
    url(r'^category/create/$',views.create_category,name='create_category'),
    url(r'^categories/$',views.categories,name='categories'),
    url(r'^category/edit/(?P<pk>.*)/$',views.edit_category,name='edit_category'),
    url(r'^category/view/(?P<pk>.*)/$',views.category,name='category'),
    url(r'^category/delete/(?P<pk>.*)/$',views.delete_category,name='delete_category'),
    url(r'^category/delete-selected/$', views.delete_selected_categories, name='delete_selected_categories'),

    url(r'^create-subcategory/$', views.create_subcategory, name='create_subcategory'),
    url(r'^subcategories/$', views.subcategories, name='subcategories'),
    url(r'^edit-subcategory/(?P<pk>.*)/$', views.edit_subcategory, name='edit_subcategory'),
    url(r'^subcategory/(?P<pk>.*)/$', views.subcategory, name='subcategory'),
    url(r'^delete-subcategory/(?P<pk>.*)/$', views.delete_subcategory, name='delete_subcategory'),
    url(r'^delete-selected-subcategories/$', views.delete_selected_subcategories, name='delete_selected_subcategories'),

    url(r'^brand/create/$',views.create_brand,name='create_brand'),
    url(r'^brands/$',views.brands,name='brands'),
    url(r'^brand/edit/(?P<pk>.*)/$',views.edit_brand,name='edit_brand'),
    url(r'^brand/view/(?P<pk>.*)/$',views.brand,name='brand'),
    url(r'^brand/delete/(?P<pk>.*)/$',views.delete_brand,name='delete_brand'),
    url(r'^brand/delete-selected/$', views.delete_selected_brands, name='delete_selected_brands'),

    url(r'^unit/create/$',views.create_unit,name='create_unit'),
    url(r'^units/$',views.units,name='units'),
    url(r'^unit/edit/(?P<pk>.*)/$',views.edit_unit,name='edit_unit'),
    url(r'^unit/view/(?P<pk>.*)/$',views.unit,name='unit'),
    url(r'^unit/delete/(?P<pk>.*)/$',views.delete_unit,name='delete_unit'),
    url(r'^unit/delete-selected/$', views.delete_selected_units, name='delete_selected_units'),

    url(r'^asset/create/$', views.create_asset, name='create_asset'),
    url(r'^asset/view/(?P<pk>.*)/$', views.asset, name='asset'),
    url(r'^asset/edit/(?P<pk>.*)/$', views.edit_asset, name='edit_asset'),
    url(r'^assets/$', views.assets, name='assets'),
    url(r'^delete-asset/(?P<pk>.*)/$', views.delete_asset, name='delete_asset'),
    url(r'^delete-selected-assets/$', views.delete_selected_assets, name='delete_selected_assets'),

    url(r'^barcode/create/$', views.create_barcode, name='create_barcode'),
    url(r'^barcodes/print/$', views.print_barcodes, name='print_barcodes'),

    url(r'^products/export/$', views.export_products, name='export_products'),
]