from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from products.models import Product,ProductAlternativeUnitPrice,Category,SubCategory,Measurement,Asset,Brand,ProductExpiryDate
from finance.models import TaxCategory
from finance.forms import TaxCategoryForm
from sales.models import ReturnableProduct
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, shop_required, check_account_balance,permissions_required,ajax_required
from main.functions import generate_form_errors, get_auto_id, get_a_id
import datetime
from django.db.models import Q
from dal import autocomplete
from django.views.decorators.http import require_GET
from django.forms.widgets import Select,TextInput
from users.functions import get_current_shop
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from products.forms import ProductBarcodeForm,ProductAlternativeUnitPriceForm,SubCategoryForm,NewProductForm,FileForm, MeasurementForm, ProductForm, CategoryForm,BrandForm,AssetForm,ProductExpiryForm
from django.core import serializers
from decimal import Decimal
import xlrd 
from vendors.models import Vendor
from django.views.decorators.http import require_POST
from django.template.loader import render_to_string
from django.conf import settings


class CategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = Category.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) | 
                                 Q(name__istartswith=self.q)
                                )
    
        return items

    def create_object(self, text):
        current_shop = get_current_shop(self.request)
        auto_id = get_auto_id(Category)
        a_id = get_a_id(Category,self.request)
        return Category.objects.create(
            auto_id=auto_id,
            a_id=a_id,
            name=text,
            shop=current_shop,
            creator=self.request.user,
            updator=self.request.user
        )


class SubcategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = SubCategory.objects.filter(is_deleted=False,shop=current_shop)

        category = self.forwarded.get('category', None)

        if category:
            items = items.filter(category=category)
        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) | 
                                 Q(name__istartswith=self.q)
                                )
    
        return items

    def create_object(self, text):
        current_shop = get_current_shop(self.request)
        auto_id = get_auto_id(SubCategory)
        a_id = get_a_id(SubCategory,self.request)
        category_pk = self.request.POST.get('forward[category]')
        if category_pk:
            category = Category.objects.get(pk=category_pk)
            return SubCategory.objects.create(
                auto_id=auto_id,
                a_id=a_id,
                name=text,
                category=category,
                shop=current_shop,
                creator=self.request.user,
                updator=self.request.user
            )


class BrandAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = Brand.objects.filter(is_deleted=False,shop=current_shop)


        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) | 
                                 Q(name__istartswith=self.q)
                                )
    
        return items

    def create_object(self, text):
        current_shop = get_current_shop(self.request)
        auto_id = get_auto_id(Brand)
        a_id = get_a_id(Brand,self.request)

        return Brand.objects.create(
            auto_id=auto_id,
            a_id=a_id,
            name=text,
            shop=current_shop,
            creator=self.request.user,
            updator=self.request.user
        )
    
    
class ProductAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = Product.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) | 
                                 Q(name__istartswith=self.q) | 
                                 Q(code__istartswith=self.q) | 
                                 Q(category__name__istartswith=self.q)
                                )
    
        return items



class AssetAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = Asset.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) | 
                                 Q(name__istartswith=self.q)
                                )
    
        return items

    def create_object(self, text):
        current_shop = get_current_shop(self.request)
        auto_id = get_auto_id(Asset)
        a_id = get_a_id(Asset,self.request)

        return Asset.objects.create(
            auto_id=auto_id,
            a_id=a_id,
            name=text,
            shop=current_shop,
            creator=self.request.user,
            updator=self.request.user
        )
    
    
@check_mode
@login_required
@shop_required
def dashboard(request):
    return HttpResponseRedirect(reverse('products:products'))


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_product'])
def create(request):    
    current_shop = get_current_shop(request)
    AlternativeUnitFormset = formset_factory(ProductAlternativeUnitPriceForm,extra=1)
    ProductExpiryFormset = formset_factory(ProductExpiryForm,extra=1)
    
    if request.method == 'POST':
        form = ProductForm(request.POST)
        form.fields['unit_type'].queryset = Measurement.objects.filter(shop=current_shop,is_base=True,is_deleted=False)
        alternative_unit_formset = AlternativeUnitFormset(request.POST,prefix='alternative_unit_formset')
        product_expiry_formset = ProductExpiryFormset(request.POST,prefix='product_expiry_formset')
        if form.is_valid() and alternative_unit_formset.is_valid() and product_expiry_formset.is_valid(): 
            
            auto_id = get_auto_id(Product)
            a_id = get_a_id(Product,request)
            error_messages = ''
            unit_type = form.cleaned_data['unit_type']
            base_unit = Measurement.objects.get(shop=current_shop,is_base=True,is_deleted=False,unit_type=unit_type)
            units = []
            for f in alternative_unit_formset:
                      
                unit = f.cleaned_data['unit']
                
                if unit in units:
                    error_messages += "Duplicate unit %s" % unit.unit_name
                else:
                    units.append(unit)

            code = form.cleaned_data['code'].upper()
            if Product.objects.filter(code=code,shop=current_shop,is_deleted=False).exists():
                error_messages += "Duplicate Code %s" % code

            if not error_messages:
                name = form.cleaned_data['name']
                name = name.capitalize()
                price = form.cleaned_data['price']
                wholesale_price = form.cleaned_data['wholesale_price']
                tax_category = form.cleaned_data['tax_category']
                tax_instance = None
                tax = 0
                
                if tax_category:
                    tax_instance = get_object_or_404(TaxCategory.objects.filter(is_deleted=False,pk=tax_category.pk))
                    tax = tax_instance.tax

                is_tax_included = form.cleaned_data['is_tax_included']
                is_expired = form.cleaned_data['is_expired']

                if is_tax_included:
                    tax_excluded_price = (100*price)/(100+tax)
                    wholesale_tax_excluded_price = (100*wholesale_price)/(100+tax)
                else :
                    tax_excluded_price = price
                    price = tax_excluded_price + (tax_excluded_price*tax)/100

                    wholesale_tax_excluded_price = wholesale_price
                    wholesale_price = wholesale_tax_excluded_price + (wholesale_tax_excluded_price*tax)/100

                #create product
                data = form.save(commit=False)
                data.creator = request.user
                data.updator = request.user
                data.auto_id = auto_id
                data.shop = current_shop
                data.name = name
                data.code = code
                data.tax_excluded_price = tax_excluded_price
                data.is_tax_included = is_tax_included
                data.wholesale_tax_excluded_price = wholesale_tax_excluded_price
                data.is_expired = is_expired
                data.price = price
                data.a_id = a_id 
                data.tax = tax
                data.unit = base_unit
                data.save()

                #save alternate units
                for f in alternative_unit_formset:
                      
                    unit = f.cleaned_data['unit']
                    cost = f.cleaned_data['cost']
                    price = f.cleaned_data['price']
                        
                    ProductAlternativeUnitPrice(
                        product = data,
                        unit = unit,
                        cost = cost,
                        price = price,
                        shop = current_shop
                    ).save()
                if is_expired:
                    for f in product_expiry_formset:
                        f_auto_id = get_auto_id(ProductExpiryDate)
                        manufacture_date = f.cleaned_data['manufacture_date']
                        best_before = f.cleaned_data['best_before']
                        period = f.cleaned_data['period']
                        batch_code = str(current_shop.name[:2]) + str(f_auto_id) 
                        if period == 'days' :
                            expiry_date = manufacture_date +  datetime.timedelta(days=best_before)
                        if period == 'month':
                            month_days = 30 * best_before
                            expiry_date = manufacture_date + datetime.timedelta(days=month_days)
                        
                        ProductExpiryDate(
                            auto_id = f_auto_id,
                            product = data,
                            manufacture_date = manufacture_date,
                            best_before = best_before,
                            period = period,
                            shop = current_shop,
                            expiry_date = expiry_date,
                            batch_code = batch_code
                        ).save()  
                new_window_url = reverse('products:create_barcode') + "?qty=" + str(data.stock) + "&product="+str(data.pk)
                response_data = {
                    "status" : "true",
                    "title" : "Successfully Created",
                    "message" : "Product created successfully.",
                    'new_redirect_window' : 'true',
                    "new_window_url" : new_window_url
                } 
            else:            
        
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Duplicate Entry",
                    "message" : error_messages
                }   
        
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')  
        
        else:            
            message = generate_form_errors(form,formset=False)
            message += generate_form_errors(alternative_unit_formset,formset=True)
            message += generate_form_errors(product_expiry_formset,formset=True)   
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        unit_type_instance = Measurement.objects.get(shop=current_shop,unit_type="quantity",is_base=True)        
        product_form = ProductForm(initial={'shop':current_shop,'unit_type':unit_type_instance})
        product_form.fields['unit_type'].queryset = Measurement.objects.filter(shop=current_shop,is_base=True,is_deleted=False)   
        product_form.fields['unit_type'].label_from_instance = lambda obj: "%s" % (obj.unit_type.capitalize()) 
        alternative_unit_formset = AlternativeUnitFormset(prefix='alternative_unit_formset')
        product_expiry_formset = ProductExpiryFormset(prefix='product_expiry_formset')
        for form in alternative_unit_formset:
            form.fields['unit'].queryset = Measurement.objects.filter(shop=current_shop,is_base=False,is_deleted=False)
            form.fields['unit'].label_from_instance = lambda obj: "%s (%s)" % (obj.unit_name, obj.code)
        context = {
            "title" : "Create Product ",
            "form" : product_form,
            "alternative_unit_formset" : alternative_unit_formset,
            "product_expiry_formset" : product_expiry_formset,
            "url" : reverse('products:create'),
            'new_redirect_window':True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_create_page" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'products/entry.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_product'])
def products(request):
    current_shop = get_current_shop(request)
    instances = Product.objects.filter(is_deleted=False,shop=current_shop)
    categories = Category.objects.filter(shop=current_shop,is_deleted=False)
    subcategories = SubCategory.objects.filter(shop=current_shop,is_deleted=False)
    brands = Brand.objects.filter(shop=current_shop,is_deleted=False)
    title = "Products"
    #filter block
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(name__icontains=query) | Q(code__icontains=query))
        title = "Products - %s" %query
        
    category = request.GET.get('category')
    subcategory = request.GET.get('subcategory')
    brand = request.GET.get('brand')
    if category:
        instances = instances.filter(category=category)
        if Category.objects.filter(pk=category).exists():
            cat = Category.objects.get(pk=category)
            title = "Products - %s" %cat.name

    if subcategory:
        instances = instances.filter(subcategory=subcategory)
        if SubCategory.objects.filter(pk=subcategory).exists():
            sub = SubCategory.objects.get(pk=subcategory)
            title = "Products - %s" %sub.name

    if brand:
        instances = instances.filter(brand=brand)
        if Brand.objects.filter(pk=brand).exists():
            bnd = Brand.objects.get(pk=brand)
            title = "Products - %s" %bnd.name
            
    context = {
        "instances" : instances,
        'title' : title,
        "brands" : brands,
        "categories" : categories,
        "subcategories" : subcategories,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/products.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_product'])
def product(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Product.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    alternative_units=ProductAlternativeUnitPrice.objects.filter(shop=current_shop,product=instance,is_deleted=False)
    expiry_dates = ProductExpiryDate.objects.filter(shop=current_shop,product=instance,is_deleted=False)
    context = {
        "instance" : instance,
        "alternative_units" : alternative_units,
        'expiry_dates': expiry_dates,
        "title" : "Product : " + instance.name,
        "single_page" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/product.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_product'])
def edit(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Product.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 

    if ProductAlternativeUnitPrice.objects.filter(shop=current_shop,product=instance).exists():
        extra_alternative_unit = 0
    else:
        extra_alternative_unit = 1

    if ProductExpiryDate.objects.filter(shop=current_shop,product=instance).exists():
        extra_product_expiry = 0
    else:
        extra_product_expiry = 1
    AlternativeUnitFormset = inlineformset_factory(
                                            Product, 
                                            ProductAlternativeUnitPrice, 
                                            can_delete=True,
                                            extra=extra_alternative_unit,
                                            fields = ["unit","cost","price"],
                                            widgets = {
                                                'unit': Select(attrs={'class': 'required form-control selectpicker'}),
                                                'cost': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Cost'}),
                                                'price': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Price'}),
                                                }
                                            )
    ProductExpiryDateFormset = inlineformset_factory(
                                            Product, 
                                            ProductExpiryDate, 
                                            can_delete=True,
                                            extra=extra_product_expiry,
                                            fields = ["manufacture_date","best_before","period"],
                                            widgets = {
                                                'manufacture_date': TextInput(attrs={'class': 'required form-control datepickerold','placeholder' : 'Date'}),
                                                'best_before': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Best before'}),
                                                'period': Select(attrs={'class': 'required form-control selectpicker'}),
                                                }
                                            )
    if request.method == 'POST':
        response_data = {}
        form = ProductForm(request.POST,instance=instance)
        alternative_unit_formset = AlternativeUnitFormset(request.POST,prefix='alternative_unit_formset',instance=instance)
        product_expiry_formset = ProductExpiryDateFormset(request.POST,prefix='product_expiry_formset',instance=instance)
        if form.is_valid() and alternative_unit_formset.is_valid() and product_expiry_formset.is_valid():  
            name = form.cleaned_data['name']
            name=name.capitalize()    
            price = form.cleaned_data['price']
            discount = form.cleaned_data['discount']
            actual_price = price - discount
            wholesale_price = form.cleaned_data['wholesale_price']
            tax_category = form.cleaned_data['tax_category']
            if tax_category :
                tax_instance = get_object_or_404(TaxCategory.objects.filter(is_deleted=False,pk=tax_category.pk))
                tax = tax_instance.tax
            else:
                tax = 0
            is_tax_included = form.cleaned_data['is_tax_included']

            if is_tax_included:
                tax_excluded_price = (100*actual_price)/(100+tax)
                wholesale_tax_excluded_price = (100*wholesale_price)/(100+tax)
            else :
                tax_excluded_price = actual_price
                price = tax_excluded_price + (tax_excluded_price*tax)/100

                wholesale_tax_excluded_price = wholesale_price
                wholesale_price = wholesale_tax_excluded_price + (wholesale_tax_excluded_price*tax)/100
     
            #update product
            data = form.save(commit=False)
            data.updator = request.user
            data.tax_excluded_price = tax_excluded_price
            data.is_tax_included = is_tax_included
            data.wholesale_tax_excluded_price = wholesale_tax_excluded_price
            data.price = price
            data.date_updated = datetime.datetime.now()
            data.name = name
            data.tax = tax
            data.save() 

            # Update AlternativeUnit
            alternative_units = alternative_unit_formset.save(commit=False)  
            for item in alternative_units:
                item.product = data
                item.creator = request.user
                item.updator = request.user
                item.date_updated = datetime.datetime.now()
                item.shop = current_shop
                item.save()   
            for obj in alternative_unit_formset.deleted_objects:
                obj.delete()

            product_expiry_dates = product_expiry_formset.save(commit=False)  
            for item in product_expiry_dates:
                item.product = data
                item.date_updated = datetime.datetime.now()
                item.shop = current_shop
                item.save()   
            for obj in product_expiry_formset.deleted_objects:
                obj.delete()            
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Product Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('products:product',kwargs={'pk':data.pk})
            }   
        else:
            message = {}
            message = generate_form_errors(form,formset=False)
            print product_expiry_formset.errors
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        product_form = ProductForm(instance=instance,initial={'shop':current_shop, 'price':tax_excluded_price, 'wholesale_price':wholesale_tax_excluded_price})
        product_form.fields['unit_type'].queryset = Measurement.objects.filter(shop=current_shop,is_base=True,is_deleted=False)   
        product_form.fields['unit_type'].label_from_instance = lambda obj: "%s" % (obj.unit_type.capitalize()) 
        product_form.fields['subcategory'].queryset = SubCategory.objects.filter(shop=current_shop,category=instance.category,is_deleted=False)
        alternative_unit_formset = AlternativeUnitFormset(prefix='alternative_unit_formset',instance=instance)
        product_expiry_formset = ProductExpiryDateFormset(prefix='product_expiry_formset',instance=instance)
        for form in alternative_unit_formset:
            form.fields['unit'].queryset = Measurement.objects.filter(shop=current_shop,is_base=False,unit_type=instance.unit_type)
            form.fields['unit'].label_from_instance = lambda obj: "%s (%s)" % (obj.unit_name, obj.code)

        context = {
            "form" : product_form,
            'alternative_unit_formset':alternative_unit_formset,
            'product_expiry_formset' :product_expiry_formset,
            "title" : "Edit Product : " + instance.name,
            "instance" : instance,
            "url" : reverse('products:edit',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'products/edit.html', context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_product'],allow_self=True,model=Product)
def delete(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Product.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    Product.objects.filter(pk=pk).update(is_deleted=True,code=instance.code + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Product Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('products:products')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_product'])
def delete_selected_products(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Product.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
            Product.objects.filter(pk=pk).update(is_deleted=True,code=instance.code + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Product(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('products:products')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
def get_product(request):
    current_shop = get_current_shop(request)
    pk = request.GET.get('id')
    barcode = request.GET.get('barcode')
    sale_type = request.GET.get('sale_type')
    product_exists = False

    if barcode == "yes": 
        if Product.objects.filter(shop=current_shop,code=pk.upper()).exists():
            item = Product.objects.get(shop=current_shop,code=pk.upper())
            product_exists = True
    else:     
        if Product.objects.filter(pk=pk,shop=current_shop).exists():
            item = Product.objects.get(pk=pk)
            product_exists = True
    
    if product_exists:
        units = []
        unit = {
            "pk" : str(item.unit.pk),
            "code" : item.unit.code,
            "name" : item.unit.unit_name,
        }
        units.append(unit)
        alternate_units = ProductAlternativeUnitPrice.objects.filter(product=item,shop=current_shop)
        for u in alternate_units:
            unit = {
                "pk" : str(u.unit.pk),
                "code" : u.unit.code,
                "name" : u.unit.unit_name,
            }
            units.append(unit)  
        tax_excluded_price = item.tax_excluded_price 
        if sale_type ==  "retail" :
            tax_excluded_price = item.tax_excluded_price 
        elif sale_type == "wholesale":
            tax_excluded_price = item.wholesale_tax_excluded_price
        elif sale_type == "wholesale_with_customer":
            tax_excluded_price = item.wholesale_tax_excluded_price

        response_data = {
            "status" : "true",
            'pk' : str(item.pk),
            'code' : item.code,
            'name' : item.name,
            'unit_code' : item.unit.code,
            'unit_name' : item.unit.unit_name,
            'unit_type' : item.unit.unit_type,
            'cost' : str(item.cost),
            'price' : str(tax_excluded_price),   
            'stock' : str(item.stock),    
            'tax' : str(item.tax),
            'stock' : str(item.stock),
            'discount' : str(item.discount),         
            'is_deleted' : item.is_deleted,   
            'units' : units ,
            'wholesale_price' : str(item.wholesale_price)
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Product not found"
        }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required 
@shop_required
@require_GET
def get_unit_price(request):
    current_shop = get_current_shop(request)
    pk = request.GET.get('unit')
    product = request.GET.get('product')
    sale_type = request.GET.get('sale_type')

    if Product.objects.filter(shop=current_shop,pk=product).exists() and Measurement.objects.filter(shop=current_shop,pk=pk).exists():
        instance =  Measurement.objects.get(pk=pk)
        product_instance = Product.objects.get(shop=current_shop,pk=product)
        if instance.is_base:
            cost = product_instance.cost
            price = product_instance.tax_excluded_price 
            if sale_type ==  "retail" :
                price = product_instance.tax_excluded_price 
            elif sale_type == "wholesale":
                price = product_instance.wholesale_tax_excluded_price 
            elif sale_type == "wholesale_with_customer":
                price = product_instance.wholesale_tax_excluded_price 
        else:
            alternate_unit_instance = ProductAlternativeUnitPrice.objects.get(unit=instance,product=product_instance,shop=current_shop)
            cost = alternate_unit_instance.cost
            price = alternate_unit_instance.price
            
        response_data = {
            'price' : str(price),
            'cost' : str(cost),
            "pk" : str(instance.pk),
            "code" : instance.code,
            "name" : instance.unit_name, 
            "status" : "true"
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Unit or product not found"
        }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
            
        
@check_mode
@ajax_required
@login_required 
@shop_required
@require_GET
def get_returnable_product(request):
    current_shop = get_current_shop(request)
    pk = request.GET.get('id') 

    product_exists = False

    if ReturnableProduct.objects.filter(pk=pk,shop=current_shop).exists():
        
        item = ReturnableProduct.objects.get(pk=pk,shop=current_shop).product
        product_exists = True
    
    if product_exists:
        units = []
        unit = {
            "pk" : str(item.unit.pk),
            "code" : item.unit.code,
            "name" : item.unit.unit_name,
            }
        units.append(unit)
        alternate_units = ProductAlternativeUnitPrice.objects.filter(product=item,shop=current_shop)
        print alternate_units
        for u in alternate_units:
            unit = {
                "pk" : str(u.unit.pk),
                "code" : u.unit.code,
                "name" : u.unit.unit_name,
            }
            units.append(unit)  
            
        response_data = {
            "status" : "true",
            'pk' : str(item.pk),
            'code' : item.code,
            'name' : item.name,
            'cost' : str(item.cost),
            'price' : str(item.price),   
            'stock' : str(item.stock),    
            'tax' : str(item.tax),
            'discount' : str(item.discount),
            'is_deleted' : item.is_deleted,
            "units" : units       
             }
    else:
        response_data = {
            "status" : "false",
            "message" : "Product not found"
        }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

@check_mode
@login_required 
def get_asset(request):
    current_shop = get_current_shop(request)
    pk = request.GET.get('id')

    asset_exists = False    

    if Asset.objects.filter(pk=pk,shop=current_shop).exists():
        item = Asset.objects.get(pk=pk)
        asset_exists = True
    
    if asset_exists:
            
        response_data = {
            "status" : "true",
            'pk' : str(item.pk),
            'name' : item.name, 
            'stock' : str(item.available_stock), 
            'is_deleted' : item.is_deleted
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Asset not found"
        }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_category'])
def create_category(request):    
    current_shop = get_current_shop(request)
    
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        
        if form.is_valid(): 
            
            auto_id = get_auto_id(Category)
            a_id = get_a_id(Category,request)
            
            #create category
            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.shop = current_shop
            data.a_id = a_id
            data.save()    
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Category created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('products:category',kwargs={'pk':data.pk})
            }   
        
        else:            
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        form = CategoryForm()
        context = {
            "title" : "Create Category ",
            "form" : form,
            "url" : reverse('products:create_category'),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'products/category_entry.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_category'])
def categories(request):
    current_shop = get_current_shop(request)
    instances = Category.objects.filter(is_deleted=False,shop=current_shop)
    title = "Categories"
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(name__icontains=query))
        title = "Categories - %s" %query
        
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/categories.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_category'])
def category(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Category.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    context = {
        "instance" : instance,
        "title" : "Category : " + instance.name,
        "single_page" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/category.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_category'])
def edit_category(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Category.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    
    if request.method == 'POST':
        response_data = {}
        form = CategoryForm(request.POST,instance=instance)
        
        if form.is_valid():      
                   
            #update category
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()            
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Category Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('products:category',kwargs={'pk':data.pk})
            }   
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = CategoryForm(instance=instance)
        
        context = {
            "form" : form,
            "title" : "Edit Category : " + instance.name,
            "instance" : instance,
            "url" : reverse('products:edit_category',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'products/category_entry.html', context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_category'])
def delete_category(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Category.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    Category.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Category Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('products:categories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_category'])
def delete_selected_categories(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Category.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
            Category.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Categories Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('products:categories')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

@check_mode
@login_required
@shop_required
@permissions_required(['can_create_subcategory'])
def create_subcategory(request):
    current_shop = get_current_shop(request)
    instances = SubCategory.objects.filter(is_deleted=False, shop=current_shop)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query)|Q(category__name=query))

    if request.method == 'POST':
        form = SubCategoryForm(request.POST)

        if form.is_valid():
            auto_id = get_auto_id(SubCategory)
            a_id = get_a_id(SubCategory,request)

            data = form.save(commit=False)
            data.auto_id = auto_id
            data.a_id = a_id
            data.creator = request.user
            data.updator = request.user
            data.shop = current_shop
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "SubCategory created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('products:create_subcategory')
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = SubCategoryForm()

        context = {
            "title" : "Create SubCategory",
            "form" : form,
            "redirect" : True,
            "url" : reverse('products:create_subcategory'),
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "products" : True,
            "products" : True,
        }
        return render(request,'products/subcategory_entry.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_subcategory'])
def subcategories(request):
    current_shop = get_current_shop(request)
    instances = SubCategory.objects.filter(is_deleted=False, shop=current_shop)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query)|Q(category__name=query))

    context = {
        'instances': instances,
        "title" : 'subcategories',
        "is_need_select_picker" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_popup_box" : True,
        "is_need_animations": True,
        "products" : True,
        "products" : True,
        }
    return render(request, "products/subcategories.html", context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_subcategory'])
def edit_subcategory(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(SubCategory.objects.filter(pk=pk, is_deleted=False, shop=current_shop))
    query = request.GET.get("q")
    if query:
        instance = instance.filter(Q(name__icontains=query)|Q(category__name=query))

    if request.method == "POST":
        form = SubCategoryForm(request.POST, instance=instance)

        if form.is_valid():
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "SubCategory updated successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('products:subcategories')
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = SubCategoryForm(instance=instance)

        context = {
            "instance" : instance,
            "title" : "Edit SubCategory :" + instance.name,
            "form" : form,
            "redirect" : True,
            "url" : reverse('products:edit_subcategory', kwargs={'pk':instance.pk}),
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "products" : True,
            
        }
        return render(request,'products/subcategory_entry.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_subcategory'])
def subcategory(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(SubCategory.objects.filter(pk=pk, is_deleted=False, shop=current_shop))
    query = request.GET.get("q")
    if query:
        instance = instance.filter(Q(name__icontains=query)|Q(category__name=query))

    context = {
        'instance': instance,
        'title':'SubCategory',
        "is_need_select_picker" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_popup_box" : True,
        "products" : True,
    }
    return render(request, "products/subcategory.html", context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_subcategory'])
def delete_subcategory(request,pk):
    current_shop = get_current_shop(request)
    SubCategory.objects.filter(pk=pk, shop=current_shop).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "SubCategory deleted successfully.",
        "redirect" : "true",
        "redirect_url" : reverse('products:subcategories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_subcategory'])
def delete_selected_subcategories(request):
    current_shop = get_current_shop(request)

    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(SubCategory.objects.filter(pk=pk, is_deleted=False, shop=current_shop))
            SubCategory.objects.filter(pk=pk, shop=current_shop).update(is_deleted=True, name=instance.name + "_deleted_" + str(instance.auto_id))

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Sub Category Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('products:subcategories')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_brand'])
def create_brand(request):    
    current_shop = get_current_shop(request)
    
    if request.method == 'POST':
        form = BrandForm(request.POST)
        
        if form.is_valid(): 
            
            auto_id = get_auto_id(Brand)
            a_id = get_a_id(Brand,request)
            
            #create brand

            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.shop = current_shop
            data.a_id = a_id
            data.save()    
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Brand created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('products:brand',kwargs={'pk':data.pk})
            }   
        
        else:            
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        form = BrandForm()
        context = {
            "title" : "Create Brand ",
            "form" : form,
            "url" : reverse('products:create_brand'),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'products/brand_entry.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_brand'])
def brands(request):
    current_shop = get_current_shop(request)
    instances = Brand.objects.filter(is_deleted=False,shop=current_shop)
    title = "Brands"
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(name__icontains=query))
        title = "Brands - %s" %query
        
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/brands.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_brand'])
def brand(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Brand.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    context = {
        "instance" : instance,
        "title" : "Brand : " + instance.name,
        "single_page" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/brand.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_brand'])
def edit_brand(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Brand.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    
    if request.method == 'POST':
        response_data = {}
        form = BrandForm(request.POST,instance=instance)
        
        if form.is_valid():      
                   
            #update brand
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()            
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Brand Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('products:brand',kwargs={'pk':data.pk})
            }   
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = BrandForm(instance=instance)
        
        context = {
            "form" : form,
            "title" : "Edit Brand : " + instance.name,
            "instance" : instance,
            "url" : reverse('products:edit_brand',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'products/brand_entry.html', context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_brand'])
def delete_brand(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Brand.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    Brand.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Brand Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('products:categories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_brand'])
def delete_selected_brands(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Brand.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
            Brand.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Brands Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('products:brands')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



@check_mode
@login_required
@shop_required
@permissions_required(['can_create_unit'])
def create_unit(request):    
    current_shop = get_current_shop(request)
    
    if request.method == 'POST':
        form = MeasurementForm(request.POST)
        
        if form.is_valid(): 
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            
            #create unit
            code = form.cleaned_data['code']
            code = code.upper()
            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.shop = current_shop
            data.a_id = a_id
            data.code = code
            data.save()    
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Measurement created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('products:unit',kwargs={'pk':data.pk})
            }   
        
        else:            
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        form = MeasurementForm(initial={'shop':current_shop})
        context = {
            "title" : "Create Measurement ",
            "form" : form,
            "url" : reverse('products:create_unit'),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'products/unit_entry.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_unit'])
def units(request):
    current_shop = get_current_shop(request)
    instances = Measurement.objects.filter(is_deleted=False,shop=current_shop)
    title = "Units"
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(name__icontains=query))
        title = "Units - %s" %query
        
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/units.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_unit'])
def unit(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Measurement.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    context = {
        "instance" : instance,
        "title" : "Measurement : " + instance.unit_name,
        "single_page" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/unit.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_unit'])
def edit_unit(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Measurement.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    
    if request.method == 'POST':
        response_data = {}
        form = MeasurementForm(request.POST,instance=instance)
        
        if form.is_valid():      
                   
            #update unit
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()            
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Measurement Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('products:unit',kwargs={'pk':data.pk})
            }   
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = MeasurementForm(instance=instance)
        
        context = {
            "form" : form,
            "title" : "Edit Measurement : " + instance.unit_name,
            "instance" : instance,
            "url" : reverse('products:edit_unit',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'products/unit_entry.html', context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_unit'])
def delete_unit(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Measurement.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    Measurement.objects.filter(pk=pk).update(is_deleted=True,code=instance.code + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Measurement Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('products:units')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_unit'])
def delete_selected_units(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Measurement.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
            Measurement.objects.filter(pk=pk).update(is_deleted=True,code=instance.code + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Categories Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('products:units')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



@check_mode
@login_required
@shop_required
@permissions_required(['can_manage_barcode'])
def create_barcode(request):
    product = None
    pk = request.GET.get('product')
    if pk:
        product = get_object_or_404(Product.objects.filter(pk=pk))
    current_shop = get_current_shop(request)

    unit = 10
    qty = request.GET.get('qty')
    if qty:
        if qty.isdigit():
            unit = qty

    form = ProductBarcodeForm(initial={"unit":unit,"product":product})

    context = {
        'title' : "Create Barcode",
        "form" : form,  

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'products/create_barcode.html',context)

@check_mode
@login_required
def print_barcodes(request):
    current_shop = get_current_shop(request)
    unit = request.GET.get('unit')
    if not unit:
        unit = 1
    else:
        unit = int(unit)
        
    skip_row = request.GET.get('skip_row')
    if not skip_row:
        skip_row = 0
    else:
        skip_row = int(skip_row)
        
    pk = request.GET.get('product')
    instance = get_object_or_404(Product.objects.filter(pk=pk,shop=current_shop,is_deleted=False))
    n_range = range(0,unit)
    
    padding_top = 10.9
    additional_padding = skip_row * 21.2
    padding_top += additional_padding
    
    context = {
        'title' : "Barcodes : " + instance.name,
        "instance" : instance,
        "n_range" : n_range,
        "padding_top" : padding_top
        
    }
    return render(request,'products/print_barcodes.html',context)
    
    
@login_required 
def get_product_sub_categories(request):
    pk = request.GET.get('pk')
    instances = SubCategory.objects.filter(category__pk=pk,is_deleted=False)
    
    json_models = serializers.serialize("json", instances)
    return HttpResponse(json_models, content_type="application/javascript")


@check_mode
@ajax_required
@login_required
@shop_required
def get_product_units(request):
    current_shop = get_current_shop(request)
    pk = request.GET.get('pk')
    if Measurement.objects.filter(shop=current_shop,is_deleted=False,unit_type=pk,is_base=False).exists():
        instances = Measurement.objects.filter(shop=current_shop,unit_type=pk,is_deleted=False,is_base=False)
    else:
        instances = []
    
    json_models = serializers.serialize("json", instances)
    return HttpResponse(json_models, content_type="application/javascript")


@check_mode
@login_required
def upload_product_list(request):
    current_shop = get_current_shop(request)
    if request.method == 'POST':
        form = FileForm(request.POST,request.FILES)

        if form.is_valid():
            input_excel = request.FILES['file']
            book = xlrd.open_workbook(file_contents=input_excel.read())
            sheet = book.sheet_by_index(0)

            dict_list = []
            keys = [str(sheet.cell(0, col_index).value) for col_index in xrange(sheet.ncols)]
            for row_index in xrange(1, sheet.nrows):
                d = {keys[col_index]: str(sheet.cell(row_index, col_index).value)
                    for col_index in xrange(sheet.ncols)}
                dict_list.append(d)

            is_ok = True
            message = ''
            row_count = 2

            for item in dict_list:
                code = item['code']
                category = item['category']
                product = item['product']
                stock = Decimal(item['qty'])
                unit = item['unit']
                cost = Decimal(item['cost'])
                price = Decimal(item['price'])
                wholesale_price = Decimal(item['wholesale_price'])
                tax = Decimal(item['tax'])
                vendor = item['vendor']

                if Category.objects.filter(name=category,is_deleted=False,shop=current_shop).exists():
                    category = Category.objects.get(name=category,is_deleted=False,shop=current_shop)
                else:
                    category = Category.objects.create(
                                    name=category,
                                    shop=current_shop,
                                    creator=request.user,
                                    updator=request.user,
                                    auto_id=get_auto_id(Category),
                                    a_id=get_a_id(Category,request)
                                )
                

                if Vendor.objects.filter(name=vendor,is_deleted=False,shop=current_shop).exists():
                    vendor = Vendor.objects.get(name=vendor,is_deleted=False,shop=current_shop)
                else:
                    vendor = Vendor.objects.create(
                                    name=vendor,
                                    address='address',
                                    phone="9999999999",
                                    shop=current_shop,
                                    creator=request.user,
                                    updator=request.user,
                                    auto_id=get_auto_id(Vendor),
                                    a_id=get_a_id(Vendor,request)
                                ) 
                tax_category = None
                if TaxCategory.objects.filter(shop=current_shop,is_deleted=False,tax=tax).exists():
                	tax_category = TaxCategory.objects.get(shop=current_shop,is_deleted=False,tax=tax)
                else:
                	tax_category = TaxCategory.objects.create(
                		shop=current_shop,
                		name = str(tax),
                		is_deleted=False,
                		tax=tax,
                        creator=request.user,
                        updator=request.user,
                        auto_id=get_auto_id(TaxCategory),
                        a_id=get_a_id(TaxCategory,request)
                	)
                # tax_excluded_price = (100*price)/(100+tax)
                # wholesale_tax_excluded_price = (100*wholesale_price)/(100+tax)

                #price and taxed price calcualtion
                tax_excluded_price = price
                price = tax_excluded_price + (tax_excluded_price*tax)/100
                wholesale_tax_excluded_price = wholesale_price
                wholesale_price = wholesale_tax_excluded_price + (wholesale_tax_excluded_price*tax)/100

                unit_type = "quantity"
                unit_instance = None
                if unit == "each":
                    unit_type = "quantity"
                    unit_instance = Measurement.objects.get(shop=current_shop,is_system_generated=True,code="EA")
                elif unit == "m":
                    unit_type = "distance"
                    unit_instance = Measurement.objects.get(shop=current_shop,is_system_generated=True,code="MT")
                elif unit == "sq":
                    unit_type = "area"
                    if not Measurement.objects.filter(shop=current_shop,is_system_generated=True,code="SQ").exists():
                        auto_id = get_auto_id(Measurement)
                        a_id = get_a_id(Measurement,request)
                        Measurement(code='SQ',unit_type='area',unit_name='square_feet',conversion_factor=0.0,shop=current_shop,creator=request.user,updator=request.user,auto_id=auto_id,a_id=a_id,is_system_generated=True).save()

                    unit_instance = Measurement.objects.get(shop=current_shop,is_system_generated=True,code="SQ")
                    ('area', 'Area'),
                elif unit == "set":
                    unit_type = "quantity"
                    if Measurement.objects.filter(shop=current_shop,code="SET").exists():
                        unit_instance = Measurement.objects.get(shop=current_shop,code="SET")
                    else:
                        auto_id = get_auto_id(Measurement)
                        a_id = get_a_id(Measurement,request)
                        unit_instance = Measurement.objects.create(code='SET',unit_type='quantity',unit_name='set',conversion_factor=2,shop=current_shop,creator=request.user,updator=request.user,auto_id=auto_id,a_id=a_id)
                elif unit == "dz":
                    unit_type = "quantity"
                    unit_instance = Measurement.objects.get(shop=current_shop,code="DZ")
                elif unit == "kg":
                    unit_type = "weight"
                    unit_instance = Measurement.objects.get(shop=current_shop,unit_type='weight',is_system_generated=True,code="KG")

                if not Product.objects.filter(code=code,shop=current_shop,is_deleted=False).exists():
                    instance = Product(
                                   code = code,
                                   name = product,
                                   cost = cost,
                                   price = price,
                                   wholesale_price = wholesale_price,
                                   tax = tax,
                                   discount = 0,
                                   stock = stock,
                                   category = category,
                                   low_stock_limit = 0,
                                   creator = request.user,
                                   updator = request.user,
                                   shop = current_shop,
                                   a_id = get_a_id(Product,request),
                                   auto_id=get_auto_id(Product),
                                   is_tax_included = False,
                                   tax_excluded_price = tax_excluded_price,
                                   tax_category = tax_category,
                                   unit_type = unit_type,
                                   unit = unit_instance,
                                   wholesale_tax_excluded_price = wholesale_tax_excluded_price,
                                   vendor=vendor
                                )
                    instance.save()

            return HttpResponseRedirect(reverse('products:products'))
        else:
            form = FileForm()

            context = {
                "form" : form,
                "title" : "Upload Product List",

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
            }
            return render(request, 'products/upload_product_list.html', context)
    else:
        form = FileForm()

        context = {
            "form" : form,
            "title" : "Upload Product List",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'products/upload_product_list.html', context)


@login_required
def export_products(request):
    current_shop = get_current_shop(request)
    instances = Product.objects.filter(is_deleted=False,shop=current_shop)

    import xlwt
    import urllib

    wb = xlwt.Workbook()
    ws = wb.add_sheet('Products')

    ws.write(0, 0, "code")
    ws.write(0, 1, "category")
    ws.write(0, 2, "product")
    ws.write(0, 3, "qty")
    ws.write(0, 4, "unit")
    ws.write(0, 5, "cost")
    ws.write(0, 6, "price")
    ws.write(0, 7, "wholesale_price")
    ws.write(0, 8, "tax")
    ws.write(0, 9, "vendor")

    counter = 1
    for instance in instances:
        vendor_name = ""
        if instance.vendor:
            vendor_name = instance.vendor.name

        category_name = ""
        if instance.category:
            category_name = instance.category.name

        unit_name = ""
        if instance.unit:
            unit_name = instance.unit.unit_name

        ws.write(counter, 0, instance.code)
        ws.write(counter, 1, category_name)
        ws.write(counter, 2, instance.name)
        ws.write(counter, 3, instance.stock)
        ws.write(counter, 4, unit_name)
        ws.write(counter, 5, instance.cost)
        ws.write(counter, 6, instance.price)
        ws.write(counter, 7, instance.wholesale_price)
        ws.write(counter, 8, instance.tax)
        ws.write(counter, 9, vendor_name)
        counter += 1
    
    file_location = settings.MEDIA_ROOT + "product_list.xls"
    wb.save(file_location)
    protocol = "http://"
    if request.is_secure():
        protocol = "https://"

    web_host = request.get_host()
    file_url = protocol + web_host + "/media/product_list.xls"
    print file_url

    response_data = {
        "status" : "true",
        "file_url" : file_url
    }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
def get_expiry(request):
    manufacture_date = request.GET.get('manufacture_date')
    period = request.GET.get('period')
    best_before = int(request.GET.get('best_before'))


    if manufacture_date:
        try:
            manufacture_date = datetime.datetime.strptime(manufacture_date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    if period == 'days' :
        expiry_date = manufacture_date +  datetime.timedelta(days=best_before)
    if period == 'month':
        month_days = 30 * best_before
        expiry_date = manufacture_date + datetime.timedelta(days=month_days)
    response_data = {
            "status" : "true",
            "expiry_date" : str(expiry_date)
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_asset'])
def create_asset(request): 
    current_shop = get_current_shop(request)    
    if request.method == "POST":
        form = AssetForm(request.POST)
            
        if form.is_valid():
            
            auto_id = get_auto_id(Asset)
            a_id = get_a_id(Asset,request)
              
            #create asset
            stock = form.cleaned_data['stock']
            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.a_id = a_id
            data.shop = current_shop
            data.available_stock = stock
            data.save()
            
            response_data = {
                "status" : "true",
                "title" : "Succesfully Created",
                "redirect" : "true",
                "redirect_url" : reverse('products:assets'),
                "message" : "Asset Successfully Created."
            }
        else:
            message = generate_form_errors(form,formset=False)  
            print form.errors()      
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }            
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = AssetForm()        
        context = {
            "form" : form,
            "title" : "Create Asset",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_animations": True,
            "is_need_datetime_picker" : True,
            
        }
        return render(request, 'products/entry_asset.html', context)
    
    
@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_asset'])
def edit_asset(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Asset.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    if request.method == "POST":
        response_data = {}  
        form = AssetForm(request.POST,instance=instance)
        
        if form.is_valid(): 
            
            #update asset
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()
            
            response_data = {
                "status" : "true",
                "title" : "Succesfully Updated",
                "redirect" : "true",
                "redirect_url" : reverse('products:asset', kwargs = {'pk' :pk}),
                "message" : "Asset Successfully Updated."
            }
        else:
            message = generate_form_errors(form,formset=False)        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : form.errors
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = AssetForm(instance=instance)
        
        context = {
            "form" : form,
            "title" : "Edit Asset : " + instance.name,
            "instance" : instance,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_animations": True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'products/entry_asset.html', context)

    
@check_mode
@login_required
@shop_required 
@permissions_required(['can_view_asset'])     
def assets(request):
    current_shop = get_current_shop(request)
    instances = Asset.objects.filter(is_deleted=False,shop=current_shop)

    title = "Assets"
    
    #filter by query
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query))
        
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/assets.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_asset'])
def asset(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Asset.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    context = {
        "instance" : instance,
        "title" : "Asset : " + instance.name,
        "single_page" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'products/asset.html',context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_asset'])
def delete_asset(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Asset.objects.filter(pk=pk,shop=current_shop))
    Asset.objects.filter(pk=pk).update(is_deleted=True)
    
    response_data = {
        "status" : "true",
        "title" : "Succesfully Deleted",
        "redirect" : "true",
        "redirect_url" : reverse('products:assets'),
        "message" : "Asset Successfully Deleted."
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
def delete_selected_assets(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Asset.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
            Asset.objects.filter(pk=pk,shop=current_shop).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Asset(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('products:assets')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some asset first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    

@check_mode
@ajax_required
@login_required
@shop_required
@require_POST
@permissions_required(['can_create_product'])
def create_new_product(request):    
    current_shop = get_current_shop(request)
    AlternativeUnitFormset = formset_factory(ProductAlternativeUnitPriceForm,extra=1)
    
    form = NewProductForm(request.POST)
    form.fields['unit_type'].queryset = Measurement.objects.filter(shop=current_shop,is_base=True,is_deleted=False)
    alternative_unit_formset = AlternativeUnitFormset(request.POST,prefix='alternative_unit_formset')
    if form.is_valid() and alternative_unit_formset.is_valid():
        auto_id = get_auto_id(Product)
        input_row_counter = request.POST.get('input_row_counter')
        a_id = get_a_id(Product,request)
        error_messages = ''
        unit_type = form.cleaned_data['unit_type']
        base_unit = Measurement.objects.get(shop=current_shop,is_base=True,is_deleted=False,unit_type=unit_type)
        units = []
        for f in alternative_unit_formset:
                  
            unit = f.cleaned_data['unit']

            
            if unit in units:
                error_messages += "Duplicate unit %s" % unit.unit_name
            else:
                units.append(unit)

        if not error_messages:
            name = form.cleaned_data['name']
            name = name.capitalize()
            price = form.cleaned_data['price']
            wholesale_price = form.cleaned_data['wholesale_price']
            tax_category = form.cleaned_data['tax_category']
            tax_instance = None
            tax = 0
            
            if tax_category:
                tax_instance = get_object_or_404(TaxCategory.objects.filter(is_deleted=False,pk=tax_category.pk))
                tax = tax_instance.tax

            is_tax_included = form.cleaned_data['is_tax_included']
            is_expired = form.cleaned_data['is_expired']

            if is_tax_included:
                tax_excluded_price = (100*price)/(100+tax)
                wholesale_tax_excluded_price = (100*wholesale_price)/(100+tax)
            else :
                tax_excluded_price = price
                price = tax_excluded_price + (tax_excluded_price*tax)/100

                wholesale_tax_excluded_price = wholesale_price
                wholesale_price = wholesale_tax_excluded_price + (wholesale_tax_excluded_price*tax)/100

            #create product
            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.shop = current_shop
            data.name = name
            data.tax_excluded_price = tax_excluded_price
            data.is_tax_included = is_tax_included
            data.wholesale_tax_excluded_price = wholesale_tax_excluded_price
            data.is_expired = is_expired
            data.price = price
            data.a_id = a_id 
            data.tax = tax
            data.unit = base_unit
            data.save()

            #save alternate units
            for f in alternative_unit_formset:
                  
                unit = f.cleaned_data['unit']
                cost = f.cleaned_data['cost']
                price = f.cleaned_data['price']
                    
                ProductAlternativeUnitPrice(
                    product = data,
                    unit = unit,
                    cost = cost,
                    price = price,
                    shop = current_shop
                ).save()
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Product created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('products:product',kwargs={'pk':data.pk}),
                "data" : {
                    "pk" : str(data.pk),
                    "text" : data.code + " - " + data.name,
                    "input_row_counter" : str(input_row_counter)
                }
            } 
        else:            
    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Duplicate Entry",
                "message" : error_messages
            }   
    
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')  
    
    else:            
        message = generate_form_errors(form,formset=False)
        message += generate_form_errors(alternative_unit_formset,formset=True) 
        response_data = {
            "status" : "false",
            "stable" : "true",
            "title" : "Form validation error",
            "message" : form.errors
        }   
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    

@check_mode
@ajax_required
@login_required
@shop_required
def get_purchase_items(request):
    current_shop = get_current_shop(request)
    pk = request.GET.get('id')
    response_data = {}
    template_name = 'purchases/includes/purchase_invoice_items.html'
    purchase_invoice_items_list = []
    instances = []
    if Product.objects.filter(vendor__pk=pk).exists():
        instances = Product.objects.filter(vendor__pk=pk)
        for instance in instances:
            if instance.stock < instance.product_expiry_before:
                purchase_invoice_items_dict = {
                    'product':instance,
                }
                purchase_invoice_items_list.append(purchase_invoice_items_dict)
    if instances :  
        context = {
            'purchase_invoice_items' : purchase_invoice_items_list,
        }
        html_content = render_to_string(template_name,context)
        response_data = {
            "status" : "true",
            'template' : html_content,
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Product not found"
        }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')