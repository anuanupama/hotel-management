from products.models import Product,Asset

def get_exact_qty(qty,unit):
    is_base = unit.is_base
    if not is_base:
        conversion_factor = unit.conversion_factor
        return qty * conversion_factor
    else:
        return qty

def update_sock(pk,qty,status):
    product = Product.objects.get(pk=pk)
    stock = product.stock
    if status == "increase":
        balance_stock = stock + qty
    elif status == "decrease":
        balance_stock = stock - qty
    Product.objects.filter(pk=pk).update(stock=balance_stock)

def update_asset_stock(pk,qty,status):
    asset = Asset.objects.get(pk=pk)
    stock = asset.stock
    if status == "increase":
        balance_stock = stock + qty
    elif status == "decrease":
        balance_stock = stock - qty
    Asset.objects.filter(pk=pk).update(stock=balance_stock)


def update_asset_available_stock(pk,qty,status):
    print "dsfsf"
    asset = Asset.objects.get(pk=pk)
    stock = asset.available_stock
    if status == "increase":
        balance_stock = stock + qty
    elif status == "decrease":
        balance_stock = stock - qty
    Asset.objects.filter(pk=pk).update(available_stock=balance_stock)
    