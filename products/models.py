from __future__ import unicode_literals
from django.db import models
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from main.models import BaseModel


UNIT_TYPES = (
    ('quantity', 'Quantity'),
    ('weight', 'Weight'),
    ('distance', 'Distance'),
    ('volume', 'Volume'),
    ('time', 'Time'),
    ('area', 'Area'),
)


PERIOD = (
    ('days', 'Days'),
    ('month', 'Month'),
)


class Product(BaseModel):
    shop = models.ForeignKey("main.Shop")
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=128) 
    hsn = models.CharField(max_length=128,blank=True,null=True) 
    
    brand = models.ForeignKey("products.Brand",null=True,blank=True,limit_choices_to={'is_deleted': False}) 
    category = models.ForeignKey("products.Category",null=True,blank=True,limit_choices_to={'is_deleted': False})
    subcategory = models.ForeignKey("products.SubCategory",null=True,blank=True,limit_choices_to={'is_deleted': False}) 
    unit_type = models.CharField(max_length=128, choices=UNIT_TYPES,default="quantity") 
    unit = models.ForeignKey("products.Measurement") 
    vendor = models.ForeignKey("vendors.Vendor",blank=True,null=True)
    stock = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    cost = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    price = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    wholesale_price = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    wholesale_tax_excluded_price = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    tax_excluded_price = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    tax_category = models.ForeignKey("finance.TaxCategory",null=True,blank=True,limit_choices_to={'is_deleted': False})   
    tax = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    discount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    best_before = models.PositiveIntegerField(default=0,blank=True,null=True)
    
    low_stock_limit = models.PositiveIntegerField(default=1)
    product_expiry_before = models.PositiveIntegerField(default=10)
    
    is_deleted = models.BooleanField(default=False)
    is_tax_included = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'products_product'
        verbose_name = _('product')
        verbose_name_plural = _('products')
        ordering = ('auto_id',)     
        unique_together = (("shop","code"),)   
    
    def __unicode__(self):
        value =  self.name
        if self.category:
            value += " - " + self.category.name
        if self.subcategory:
            value += " - " + self.subcategory.name

        return value

    
class ProductExpiryDate(models.Model):
    shop = models.ForeignKey("main.Shop")
    auto_id = models.PositiveIntegerField(db_index=True,unique=True)
    date_added = models.DateTimeField(db_index=True,auto_now_add=True) 
    product = models.ForeignKey("products.Product",null=True,blank=True,limit_choices_to={'is_deleted': False})
    purchase = models.ForeignKey("purchases.Purchase",null=True,blank=True,limit_choices_to={'is_deleted': False})
    batch_code = models.CharField(max_length=128)
    manufacture_date = models.DateField(null=True,blank=True,)
    best_before = models.PositiveIntegerField(null=True,blank=True,)
    expiry_date = models.DateField(null=True,blank=True,)
    period = models.CharField(max_length=128, choices=PERIOD)
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'products_expiry_date'
        verbose_name = _('products_expiry_date')
        verbose_name_plural = _('products_expiry_dates')
        ordering = ('product',)      
    
    def __unicode__(self): 
        return self.batch_code


class Category(BaseModel):
    shop = models.ForeignKey("main.Shop")
    name = models.CharField(max_length=128,unique=True)
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'products_category'
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        ordering = ('name',)      
    
    def __unicode__(self): 
        return self.name
        

class SubCategory(BaseModel):
    shop = models.ForeignKey("main.Shop")
    category = models.ForeignKey("products.Category")
    name = models.CharField(max_length=128)

    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'products_sub_category'
        verbose_name = _('Sub category')
        verbose_name_plural = _('Sub categories')
        ordering = ('name',)
        
    def __unicode__(self):
        return self.name 


class Brand(BaseModel):
    shop = models.ForeignKey("main.Shop")
    name = models.CharField(max_length=128,unique=True)
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'products_brand'
        verbose_name = _('brand')
        verbose_name_plural = _('brands')
        ordering = ('name',)      
    
    def __unicode__(self): 
        return self.name


class Measurement(BaseModel):
    shop = models.ForeignKey("main.Shop")
    code = models.CharField(max_length=18)
    unit_type = models.CharField(max_length=128, choices=UNIT_TYPES,default="quantity")
    unit_name = models.CharField(max_length=128)
    is_base = models.BooleanField(default=False)
    conversion_factor = models.DecimalField(default=0,decimal_places=4, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    is_system_generated = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'products_measurement'
        verbose_name = _('measurement')
        verbose_name_plural = _('measurements')
        ordering = ('auto_id',) 
        unique_together = (("shop", "code"),)
        
    class Admin:
        list_display = ('code','unit_type','unit_name')
        
    def __unicode__(self):
        return self.unit_name + '(' + self.code +')'  


class ProductAlternativeUnitPrice(models.Model):
    shop = models.ForeignKey("main.Shop")
    product = models.ForeignKey("products.Product")
    unit = models.ForeignKey("products.Measurement")
    cost = models.DecimalField(decimal_places=2, max_digits=15, validators=[MinValueValidator(Decimal('0.00'))])
    price = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'product_alternative_unit_price'
        verbose_name = _('product alternative price')
        verbose_name_plural = _('product alternative prices')
        ordering = ('product',) 
    
    class Admin:
        list_display = ('product','unit','cost','price')
        
    def __unicode__(self):
        return str(self.price)


class Asset(BaseModel):
    shop = models.ForeignKey("main.Shop")  
    name = models.CharField(max_length=128)
    stock = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    available_stock = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    is_deleted = models.BooleanField(default=False)    
    
    class Meta:
        db_table = 'products_asset'
        verbose_name = _('asset')
        verbose_name_plural = _('assets')
        ordering = ('auto_id',) 

    def __unicode__(self):
        return self.name   

    

