from __future__ import unicode_literals
from django.db import models
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from main.models import BaseModel


PURPOSE = (
    # ('cleaning', 'Cleaning'),
    ('from_room', 'From Room'),
    ('to_room', 'To Room'),
    ('damaged', 'Damaged'),
)
ROOMTYPE = (
    ('ac_single','Single A/C'),
    ('non_ac_single','Single Non A/C'),
    ('ac_double','Double A/C'),
    ('non_ac_double','Double Non A/C'),
    ('dormitory','Dormitory'),
    ('double_double','Double Double'),
    ('triple','Triple'),
    ('quad','Quad'),
    ('queen','Queen'),
    ('king','King'),
    ('guest_room','Guest Room'),
    ('duplex','Duplex'),
    ('cabana','Cabana'),
)

class Room(BaseModel):
    shop = models.ForeignKey("main.Shop")
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=128) 
    hsn = models.CharField(max_length=128,blank=True,null=True) 

    category = models.ForeignKey("rooms.RoomCategory",null=True,blank=True,limit_choices_to={'is_deleted': False})
    subcategory = models.ForeignKey("rooms.RoomSubCategory",null=True,blank=True,limit_choices_to={'is_deleted': False}) 

    normal_rate = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    offtime_rate = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    peaktime_rate = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    total_adult = models.PositiveIntegerField(default=1)
    total_child = models.PositiveIntegerField(default=1)

    tax_excluded_price = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    tax_category = models.ForeignKey("finance.TaxCategory",null=True,blank=True,limit_choices_to={'is_deleted': False})   
    tax = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    discount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    room_type = models.CharField(max_length=128,choices=ROOMTYPE,default="ac_single")   
    
    is_deleted = models.BooleanField(default=False)
    is_tax_included = models.BooleanField(default=False)
    is_cleaning = models.BooleanField(default=False)
    is_booked = models.BooleanField(default=False)
    is_vacant = models.BooleanField(default=True)
    
    class Meta:
        db_table = 'rooms_room'
        verbose_name = _('room')
        verbose_name_plural = _('rooms')
        ordering = ('auto_id',)     
        unique_together = (("shop","code"),)   
    
    def __unicode__(self):
        value =  self.name
        if self.category:
            value += " - " + self.category.name
        if self.subcategory:
            value += " - " + self.subcategory.name

        return value        

class RoomStock(models.Model):
    shop = models.ForeignKey("main.Shop")
    room = models.ForeignKey("rooms.Room",blank=True,null=True)
    asset = models.ForeignKey("products.Asset")
    qty = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'rooms_room_stock'
        verbose_name = _('room stock')
        verbose_name_plural = _('room stocks')    
    
    def __unicode__(self): 
        return self.room.name

class CleaningStock(models.Model):
    shop = models.ForeignKey("main.Shop")
    staff = models.ForeignKey("staffs.Staff",blank=True,null=True)
    asset = models.ForeignKey("products.Asset")
    qty = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'rooms_cleaning_stock'
        verbose_name = _('cleaning stock')
        verbose_name_plural = _('cleaning stocks')    
    
    def __unicode__(self): 
        return self.staff.name           

class RoomCategory(BaseModel):
    shop = models.ForeignKey("main.Shop")
    name = models.CharField(max_length=128,unique=True)
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'rooms_category'
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        ordering = ('name',)      
    
    def __unicode__(self): 
        return self.name
        

class RoomSubCategory(BaseModel):
    shop = models.ForeignKey("main.Shop")
    category = models.ForeignKey("rooms.RoomCategory")
    name = models.CharField(max_length=128)

    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'rooms_sub_category'
        verbose_name = _('Sub category')
        verbose_name_plural = _('Sub categories')
        ordering = ('name',)
        
    def __unicode__(self):
        return self.name 


class AssetTransfer(BaseModel):
    time = models.DateTimeField()
    shop = models.ForeignKey("main.Shop")
    room = models.ForeignKey("rooms.Room",blank=True,null=True)
    staff = models.ForeignKey("staffs.Staff",blank=True,null=True)
    purpose = models.CharField(max_length=128, choices=PURPOSE,default="to_room")
    is_deleted = models.BooleanField(default=False)
    
    class meta:
        db_table = 'rooms_asset_transfer'
        verbose_name = _('transfer asset')
        verbose_name_plural = _('transfer assets')
        ordering = ('-a_id',)

    def __unicode__(self):
        return self.purpose


class AssetTransferItem(models.Model):
    asset_transfer = models.ForeignKey("rooms.AssetTransfer")
    asset = models.ForeignKey("products.Asset")
    qty = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    regular_item = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    class meta:
        db_table = 'rooms_asset_transfer_item'
        verbose_name = _('asset transfer item')
        verbose_name_plural = _('asset transfer items')

    def __unicode__(self):
        return self.asset.name


class Cleaning(BaseModel):
    time = models.DateTimeField()
    shop = models.ForeignKey("main.Shop")
    staff = models.ForeignKey("staffs.Staff",blank=True,null=True)
    is_deleted = models.BooleanField(default=False)
    
    class meta:
        db_table = 'rooms_cleaning'
        verbose_name = _('cleaning')
        verbose_name_plural = _('cleanings')
        ordering = ('-a_id',)

    def __unicode__(self):
        return self.a_id


class CleaningItem(models.Model):
    cleaning = models.ForeignKey("rooms.Cleaning")
    staff = models.ForeignKey("staffs.Staff")
    room = models.ForeignKey("rooms.Room")
    
    is_deleted = models.BooleanField(default=False)

    class meta:
        db_table = 'rooms_cleaning_item'
        verbose_name = _('cleaning item')
        verbose_name_plural = _('cleaning items')

    def __unicode__(self):
        return self.room.name

class RoomStatus(models.Model):    
    room = models.ForeignKey("rooms.Room")
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    checkin = models.ForeignKey("bookings.Checkin",limit_choices_to={'is_deleted': False},blank=True,null=True)
    booking = models.ForeignKey("bookings.Booking",limit_choices_to={'is_deleted': False},blank=True,null=True)
    is_booked = models.BooleanField(default=False)
    is_occupied = models.BooleanField(default=False)
    
    is_deleted = models.BooleanField(default=False)

    class meta:
        db_table = 'rooms_room_status'
        verbose_name = _('room status room status')
        verbose_name_plural = _('room statuses')

    def __unicode__(self):
        return self.room.name






  

    

