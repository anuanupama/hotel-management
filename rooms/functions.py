from rooms.models import Room,CleaningStock,RoomStock,RoomStatus
from products.models import Asset
from users.functions import get_current_shop

def room_status(pk,time,status):
    room = Room.objects.get(pk=pk)
    if status == "booked" :
        if RoomStatus.objects.filter(room=room,from_date__gte=time,to_date__lte=time,is_booked=True).exists():
            return True
        else :
            return False
    elif status == "occupied" :
        if RoomStatus.objects.filter(room=room,from_date__gte=time,to_date__lte=time,is_occupied=True).exists():
            return True
        else :
            return False


def room_stock_update(request,room_pk,pk,qty,status):
    room = Room.objects.get(pk=room_pk)
    asset = Asset.objects.get(pk=pk)
    current_shop = get_current_shop(request)
    if status == "increase":
        if RoomStock.objects.filter(is_deleted=False,asset=asset,shop=current_shop,room=room).exists():
            room_stock = RoomStock.objects.get(room=room,asset=asset,shop=current_shop)
            old_stock = room_stock.qty
            room_stock.qty = old_stock + qty
            room_stock.save()
        else:
            RoomStock.objects.create(room=room,asset=asset,shop=current_shop,qty=qty)
    elif status == "decrease":
        if RoomStock.objects.filter(is_deleted=False,room=room,asset=asset,shop=current_shop).exists():
            room_stock = RoomStock.objects.get(room=room,asset=asset,shop=current_shop)
            old_stock = room_stock.qty
            room_stock.qty = old_stock - qty
            room_stock.save()

def cleaning_stock_update(request,staff_pk,pk,qty,status):
    staff = Staff.objects.get(pk=staff_pk)
    asset = Asset.objects.get(pk=pk)
    current_shop = get_current_shop(request)
    if status == "increase":
        if CleaningStock.objects.filter(is_deleted=False,asset=asset,shop=current_shop,staff=staff).exists():
            staff_stock = CleaningStock.objects.get(staff=staff,asset=asset,shop=current_shop)
            old_stock = staff_stock.qty
            staff_stock.qty = old_stock + qty
            staff_stock.save()
        else:
            CleaningStock.objects.create(staff=staff,asset=asset,shop=current_shop,qty=qty)
    elif status == "decrease":
        if CleaningStock.objects.filter(is_deleted=False,staff=staff,asset=asset,shop=current_shop).exists():
            staff_stock = CleaningStock.objects.get(staff=staff,asset=asset,shop=current_shop)
            old_stock = staff_stock.qty
            staff_stock.qty = old_stock - qty
            staff_stock.save()
    