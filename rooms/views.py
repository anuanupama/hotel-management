from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from rooms.models import Room,RoomCategory,RoomSubCategory,Cleaning,CleaningItem,AssetTransferItem,AssetTransfer
from finance.models import TaxCategory
from finance.forms import TaxCategoryForm
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, shop_required, check_account_balance,permissions_required,ajax_required
from main.functions import generate_form_errors, get_auto_id, get_a_id
import datetime
from django.db.models import Q
from dal import autocomplete
from django.views.decorators.http import require_GET
from django.forms.widgets import Select,TextInput
from users.functions import get_current_shop
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from rooms.forms import RoomSubCategoryForm,RoomForm,RoomCategoryForm,AssetTransferForm,AssetTransferItemForm,\
CleaningForm,CleaningItemForm
from django.core import serializers
from decimal import Decimal
import xlrd 
from vendors.models import Vendor
from django.views.decorators.http import require_POST
from django.template.loader import render_to_string
from django.conf import settings
from products.models import Asset
from products.forms import AssetForm
from products.functions import update_asset_available_stock
from rooms.functions import room_stock_update,cleaning_stock_update


class RoomCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = RoomCategory.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) | 
                                 Q(name__istartswith=self.q)
                                )
    
        return items

    def create_object(self, text):
        current_shop = get_current_shop(self.request)
        auto_id = get_auto_id(RoomCategory)
        a_id = get_a_id(RoomCategory,self.request)
        return RoomCategory.objects.create(
            auto_id=auto_id,
            a_id=a_id,
            name=text,
            shop=current_shop,
            creator=self.request.user,
            updator=self.request.user
        )


class RoomSubcategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = RoomSubCategory.objects.filter(is_deleted=False,shop=current_shop)

        category = self.forwarded.get('category', None)

        if category:
            items = items.filter(category=category)
        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) | 
                                 Q(name__istartswith=self.q)
                                )
    
        return items

    def create_object(self, text):
        current_shop = get_current_shop(self.request)
        auto_id = get_auto_id(RoomSubCategory)
        a_id = get_a_id(RoomSubCategory,self.request)
        category_pk = self.request.POST.get('forward[category]')
        if category_pk:
            category = RoomCategory.objects.get(pk=category_pk)
            return RoomSubCategory.objects.create(
                auto_id=auto_id,
                a_id=a_id,
                name=text,
                category=category,
                shop=current_shop,
                creator=self.request.user,
                updator=self.request.user
            )

    
class RoomAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = Room.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) | 
                                 Q(name__istartswith=self.q) | 
                                 Q(code__istartswith=self.q) | 
                                 Q(category__name__istartswith=self.q)
                                )
    
        return items
    
@check_mode
@login_required
@shop_required
def dashboard(request):
    return HttpResponseRedirect(reverse('rooms:rooms'))


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_product'])
def create(request):    
    current_shop = get_current_shop(request)    
    if request.method == 'POST':
        form = RoomForm(request.POST)        
        if form.is_valid():             
            auto_id = get_auto_id(Room)
            a_id = get_a_id(Room,request)
            error_messages = ''            
            code = form.cleaned_data['code'].upper()
            if Room.objects.filter(code=code,shop=current_shop,is_deleted=False).exists():
                error_messages += "Duplicate Code %s" % code

            if not error_messages:
                name = form.cleaned_data['name']
                name = name.capitalize()
                normal_rate = form.cleaned_data['normal_rate']
                tax_category = form.cleaned_data['tax_category']
                tax_instance = None
                tax = 0
                
                if tax_category:
                    tax_instance = get_object_or_404(TaxCategory.objects.filter(is_deleted=False,pk=tax_category.pk))
                    tax = tax_instance.tax

                is_tax_included = form.cleaned_data['is_tax_included']

                if is_tax_included:
                    tax_excluded_price = (100*normal_rate)/(100+tax)
                else :
                    tax_excluded_price = normal_rate
                    normal_rate = tax_excluded_price + (tax_excluded_price*tax)/100

                #create product
                data = form.save(commit=False)
                data.creator = request.user
                data.updator = request.user
                data.auto_id = auto_id
                data.shop = current_shop
                data.name = name
                data.code = code
                data.tax_excluded_price = tax_excluded_price
                data.is_tax_included = is_tax_included
                data.normal_rate = normal_rate
                data.a_id = a_id 
                data.tax = tax
                data.save() 
                response_data = {
                    "status" : "true",
                    "title" : "Successfully Created",
                    "message" : "Room created successfully.",
                    "redirect" : "true",
                    "redirect_url" : reverse('rooms:room',kwargs={'pk':data.pk})
                } 
            else:            
        
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Duplicate Entry",
                    "message" : error_messages
                }   
        
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')  
        
        else:            
            message = generate_form_errors(form,formset=False) 
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:               
        product_form = RoomForm(initial={'shop':current_shop})
        context = {
            "title" : "Create Room ",
            "form" : product_form,
            "url" : reverse('rooms:create'),
            'redirect':True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_create_page" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'rooms/entry.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_room'])
def rooms(request):
    current_shop = get_current_shop(request)
    # for i in range(54):
    #     auto_id = get_auto_id(Room)
    #     a_id = get_a_id(Room,request)
    #     name = "R10"+str(i)
    #     code = "R10"+str(i)
    #     Room(
    #         creator = request.user,
    #         updator = request.user,
    #         auto_id = auto_id,
    #         shop = current_shop,
    #         name = name,
    #         code = code,
    #         tax_excluded_price = 0,
    #         normal_rate = 100,
    #         a_id = a_id ,
    #         tax = 0,
    #         offtime_rate = 100,
    #         peaktime_rate = 100,
    #         total_adult = 1,
    #         total_child = 1,
    #         discount = 0,
    #         room_type = "ac_single",
    #         is_deleted =False,
    #         is_tax_included =False,
    #         is_cleaning =False,
    #         is_booked =False,
    #         is_vacant = True,
    #     ).save()
    instances = Room.objects.filter(is_deleted=False,shop=current_shop)
    categories = RoomCategory.objects.filter(shop=current_shop,is_deleted=False)
    subcategories = RoomSubCategory.objects.filter(shop=current_shop,is_deleted=False)
    title = "Rooms"
    #filter block
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(name__icontains=query) | Q(code__icontains=query))
        title = "Rooms - %s" %query
        
    category = request.GET.get('category')
    subcategory = request.GET.get('subcategory')
    if category:
        instances = instances.filter(category=category)
        if RoomCategory.objects.filter(pk=category).exists():
            cat = RoomCategory.objects.get(pk=category)
            title = "Rooms - %s" %cat.name

    if subcategory:
        instances = instances.filter(subcategory=subcategory)
        if RoomSubCategory.objects.filter(pk=subcategory).exists():
            sub = RoomSubCategory.objects.get(pk=subcategory)
            title = "Rooms - %s" %sub.name

    context = {
        "instances" : instances,
        'title' : title,
        "categories" : categories,
        "subcategories" : subcategories,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'rooms/rooms.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_room'])
def room(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Room.objects.filter(pk=pk,is_deleted=False,shop=current_shop))    
    context = {
        "instance" : instance,
        "title" : "Room : " + instance.name,
        "single_page" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'rooms/room.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_room'])
def edit(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Room.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    if request.method == 'POST':
        response_data = {}
        form = RoomForm(request.POST,instance=instance)        
        if form.is_valid():  
            name = form.cleaned_data['name']
            name=name.capitalize()    
            normal_rate = form.cleaned_data['normal_rate']
            discount = form.cleaned_data['discount']
            tax_category = form.cleaned_data['tax_category']
            if tax_category :
                tax_instance = get_object_or_404(TaxCategory.objects.filter(is_deleted=False,pk=tax_category.pk))
                tax = tax_instance.tax
            else:
                tax = 0
            is_tax_included = form.cleaned_data['is_tax_included']

            if is_tax_included:
                tax_excluded_price = (100*normal_rate)/(100+tax)
            else :
                tax_excluded_price = normal_rate
                normal_rate = tax_excluded_price + (tax_excluded_price*tax)/100
     
            #update room
            data = form.save(commit=False)
            data.updator = request.user
            data.tax_excluded_price = tax_excluded_price
            data.is_tax_included = is_tax_included
            data.normal_rate = normal_rate
            data.date_updated = datetime.datetime.now()
            data.name = name
            data.tax = tax
            data.save() 
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Room Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('rooms:room',kwargs={'pk':data.pk})
            }   
        else:
            message = {}
            message = generate_form_errors(form,formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        room_form = RoomForm(instance=instance,initial={'shop':current_shop}) 
        room_form.fields['subcategory'].queryset = RoomSubCategory.objects.filter(shop=current_shop,category=instance.category,is_deleted=False)        
        
        context = {
            "form" : room_form,
            "title" : "Edit Room : " + instance.name,
            "instance" : instance,
            "url" : reverse('rooms:edit',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'rooms/entry.html', context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_room'],allow_self=True,model=Room)
def delete(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Room.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    Room.objects.filter(pk=pk).update(is_deleted=True,code=instance.code + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Room Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('rooms:rooms')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_room'])
def delete_selected_rooms(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Room.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
            Room.objects.filter(pk=pk).update(is_deleted=True,code=instance.code + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Room(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('rooms:rooms')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
def get_room(request):
    current_shop = get_current_shop(request)
    pk = request.GET.get('id')
    room_exists = False     
    if Room.objects.filter(pk=pk,shop=current_shop).exists():
        item = Room.objects.get(pk=pk)
        room_exists = True
    
    if room_exists:        

        response_data = {
            "status" : "true",
            'pk' : str(item.pk),
            'code' : item.code,
            'name' : item.name,
            
            'normal_rate' : str(tax_excluded_price),   
            'stock' : str(item.stock),    
            'tax' : str(item.tax),
            'stock' : str(item.stock),
            'discount' : str(item.discount),         
            'is_deleted' : item.is_deleted, 
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Room not found"
        }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

@check_mode
@login_required
@shop_required
@permissions_required(['can_create_category'])
def create_category(request):    
    current_shop = get_current_shop(request)
    
    if request.method == 'POST':
        form = RoomCategoryForm(request.POST)
        
        if form.is_valid(): 
            
            auto_id = get_auto_id(RoomCategory)
            a_id = get_a_id(RoomCategory,request)
            
            #create category
            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.shop = current_shop
            data.a_id = a_id
            data.save()    
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Category created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('rooms:category',kwargs={'pk':data.pk})
            }   
        
        else:            
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        form = RoomCategoryForm()
        context = {
            "title" : "Create Category ",
            "form" : form,
            "url" : reverse('rooms:create_category'),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'rooms/category_entry.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_category'])
def categories(request):
    current_shop = get_current_shop(request)
    instances = RoomCategory.objects.filter(is_deleted=False,shop=current_shop)
    title = "Categories"
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(name__icontains=query))
        title = "Categories - %s" %query
        
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'rooms/categories.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_category'])
def category(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(RoomCategory.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    context = {
        "instance" : instance,
        "title" : "Category : " + instance.name,
        "single_page" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'rooms/category.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_category'])
def edit_category(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(RoomCategory.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    
    if request.method == 'POST':
        response_data = {}
        form = RoomCategoryForm(request.POST,instance=instance)
        
        if form.is_valid():      
                   
            #update category
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()            
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Category Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('rooms:category',kwargs={'pk':data.pk})
            }   
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = RoomCategoryForm(instance=instance)
        
        context = {
            "form" : form,
            "title" : "Edit Category : " + instance.name,
            "instance" : instance,
            "url" : reverse('rooms:edit_category',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'rooms/category_entry.html', context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_category'])
def delete_category(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(RoomCategory.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    RoomCategory.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Category Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('rooms:categories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_category'])
def delete_selected_categories(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(RoomCategory.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
            RoomCategory.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Categories Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('rooms:categories')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

@check_mode
@login_required
@shop_required
@permissions_required(['can_create_subcategory'])
def create_subcategory(request):
    current_shop = get_current_shop(request)
    instances = RoomSubCategory.objects.filter(is_deleted=False, shop=current_shop)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query)|Q(category__name=query))

    if request.method == 'POST':
        form = RoomSubCategoryForm(request.POST)

        if form.is_valid():
            auto_id = get_auto_id(RoomSubCategory)
            a_id = get_a_id(RoomSubCategory,request)

            data = form.save(commit=False)
            data.auto_id = auto_id
            data.a_id = a_id
            data.creator = request.user
            data.updator = request.user
            data.shop = current_shop
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "RoomSubCategory created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('rooms:create_subcategory')
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = RoomSubCategoryForm()

        context = {
            "title" : "Create RoomSubCategory",
            "form" : form,
            "redirect" : True,
            "url" : reverse('rooms:create_subcategory'),
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "rooms" : True,
            "rooms" : True,
        }
        return render(request,'rooms/subcategory_entry.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_subcategory'])
def subcategories(request):
    current_shop = get_current_shop(request)
    instances = RoomSubCategory.objects.filter(is_deleted=False, shop=current_shop)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query)|Q(category__name=query))

    context = {
        'instances': instances,
        "title" : 'subcategories',
        "is_need_select_picker" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_popup_box" : True,
        "is_need_animations": True,
        "rooms" : True,
        "rooms" : True,
        }
    return render(request, "rooms/subcategories.html", context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_subcategory'])
def edit_subcategory(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(RoomSubCategory.objects.filter(pk=pk, is_deleted=False, shop=current_shop))
    query = request.GET.get("q")
    if query:
        instance = instance.filter(Q(name__icontains=query)|Q(category__name=query))

    if request.method == "POST":
        form = RoomSubCategoryForm(request.POST, instance=instance)

        if form.is_valid():
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "RoomSubCategory updated successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('rooms:subcategories')
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = RoomSubCategoryForm(instance=instance)

        context = {
            "instance" : instance,
            "title" : "Edit RoomSubCategory :" + instance.name,
            "form" : form,
            "redirect" : True,
            "url" : reverse('rooms:edit_subcategory', kwargs={'pk':instance.pk}),
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "rooms" : True,
            
        }
        return render(request,'rooms/subcategory_entry.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_subcategory'])
def subcategory(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(RoomSubCategory.objects.filter(pk=pk, is_deleted=False, shop=current_shop))
    query = request.GET.get("q")
    if query:
        instance = instance.filter(Q(name__icontains=query)|Q(category__name=query))

    context = {
        'instance': instance,
        'title':'RoomSubCategory',
        "is_need_select_picker" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_popup_box" : True,
        "rooms" : True,
    }
    return render(request, "rooms/subcategory.html", context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_subcategory'])
def delete_subcategory(request,pk):
    current_shop = get_current_shop(request)
    RoomSubCategory.objects.filter(pk=pk, shop=current_shop).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "RoomSubCategory deleted successfully.",
        "redirect" : "true",
        "redirect_url" : reverse('rooms:subcategories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_subcategory'])
def delete_selected_subcategories(request):
    current_shop = get_current_shop(request)

    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(RoomSubCategory.objects.filter(pk=pk, is_deleted=False, shop=current_shop))
            RoomSubCategory.objects.filter(pk=pk, shop=current_shop).update(is_deleted=True, name=instance.name + "_deleted_" + str(instance.auto_id))

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Sub Category Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('rooms:subcategories')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    
@login_required 
def get_product_sub_categories(request):
    pk = request.GET.get('pk')
    instances = RoomSubCategory.objects.filter(category__pk=pk,is_deleted=False)
    
    json_models = serializers.serialize("json", instances)
    return HttpResponse(json_models, content_type="application/javascript")

@check_mode
@login_required
@shop_required
@permissions_required(['can_create_asset'])
def create_asset(request): 
    current_shop = get_current_shop(request)    
    if request.method == "POST":
        form = AssetForm(request.POST)
            
        if form.is_valid():
            
            auto_id = get_auto_id(Asset)
            a_id = get_a_id(Asset,request)
              
            #create asset
            stock = form.cleaned_data['stock']
            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.a_id = a_id
            data.available_stock = stock
            data.shop = current_shop
            data.save()
            
            response_data = {
                "status" : "true",
                "title" : "Succesfully Created",
                "redirect" : "true",
                "redirect_url" : reverse('products:assets'),
                "message" : "Asset Successfully Created."
            }
        else:
            message = generate_form_errors(form,formset=False)        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }            
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = AssetForm()        
        context = {
            "form" : form,
            "title" : "Create Asset",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_animations": True,
            "is_need_datetime_picker" : True,
            
        }
        return render(request, 'rooms/entry_asset.html', context)
    
    
@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_asset'])
def edit_asset(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Asset.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    if request.method == "POST":
        response_data = {}  
        form = AssetForm(request.POST,instance=instance)
        
        if form.is_valid(): 
            
            #update asset
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()
            
            response_data = {
                "status" : "true",
                "title" : "Succesfully Updated",
                "redirect" : "true",
                "redirect_url" : reverse('rooms:asset', kwargs = {'pk' :pk}),
                "message" : "Asset Successfully Updated."
            }
        else:
            message = generate_form_errors(form,formset=False)        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : form.errors
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = AssetForm(instance=instance)
        
        context = {
            "form" : form,
            "title" : "Edit Asset : " + instance.name,
            "instance" : instance,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_animations": True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'rooms/entry_asset.html', context)

    
@check_mode
@login_required
@shop_required 
@permissions_required(['can_view_asset'])     
def assets(request):
    current_shop = get_current_shop(request)
    instances = Asset.objects.filter(is_deleted=False,shop=current_shop)

    title = "Assets"
    
    #filter by query
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query))
        
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'rooms/assets.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_asset'])
def asset(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Asset.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    context = {
        "instance" : instance,
        "title" : "Asset : " + instance.name,
        "single_page" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'rooms/asset.html',context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_asset'])
def delete_asset(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Asset.objects.filter(pk=pk,shop=current_shop))
    Asset.objects.filter(pk=pk).update(is_deleted=True)
    
    response_data = {
        "status" : "true",
        "title" : "Succesfully Deleted",
        "redirect" : "true",
        "redirect_url" : reverse('rooms:assets'),
        "message" : "Asset Successfully Deleted."
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
def delete_selected_assets(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Asset.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
            Asset.objects.filter(pk=pk,shop=current_shop).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Asset(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('rooms:assets')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some asset first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_asset_transfer'])
def create_asset_transfer(request):
    AssetTransferItemFormset = formset_factory(AssetTransferItemForm, extra=1)
    current_shop = get_current_shop(request)

    if request.method == 'POST':
        form = AssetTransferForm(request.POST)
        asset_transfer_item_formset = AssetTransferItemFormset(request.POST,prefix='asset_transfer_item_formset')

        for item in asset_transfer_item_formset:
            item.fields['asset'].queryset = Asset.objects.filter(shop=current_shop,is_deleted=False)

        if form.is_valid() and asset_transfer_item_formset.is_valid() :
            is_ok = True
            message = ""  
            for item in asset_transfer_item_formset:
                asset = item.cleaned_data['asset']
                qty = item.cleaned_data['qty']

                stock = asset.available_stock
                if qty > stock:
                    is_ok = False
                    message += "%s has only %s in asset, " %(asset.name,str(stock))

            if is_ok:

                purpose = form.cleaned_data['purpose'] 
                auto_id = get_auto_id(AssetTransfer)
                a_id = get_a_id(AssetTransfer,request)
                date = form.cleaned_data['time']
                data = form.save(commit=False)
                if purpose == 'from_room' or purpose == 'to_room':
                    room = form.cleaned_data['room']
                    data.room = room
                elif purpose == 'cleaning':
                    staff = form.cleaned_data['staff']
                    data.staff = staff
                data.auto_id = auto_id
                data.a_id = a_id
                data.shop = current_shop
                data.creator = request.user
                data.updator = request.user
                data.save()

                for f in asset_transfer_item_formset:
                    asset = f.cleaned_data['asset']
                    qty = f.cleaned_data['qty']
                    regular_item = f.cleaned_data['regular_item']
                    AssetTransferItem(
                        asset_transfer = data,
                        asset = asset,
                        qty = qty,
                    ).save()
                    if not regular_item :
                        if purpose == 'to_room':
                            room_stock_update(request,room.pk,asset.pk,qty,"increase")
                        if purpose == 'from_room':
                            room_stock_update(request,room.pk,asset.pk,qty,"decrease")
                        elif purpose == 'cleaning':
                            cleaning_stock_update(request,staff.pk,asset.pk,qty,"increase")

                    update_asset_available_stock(asset.pk,qty,"decrease")

                response_data = {
                    "status" : "true",
                    "title" : "Successfully Created",
                    "message" : "Asset Transfer created successfully.",
                    "redirect" : "true",
                    "redirect_url" : reverse('rooms:asset_transfer',kwargs={"pk":data.pk})
                }
            else:
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Out of Asset",
                    "message" : message
                }

        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = AssetTransferForm()
        asset_transfer_item_formset = AssetTransferItemFormset(prefix='asset_transfer_item_formset')
        for item in asset_transfer_item_formset:
            item.fields['asset'].queryset = Asset.objects.filter(shop=current_shop,is_deleted=False)
            
        context = {
            "asset_transfer_item_formset" : asset_transfer_item_formset,
            "title" : "Create asset transfer",
            "form" : form,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "asset_transfers" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "redirect" : True,
            "is_need_animations": True,
            "block_payment_form_media" : True,
            "purchase_page" : True
        }
        return render(request,'rooms/entry_asset_transfer.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_asset_transfer'])
def asset_transfers(request):
    current_shop = get_current_shop(request)
    instances = AssetTransfer.objects.filter(is_deleted=False,shop=current_shop)
    today = datetime.date.today()
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    payment = request.GET.get('payment')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
        except ValueError:
            date_error = "yes"

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)

    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)
        except ValueError:
            date_error = "yes"

        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])

    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)

    context = {
        'instances': instances,
        "title" : 'Asset Transfers',

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "asset_transfers" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request, "rooms/asset_transfers.html", context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_asset_transfer'])
def edit_asset_transfer(request, pk):
    current_shop = get_current_shop(request)
    instance = AssetTransfer.objects.get(pk=pk,is_deleted=False,shop=current_shop)
    if AssetTransferItem.objects.filter(asset_transfer=instance).exists():
        extra = 0
    else:
        extra= 1
    AssetTransferItemFormset = inlineformset_factory(
        AssetTransfer,
        AssetTransferItem,
        can_delete = True,
        extra = extra,
        exclude = ['creator','updator','auto_id','is_deleted','a_id','asset_transfer'],
        widgets = {
            'asset': autocomplete.ModelSelect2(url='products:asset_autocomplete', attrs={'data-placeholder': 'Asset', 'data-minimum-input-length': 1},),
            'qty' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Qty'}),            
            }
        )

    if request.method == "POST":
        form = AssetTransferForm(request.POST, instance=instance)
        asset_transfer_item_formset = AssetTransferItemFormset(request.POST,prefix='asset_transfer_item_formset',instance=instance)

        if form.is_valid() and asset_transfer_item_formset.is_valid():
            items = {}
            purpose = form.cleaned_data['purpose']

            for f in asset_transfer_item_formset:
                if f not in asset_transfer_item_formset.deleted_forms:
                    asset = f.cleaned_data['asset']
                    qty = f.cleaned_data['qty'] 
                    regular_item = f.cleaned_data['regular_item']                    
                    if str(asset.pk) in items:
                        q = items[str(asset.pk)]["qty"]
                        items[str(asset.pk)]["qty"] = q + qty
                    else:
                        dic = {
                            "qty" : qty,
                            "regular_item" : regular_item
                        }
                        items[str(asset.pk)] = dic

            #update asset_transfer

            data = form.save(commit=False)
            if purpose == 'from_room' or purpose == 'to_room':
                room = form.cleaned_data['room']
                data.room = room
            if purpose == 'cleaning':
                staff = form.cleaned_data['staff']
                data.staff = staff
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            previous_asset_transfer_items = AssetTransferItem.objects.filter(asset_transfer=instance)
            for p in previous_asset_transfer_items:
                qty = p.qty
                purpose = instance.purpose
                if purpose == 'from_room' or purpose == 'to_room':
                    room = instance.room
                elif purpose == 'cleaning':
                    staff = instance.staff
                if not p.regular_item :
                    if purpose == 'to_room':
                        room_stock_update(request,room.pk,asset.pk,qty,"decrease")
                    if purpose == 'from_room':
                        room_stock_update(request,room.pk,asset.pk,qty,"increase")
                    elif purpose == 'cleaning':
                        cleaning_stock_update(request,staff.pk,asset.pk,qty,"decrease")
                update_asset_available_stock(p.asset.pk,qty,"increase")
            previous_asset_transfer_items.delete()

            total = 0
            #save items
            for key, value in items.iteritems():
                asset = Asset.objects.get(pk=key)
                qty = value["qty"]
                regular_item = value["regular_item"]                

                AssetTransferItem(
                    asset_transfer = data,
                    asset = asset,
                    qty = qty,
                ).save()
                if not regular_item :
                    if purpose == 'to_room':
                        room_stock_update(request,room.pk,asset.pk,qty,"increase")
                    if purpose == 'from_room':
                        room_stock_update(request,room.pk,asset.pk,qty,"decrease")
                    elif purpose == 'cleaning':
                        cleaning_stock_update(request,staff.pk,asset.pk,qty,"increase")

                update_asset_available_stock(asset.pk,qty,"decrease")

            response_data = {
                "status": "true",
                "title": "Successfully Updated",
                "message": "Asset transfer updated successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('rooms:asset_transfer',kwargs={"pk":instance.pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = AssetTransferForm(instance=instance)
        asset_transfer_item_formset = AssetTransferItemFormset(prefix='asset_transfer_item_formset',instance=instance)
        for item in asset_transfer_item_formset:
            item.fields['asset'].queryset = Asset.objects.filter(shop=current_shop)
            
        context = {
            "asset_transfer_item_formset" : asset_transfer_item_formset,
            "title" : "Edit asset transfer",
            "form" : form,
            "instance" : instance,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "asset_transfers" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            'redirect':True,
            "block_payment_form_media" : True,
        }
        return render(request,'rooms/entry_asset_transfer.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_asset_transfer'])
def asset_transfer(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(AssetTransfer.objects.filter(pk=pk, is_deleted=False, shop=current_shop))
    asset_transfer_items = AssetTransferItem.objects.filter(asset_transfer=instance)
    query = request.GET.get("q")
    if query:
        instance = instance.filter(Q(time__icontains=query)|Q(distributor__name__icontains=query))


    context = {
        'instance': instance,
        "asset_transfer_items" : asset_transfer_items,
        'title':'Asset Transfers',
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_select_picker" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_popup_box" : True,
        "asset_transfers" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, "rooms/asset_transfer.html", context)


def delete_asset_transfer_fun(request,instance):

    asset_transfer_items = AssetTransferItem.objects.filter(asset_transfer=instance)
    for p in asset_transfer_items:
        qty = p.qty
        unit = p.unit
        exact_qty = get_exact_qty(qty,unit)
        update_sock(p.product.pk,exact_qty,"increase")
        distributor_asset_update(request,instance.distributor.pk,p.product.pk,exact_qty,"decrease")

    instance.is_deleted=True
    instance.save()


@check_mode
@ajax_required
@login_required
@permissions_required(['can_delete_asset_transfer'])
def delete_asset_transfer(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(AssetTransfer.objects.filter(pk=pk,is_deleted=False,shop=current_shop))

    delete_asset_transfer_fun(request,instance)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Asset Transfer deleted successfully.",
        "redirect" : "true",
        "redirect_url" : reverse('rooms:asset_transfers')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@permissions_required(['can_delete_asset_transfer'])
def delete_selected_asset_transfers(request):
    current_shop = get_current_shop(request)

    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(AssetTransfer.objects.filter(pk=pk,shop=current_shop))
            delete_asset_transfer_fun(request,instance)

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Asset Transfer Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('rooms:asset_transfers')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_cleaning'])
def cleaning_status(request):
    current_shop = get_current_shop(request)
    instances = Room.objects.filter(is_deleted=False,shop=current_shop,is_cleaning=True)
    
    context = {
        "instances" : instances,
        'title' : 'Room cleaning',

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'rooms/cleaning_status.html',context) 


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_clean_room'],allow_self=True,model=Room)
def cleaning(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Room.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    Room.objects.filter(pk=pk).update(is_cleaning=True)    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Room Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('rooms:cleaning_status')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_clean_room'],allow_self=True,model=Room)
def finish_cleaning(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Room.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    Room.objects.filter(pk=pk).update(is_cleaning=False)
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Cleaned",
        "message" : "Room Successfully Cleaned.",
        "redirect" : "true",
        "redirect_url" : reverse('rooms:cleaning_status')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_cleaning'])
def create_cleaning(request):
    CleaningItemFormset = formset_factory(CleaningItemForm, extra=1)
    current_shop = get_current_shop(request)

    if request.method == 'POST':
        form = CleaningForm(request.POST)
        cleaning_item_formset = CleaningItemFormset(request.POST,prefix='cleaning_item_formset')
        
        if form.is_valid() and cleaning_item_formset.is_valid() :
            message = ""   
            auto_id = get_auto_id(Cleaning)
            a_id = get_a_id(Cleaning,request)
            date = form.cleaned_data['time']
            data = form.save(commit=False)            
            data.auto_id = auto_id
            data.a_id = a_id
            data.shop = current_shop
            data.creator = request.user
            data.updator = request.user
            data.save()
            for item in cleaning_item_formset:
                room = item.cleaned_data['room']
                staff = item.cleaned_data['staff']
                CleaningItem(
                    cleaning = data,
                    room = room,
                    staff = staff,
                ).save()
                room.is_cleaning = True
                room.save()   

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Cleaning created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('rooms:cleaning',kwargs={"pk":data.pk})
            }          

        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = CleaningForm()

        # initial = []
        # rooms = Room.objects.filter(is_deleted=False,is_cleaning=False)
        # for room in rooms:
        #     cleaning_dict = {
        #         'room': room,
        #     }
        #     initial.append(cleaning_dict)        
        cleaning_item_formset = CleaningItemFormset(prefix='cleaning_item_formset')
            
        context = {
            "cleaning_item_formset" : cleaning_item_formset,
            "title" : "Create Cleaning",
            "form" : form,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "cleanings" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "redirect" : True,
            "is_need_animations": True,
            "block_payment_form_media" : True,
            "purchase_page" : True
        }
        return render(request,'rooms/entry_cleaning.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_cleaning'])
def cleanings(request):
    current_shop = get_current_shop(request)
    instances = Cleaning.objects.filter(is_deleted=False,shop=current_shop)
    today = datetime.date.today()
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    payment = request.GET.get('payment')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
        except ValueError:
            date_error = "yes"

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)

    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)
        except ValueError:
            date_error = "yes"

        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])

    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)

    context = {
        'instances': instances,
        "title" : 'Cleanings',

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "cleanings" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request, "rooms/cleanings.html", context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_cleaning'])
def edit_cleaning(request, pk):
    current_shop = get_current_shop(request)
    instance = Cleaning.objects.get(pk=pk,is_deleted=False,shop=current_shop)
    if CleaningItem.objects.filter(cleaning=instance).exists():
        extra = 0
    else:
        extra= 1
    CleaningItemFormset = inlineformset_factory(
        Cleaning,
        CleaningItem,
        can_delete = True,
        extra = extra,
         exclude = ['creator','updator','auto_id','is_deleted','a_id','cleaning'],
        widgets = {
            'room': autocomplete.ModelSelect2(url='rooms:room_autocomplete', attrs={'data-placeholder': 'Room', 'data-minimum-input-length': 1},),
            'staff': autocomplete.ModelSelect2(url='staffs:staffs_autocomplete', attrs={'data-placeholder': 'Staff', 'data-minimum-input-length': 1},),           
            }
        )

    if request.method == "POST":
        form = CleaningForm(request.POST, instance=instance)
        cleaning_item_formset = CleaningItemFormset(request.POST,prefix='cleaning_item_formset',instance=instance)

        if form.is_valid() and cleaning_item_formset.is_valid():
            items = {}
            #update cleaning

            data = form.save(commit=False)            
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            previous_cleaning_items = CleaningItem.objects.filter(cleaning=instance)
            for p in previous_cleaning_items:
                room = p.room
                room.is_cleaning=False
                room.save()                
            previous_cleaning_items.delete()

            total = 0
            #save items
            for f in cleaning_item_formset:
                staff = f.cleaned_data['staff']
                room = f.cleaned_data['room']               

                CleaningItem(
                    cleaning = data,
                    staff = staff,
                    room = room,
                ).save() 
                room.is_cleaning = True
                room.save()               
            response_data = {
                "status": "true",
                "title": "Successfully Updated",
                "message": "Cleaning updated successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('rooms:cleaning',kwargs={"pk":instance.pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = CleaningForm(instance=instance)
        cleaning_item_formset = CleaningItemFormset(prefix='cleaning_item_formset',instance=instance)
        context = {
            "cleaning_item_formset" : cleaning_item_formset,
            "title" : "Edit Cleaning",
            "form" : form,
            "instance" : instance,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "cleanings" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "redirect" : True,
            "is_need_animations": True,
            "block_payment_form_media" : True,
            "purchase_page" : True
        }
        return render(request,'rooms/entry_cleaning.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_cleaning'])
def cleaning(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Cleaning.objects.filter(pk=pk, is_deleted=False, shop=current_shop))
    cleaning_items = CleaningItem.objects.filter(cleaning=instance)
    query = request.GET.get("q")
    if query:
        instance = instance.filter(Q(time__icontains=query)|Q(distributor__name__icontains=query))


    context = {
        'instance': instance,
        "cleaning_items" : cleaning_items,
        'title':'Cleaning',
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_select_picker" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_popup_box" : True,
        "cleanings" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, "rooms/cleaning.html", context)


def delete_cleaning_fun(request,instance):

    cleaning_items = CleaningItem.objects.filter(cleaning=instance)
    for p in cleaning_items:
        room = p.room
        room.is_cleaning = False
        room.save()

    instance.is_deleted=True
    instance.save()


@check_mode
@ajax_required
@login_required
@permissions_required(['can_delete_cleaning'])
def delete_cleaning(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Cleaning.objects.filter(pk=pk,is_deleted=False,shop=current_shop))

    delete_cleaning_fun(request,instance)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Cleaning deleted successfully.",
        "redirect" : "true",
        "redirect_url" : reverse('rooms:cleanings')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@permissions_required(['can_delete_cleaning'])
def delete_selected_cleanings(request):
    current_shop = get_current_shop(request)

    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(Cleaning.objects.filter(pk=pk,shop=current_shop))
            delete_cleaning_fun(request,instance)

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Cleaning Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('rooms:cleanings')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')