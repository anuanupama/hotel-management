from django import forms
from django.forms.widgets import TextInput, Textarea, HiddenInput, Select
from rooms.models import Room,RoomCategory,RoomSubCategory,AssetTransfer,AssetTransferItem,Cleaning,CleaningItem
from dal import autocomplete
from django.utils.translation import ugettext_lazy as _


class RoomForm(forms.ModelForm):
    
    class Meta:
        model = Room
        exclude = ['creator','updator','auto_id','is_deleted','a_id','tax_excluded_price','tax','is_cleaning','is_booked','is_vacant']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
            'code': TextInput(attrs={'class': 'required form-control','placeholder' : 'Code'}),
            'hsn': TextInput(attrs={'class': 'form-control','placeholder' : 'HSN'}),
            
            'category' : autocomplete.ModelSelect2(url='rooms:room_category_autocomplete',attrs={'data-placeholder': 'Category','data-minimum-input-length': 1},),
            'subcategory': autocomplete.ModelSelect2(url='rooms:room_subcategory_autocomplete',forward=['category'] ,attrs={'data-placeholder': 'Sub Category', 'data-minimum-input-length': 1},),
            'tax_category':autocomplete.ModelSelect2(url='finance:tax_category_autocomplete', attrs={'data-placeholder': 'Tax Category', 'data-minimum-input-length': 1},),
            
            'normal_rate': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Normal Rate'}),
            'offtime_rate' : TextInput(attrs={'class': 'number required form-control','placeholder' : 'Offtime Rate'}),
            'peaktime_rate': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Peaktime Rate'}),
            'tax_excluded_price': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Tax Excluded Price'}),            
            'discount': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Discount'}),

            'total_adult' : TextInput(attrs={'class': 'number required form-control','placeholder' : 'Total Adult'}),
            'total_child' : TextInput(attrs={'class': 'number required form-control','placeholder' : 'Total Child'}),
            'shop': HiddenInput(),
            'room_type' : Select(attrs={'class': 'required form-control selectpicker'}),
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'code' : {
                'required' : _("Code field is required."),
            },            
            'normal_rate' : {
                'required' : _("Normal Rate field is required."),
            },
            'offtime_rate' : {
                'required' : _("Offtime Rate Price field is required."),
            },
            'peaktime_rate' : {
                'required' : _("Peaktime Rate field is required."),
            },
            'tax_excluded_price' : {
                'required' : _(" Tax Excluded Price field is required."),
            },
            'tax_category' : {
                'required' : _("Tax category field is required."),
            },
            'discount' : {
                'required' : _("Discount field is required."),
            }
        }
        
        help_texts = {
            'tax' : 'Tax in Percentage',
            'discount' : 'Discount in Rupees',
        } 

class RoomCategoryForm(forms.ModelForm):
    
    class Meta:
        model = RoomCategory
        exclude = ['shop','creator','updator','auto_id','is_deleted','a_id']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            }
        }


class RoomSubCategoryForm(forms.ModelForm):

    class Meta:
        model = RoomSubCategory
        exclude = ['creator','updator','auto_id','is_deleted','shop','a_id']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
            'category': autocomplete.ModelSelect2(url='products:category_autocomplete', attrs={'data-placeholder': 'Category','class':'form-control required', 'data-minimum-input-length': 1}),
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'category' : {
                'required' : _("Category field is required."),
            },
        }
        

class FileForm(forms.Form):
    file = forms.FileField()


class AssetTransferForm(forms.ModelForm):

    class Meta:
        model = AssetTransfer
        exclude = ['creator','updator','auto_id','is_deleted','a_id','shop']
        widgets = {
            'time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'Time'}),
            'room': autocomplete.ModelSelect2(url='rooms:room_autocomplete', attrs={'data-placeholder': 'Room', 'data-minimum-input-length': 1},),
            'staff': autocomplete.ModelSelect2(url='staffs:staffs_autocomplete', attrs={'data-placeholder': 'Staff', 'data-minimum-input-length': 1},),
            'purpose' : Select(attrs={'class': 'required form-control selectpicker'}),
        }

        error_messages = {
            'time' : {
                'required' : _("Time field is required."),
            },
            'room' : {
                'required' : _("Room field is required."),
            },
            'staff' : {
                'required' : _("Staff field is required."),
            },
            'purpose' : {
                'required' : _("Purpose field is required."),
            },

        }


class AssetTransferItemForm(forms.ModelForm):

    class Meta:
        model = AssetTransferItem
        exclude = ['creator','updator','auto_id','is_deleted','a_id','asset_transfer']
        widgets = {
            'asset': autocomplete.ModelSelect2(url='products:asset_autocomplete', attrs={'data-placeholder': 'Asset', 'data-minimum-input-length': 1},),
            'qty' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Qty'}),            
        }
        error_messages = {
            'asset' : {
                'required' : _("Asset field is required."),
            },
            'qty' : {
                'required' : _("Qty field is required."),
            },  
        }


class CleaningForm(forms.ModelForm):

    class Meta:
        model = Cleaning
        exclude = ['creator','updator','auto_id','is_deleted','a_id','shop']
        widgets = {
            'time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'Time'}),
            'staff': autocomplete.ModelSelect2(url='staffs:staffs_autocomplete', attrs={'data-placeholder': 'Staff', 'data-minimum-input-length': 1},),
        }

        error_messages = {
            'time' : {
                'required' : _("Time field is required."),
            },
            'staff' : {
                'required' : _("Staff field is required."),
            },
        }


class CleaningItemForm(forms.ModelForm):

    class Meta:
        model = CleaningItem
        exclude = ['creator','updator','auto_id','is_deleted','a_id','cleaning']
        widgets = {
            'room': autocomplete.ModelSelect2(url='rooms:room_autocomplete', attrs={'data-placeholder': 'Room', 'data-minimum-input-length': 1},),
            'staff': autocomplete.ModelSelect2(url='staffs:staffs_autocomplete', attrs={'data-placeholder': 'Staff', 'data-minimum-input-length': 1},),           
        }
        error_messages = {
            'asset' : {
                'required' : _("Asset field is required."),
            },
            'qty' : {
                'required' : _("Qty field is required."),
            },  
        }
