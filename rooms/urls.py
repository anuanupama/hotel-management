from django.conf.urls import url, include
from django.contrib import admin
import views
from rooms.views import RoomCategoryAutocomplete,RoomAutocomplete,RoomSubcategoryAutocomplete


urlpatterns = [
    url(r'^subcategory-autocomplete/$',RoomSubcategoryAutocomplete.as_view(create_field='name'),name='room_subcategory_autocomplete'),
    url(r'^category-autocomplete/$',RoomCategoryAutocomplete.as_view(create_field='name'),name='room_category_autocomplete'),
    url(r'^room-autocomplete/$',RoomAutocomplete.as_view(),name='room_autocomplete'),
    
    url(r'^$', views.dashboard,name='dashboard'), 
    
    url(r'^room/create/$',views.create,name='create'),
    url(r'^room/views/$',views.rooms,name='rooms'),
    url(r'^room/edit/(?P<pk>.*)/$',views.edit,name='edit'),
    url(r'^room/view/(?P<pk>.*)/$',views.room,name='room'),
    url(r'^room/delete/(?P<pk>.*)/$',views.delete,name='delete'),
    url(r'^room/delete-selected/$', views.delete_selected_rooms, name='delete_selected_rooms'),

    url(r'^asset/create/$', views.create_asset, name='create_asset'),
    url(r'^asset/view/(?P<pk>.*)/$', views.asset, name='asset'),
    url(r'^asset/edit/(?P<pk>.*)/$', views.edit_asset, name='edit_asset'),
    url(r'^assets/$', views.assets, name='assets'),
    url(r'^delete-asset/(?P<pk>.*)/$', views.delete_asset, name='delete_asset'),
    url(r'^delete-selected-assets/$', views.delete_selected_assets, name='delete_selected_assets'),


    url(r'^asset-transfer/create/$',views.create_asset_transfer,name='create_asset_transfer'),
    url(r'^asset-transfer/view/$',views.asset_transfers,name='asset_transfers'),
    url(r'^asset-transfer/edit/(?P<pk>.*)/$',views.edit_asset_transfer,name='edit_asset_transfer'),
    url(r'^asset-transfer/views/(?P<pk>.*)/$',views.asset_transfer,name='asset_transfer'),
    url(r'^asset-transfer/delete/(?P<pk>.*)/$',views.delete_asset_transfer,name='delete_asset_transfer'),
    url(r'^asset-transfer/delete-selected/$', views.delete_selected_asset_transfers, name='delete_selected_asset_transfers'),

    url(r'^cleaning/create/$',views.create_cleaning,name='create_cleaning'),
    url(r'^cleaning/view/$',views.cleanings,name='cleanings'),
    url(r'^cleaning/edit/(?P<pk>.*)/$',views.edit_cleaning,name='edit_cleaning'),
    url(r'^cleaning/views/(?P<pk>.*)/$',views.cleaning,name='cleaning'),
    url(r'^cleaning/delete/(?P<pk>.*)/$',views.delete_cleaning,name='delete_cleaning'),
    url(r'^cleaning/delete-selected/$', views.delete_selected_cleanings, name='delete_selected_cleanings'),

    url(r'^cleaning-status/$',views.cleaning_status,name='cleaning_status'),
    url(r'^cleaning/(?P<pk>.*)/$', views.cleaning, name='cleaning'),
    url(r'^finish-cleaning/(?P<pk>.*)/$', views.finish_cleaning, name='finish_cleaning'),

    # url(r'^get-product/$',views.get_product,name='get_product'),
    # url(r'^get-asset/$',views.get_asset,name='get_asset'),
    # url(r'^get-product-sub-categories/$', views.get_product_sub_categories, name='get_product_sub_categories'),
    # url(r'^get-product-units/$', views.get_product_units, name='get_product_units'),
    # url(r'^get-returnable-product/$',views.get_returnable_product,name='get_returnable_product'),
    # url(r'^get-unit-price/$', views.get_unit_price, name='get_unit_price'),

    # url(r'^get-expiry/$', views.get_expiry, name='get_expiry'),
    # url(r'^get-purchase-items/$', views.get_purchase_items, name='get_purchase_items'),
    
    # url(r'^upload-product-list/$', views.upload_product_list, name='upload_product_list'),
    
    # url(r'^category/create/$',views.create_category,name='create_category'),
    # url(r'^categories/$',views.categories,name='categories'),
    # url(r'^category/edit/(?P<pk>.*)/$',views.edit_category,name='edit_category'),
    # url(r'^category/view/(?P<pk>.*)/$',views.category,name='category'),
    # url(r'^category/delete/(?P<pk>.*)/$',views.delete_category,name='delete_category'),
    # url(r'^category/delete-selected/$', views.delete_selected_categories, name='delete_selected_categories'),

    # url(r'^create-subcategory/$', views.create_subcategory, name='create_subcategory'),
    # url(r'^subcategories/$', views.subcategories, name='subcategories'),
    # url(r'^edit-subcategory/(?P<pk>.*)/$', views.edit_subcategory, name='edit_subcategory'),
    # url(r'^subcategory/(?P<pk>.*)/$', views.subcategory, name='subcategory'),
    # url(r'^delete-subcategory/(?P<pk>.*)/$', views.delete_subcategory, name='delete_subcategory'),
    # url(r'^delete-selected-subcategories/$', views.delete_selected_subcategories, name='delete_selected_subcategories'),

    

    # url(r'^asset/create/$', views.create_asset, name='create_asset'),
    # url(r'^asset/view/(?P<pk>.*)/$', views.asset, name='asset'),
    # url(r'^asset/edit/(?P<pk>.*)/$', views.edit_asset, name='edit_asset'),
    # url(r'^assets/$', views.assets, name='assets'),
    # url(r'^delete-asset/(?P<pk>.*)/$', views.delete_asset, name='delete_asset'),
    # url(r'^delete-selected-assets/$', views.delete_selected_assets, name='delete_selected_assets'), 
]