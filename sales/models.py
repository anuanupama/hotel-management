from __future__ import unicode_literals
from django.db import models
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from main.models import BaseModel


STATUS_SALE_RETURN = (('returnable', 'Returnable'),('damaged', 'Damaged'))

SALE_TYPE = (
    ('retail','Retail'),
    ('wholesale','Wholesale'),
    ('wholesale_with_customer','Customer Wholesale'),
)
GST_TYPE = (
    ('sgst','SGST'),
    ('igst','IGST'),
)

class Sale(BaseModel):
    shop = models.ForeignKey("main.Shop")
    customer = models.ForeignKey("customers.Customer",limit_choices_to={'is_deleted': False},blank=True,null=True)
    sale_type = models.CharField(max_length=128,choices=SALE_TYPE,default="retail")
    time = models.DateTimeField()
    subtotal = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total_tax_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total_discount_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    special_discount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    payment_received = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    balance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    round_off = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    collected_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    payment_remainder_date = models.DateField(blank=True,null=True)
    gst_type = models.CharField(max_length=128,choices=GST_TYPE,default="sgst")
    old_debit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    old_credit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    customer_gstin_invoice_id = models.PositiveIntegerField(blank=True,null=True)
    customer_non_gstn_invoice_id = models.PositiveIntegerField(blank=True,null=True)    
    
    is_deleted = models.BooleanField(default=False)

    customer_gstin_invoice_id = models.PositiveIntegerField(blank=True,null=True)
    customer_non_gstn_invoice_id = models.PositiveIntegerField(blank=True,null=True)

    def total_taxable_amount(self):
        sale_items = SaleItem.objects.filter(sale=self)
        total_taxable_amount = 0
        for sale_item in sale_items:
            total_taxable_amount += sale_item.taxable_amount()
            
        return total_taxable_amount

    def invoice_id(self):
        if self.customer_gstin_invoice_id:
            invoice_id = "G00" + str(self.customer_gstin_invoice_id)

        if self.customer_non_gstn_invoice_id:
            invoice_id = "N00" + str(self.customer_non_gstn_invoice_id)
        return invoice_id
    
    class Meta:
        db_table = 'sales_sale'
        verbose_name = _('sale')
        verbose_name_plural = _('sales')
        ordering = ('-auto_id',)       
    
    def __unicode__(self): 
        return "%s - %s - %s" %(str(self.a_id), self.customer.name,str(self.total))

    def invoice_id(self):
        invoice_id = str(self.a_id)
        if self.customer_gstin_invoice_id:
            invoice_id = "G00" + str(self.customer_gstin_invoice_id)

        if self.customer_non_gstn_invoice_id:
            invoice_id = "N00" + str(self.customer_non_gstn_invoice_id)
        return invoice_id


class SaleItem(models.Model):
    sale = models.ForeignKey("sales.Sale",limit_choices_to={'is_deleted': False})
    product = models.ForeignKey("products.Product",limit_choices_to={'is_deleted': False})
    unit = models.ForeignKey("products.Measurement",limit_choices_to={'is_deleted': False})
    qty = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    cost = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    price = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    tax_added_price = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    tax = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    tax_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    discount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    discount_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    subtotal = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    return_qty = models.DecimalField(default=0,null=True,blank=True,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    is_deleted = models.BooleanField(default=False)

    def taxable_amount(self):
        return self.subtotal - self.tax_amount

    def actual_qty(self):
        return self.qty - self.return_qty
     
    class Meta:
        db_table = 'sales_sale_item'
        verbose_name = _('sale item')
        verbose_name_plural = _('sale items')
        ordering = ('product',)       
    
    def __unicode__(self): 
        return "%s - %s" %(self.product.name,str(self.qty))


class CollectAmount(BaseModel):
    shop = models.ForeignKey("main.Shop")
    date = models.DateField()
    collect_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    customer = models.ForeignKey("customers.Customer",blank=True,null=True)
    remaining_balance = models.DecimalField(default=0,decimal_places=2, max_digits=15)
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'collect_amount'
        verbose_name = _('collect_amount')
        verbose_name_plural = _('collect_amounts')
        ordering = ('-auto_id',)
    
    
    def __unicode__(self): 
        return "%s" %(self.collect_amount)


class SaleCollectAmountHistory(models.Model):
    sale = models.ForeignKey("sales.Sale")
    amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    collect_amount = models.ForeignKey("sales.CollectAmount",blank=True,null=True)
    paid_from_sale = models.ForeignKey("sales.Sale",related_name="paid_from_%(class)s_objects",blank=True,null=True)

    class meta:
        db_table = 'sales_sale_collect_amount_history'
        verbose_name = _('sale collect amount history')
        verbose_name_plural = _('sale collect amount histories')

    def __unicode__(self):
        return self.amount


class CustomerPayment(BaseModel):
    shop = models.ForeignKey("main.Shop")
    date = models.DateField()
    paid_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    customer = models.ForeignKey("customers.Customer",blank=True,null=True)
    remaining_balance = models.DecimalField(default=0,decimal_places=2, max_digits=15)
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'payment_amount'
        verbose_name = _('payment_amount')
        verbose_name_plural = _('payment_amounts')
        ordering = ('-auto_id',)
    
    
    def __unicode__(self): 
        return "%s" %(self.collect_amount)


class SaleReturn(BaseModel):
    shop = models.ForeignKey("main.Shop")
    customer = models.ForeignKey("customers.Customer",limit_choices_to={'is_deleted': False},blank=True,null=True)
    sale = models.ForeignKey("sales.Sale")
    time = models.DateTimeField()
    is_deleted = models.BooleanField(default=False)
    amount_returned = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    class Meta:
        db_table = 'sale_return'    
        verbose_name = _('sale return')
        verbose_name_plural = _('sale returns')

    class Admin:
        list_display = ('customer',)
        
    def __unicode__(self):
        return self.sale.customer

    def t(self):
        items_total = 0
        items = SaleReturnItem.objects.filter(sale_return=self)
        for i in items:
            qty = Decimal(i.qty)
            price = i.price
            sub = qty * price
            items_total += sub
    
        subtotal =  items_total
        total =  items_total
       
        result = {
            "subtotal" : subtotal,
            "total" : round(total,2)
        }
        return result


class Damaged(BaseModel):
    shop = models.ForeignKey("main.Shop")
    time = models.DateTimeField(blank=True)

    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'damaged'    
        verbose_name = _('damaged')
        verbose_name_plural = _('damaged')

    class Admin:
        list_display = ('time',)

    def __unicode__(self):
        return self.time


class DamagedProduct(models.Model):
    damaged = models.ForeignKey("sales.Damaged",null=True,limit_choices_to={'is_deleted': False})
    shop = models.ForeignKey("main.Shop")
    product = models.ForeignKey("products.Product")
    qty = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    unit = models.ForeignKey("products.Measurement",limit_choices_to={'is_deleted': False})
    price = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    cost = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    status = models.CharField(max_length=128,choices=STATUS_SALE_RETURN,default="damaged")
    is_deleted = models.BooleanField(default=False)
    sale_return = models.ForeignKey("sales.SaleReturn",null=True,blank=True)
    product_return = models.ForeignKey("sales.ProductReturn",null=True,blank=True)
    is_returned = models.BooleanField(default=False)

    class Meta:
        db_table = 'damaged_product'    
        verbose_name = _('damaged product')
        verbose_name_plural = _('damaged products')

    class Admin:
        list_display = ('product',)

    def __unicode__(self):
        return self.product.name


class SaleReturnItem(models.Model):
    shop = models.ForeignKey("main.Shop")
    product = models.ForeignKey("products.Product")
    sale_return = models.ForeignKey("sales.SaleReturn")
    qty = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    unit = models.ForeignKey("products.Measurement",limit_choices_to={'is_deleted': False})
    price = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    cost = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    status = models.CharField(max_length=128,choices=STATUS_SALE_RETURN,default="returnable")
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'sale_return_item'    
        verbose_name = _('sale return item')
        verbose_name_plural = _('sale return items')

    class Admin:
        list_display = ('product',)
        
    def __unicode__(self):
        return self.product.name


class ReturnableProduct(BaseModel):
    shop = models.ForeignKey("main.Shop")
    product = models.ForeignKey("products.Product")
    qty = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    unit = models.ForeignKey("products.Measurement",limit_choices_to={'is_deleted': False})
    price = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    cost = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    sale_return = models.ForeignKey("sales.SaleReturn",null=True,blank=True)
    damaged_product = models.ForeignKey("sales.DamagedProduct",null=True,blank=True)
    is_deleted = models.BooleanField(default=False)
    is_returned = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'returnable_product'
        verbose_name = _('returnable product')
        verbose_name_plural = _('returnable products')

    class Admin:
        list_display = ('product',)
        
    def __unicode__(self):
        return self.product.name


class ProductReturn(BaseModel):
    shop = models.ForeignKey("main.Shop")
    vendor = models.ForeignKey("vendors.Vendor")
    time = models.DateTimeField(blank=True)
    is_deleted = models.BooleanField(default=False)
    amount_returned = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    class Meta:
        db_table = 'product_return'    
        verbose_name = _('product return')
        verbose_name_plural = _('product returns')
 
    class Admin:
        list_display = ('vendor',)
         
    def __unicode__(self):
        return self.vendor.name


class ProductReturnItem(models.Model):
    shop = models.ForeignKey("main.Shop")
    product_return = models.ForeignKey("sales.ProductReturn")
    product = models.ForeignKey("sales.DamagedProduct")
    qty = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    unit = models.ForeignKey("products.Measurement",limit_choices_to={'is_deleted': False})
    price = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    cost = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)
     
     
    class Meta:
        db_table = 'product_return_item'
        verbose_name = _('product return item')
        verbose_name_plural = _('product return items')
 
    class Admin:
        list_display = ('product',)
         
    def __unicode__(self):
        return self.product.name  


class Estimate(BaseModel):
    shop = models.ForeignKey("main.Shop")
    customer = models.ForeignKey("customers.Customer",limit_choices_to={'is_deleted': False},blank=True,null=True)
    sale_type = models.CharField(max_length=128,choices=SALE_TYPE,default="retail")
    time = models.DateTimeField()
    subtotal = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total_tax_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total_discount_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    special_discount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    payment_received = models.DecimalField(decimal_places=2,blank=True,null=True, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    balance = models.DecimalField(default=0,decimal_places=2,blank=True,null=True, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    round_off = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    collected_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    payment_remainder_date = models.DateField(blank=True,null=True)
    gst_type = models.CharField(max_length=128,choices=GST_TYPE,default="sgst")
    old_debit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    old_credit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)

    def total_taxable_amount(self):
        estimate_items = EstimateItem.objects.filter(estimate=self)
        total_taxable_amount = 0
        for estimate_item in estimate_items:
            total_taxable_amount += estimate_item.taxable_amount()
            
        return total_taxable_amount
    
    class Meta:
        db_table = 'sales_estimate'
        verbose_name = _('estimate')
        verbose_name_plural = _('estimates')
        ordering = ('-auto_id',)       
    
    def __unicode__(self): 
        return "%s - %s - %s" %(str(self.a_id), self.customer.name,str(self.total))


class EstimateItem(models.Model):
    estimate = models.ForeignKey("sales.Estimate",limit_choices_to={'is_deleted': False})
    product = models.ForeignKey("products.Product",limit_choices_to={'is_deleted': False})
    unit = models.ForeignKey("products.Measurement",limit_choices_to={'is_deleted': False})
    qty = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    cost = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    price = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    tax_added_price = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    tax = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    tax_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    discount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 
    discount_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    subtotal = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    return_qty = models.DecimalField(default=0,null=True,blank=True,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    is_deleted = models.BooleanField(default=False)

    def taxable_amount(self):
        return self.subtotal - self.tax_amount

    def actual_qty(self):
        return self.qty - self.return_qty
     
    class Meta:
        db_table = 'sales_estimate_item'
        verbose_name = _('estimate item')
        verbose_name_plural = _('estimate items')
        ordering = ('product',)       
    
    def __unicode__(self): 
        return "%s - %s" %(self.product.name,str(self.qty))

    
    