from django.conf.urls import url, include
from django.contrib import admin
import views
from sales.views import SaleAutocomplete,ReturnableProductAutocomplete


urlpatterns = [               
    url(r'^sale-autocomplete/$',SaleAutocomplete.as_view(),name='sale_autocomplete'),
    url(r'^returnable_products-autocomplete/$',ReturnableProductAutocomplete.as_view(),name='returnable_product_autocomplete'),
    
    url(r'^$', views.dashboard,name='dashboard'), 
    url(r'^print-report/$', views.print_report,name='print_report'), 
    url(r'^taxed-sale-items/$', views.taxed_sale_items,name='taxed_sale_items'),
    url(r'^export-report/$', views.export_report,name='export_report'),
    
    url(r'^sale/create/$',views.create,name='create'),
    url(r'^sale/views/$',views.sales,name='sales'),
    url(r'^sale/edit/(?P<pk>.*)/$',views.edit,name='edit'),
    url(r'^sale/view/(?P<pk>.*)/$',views.sale,name='sale'),
    url(r'^sale/delete/(?P<pk>.*)/$',views.delete,name='delete'),
    url(r'^sale/delete-selected/$', views.delete_selected_sales, name='delete_selected_sales'),
    url(r'^invoice/(?P<pk>.*)/$',views.print_sale,name='print'),
    url(r'^email/(?P<pk>.*)/$',views.email_sale,name='email_sale'),

    url(r'^sale-items/views/$',views.sale_items,name='sale_items'),

    url(r'^collect-amount/create/$', views.create_collect_amount, name='create_collect_amount'),
    url(r'^collect-amount/view/(?P<pk>.*)/$', views.collect_amount, name='collect_amount'),
    url(r'^collect-amount/edit/(?P<pk>.*)/$', views.edit_collect_amount, name='edit_collect_amount'),
    url(r'^collect-amounts/$', views.collect_amounts, name='collect_amounts'),
    url(r'^collect-amount/delete/(?P<pk>.*)/$', views.delete_collect_amount, name='delete_collect_amount'),
    url(r'^collect-amount/delete-selected/$', views.delete_selected_collect_amounts, name='delete_selected_collect_amounts'),
    url(r'^collected-amount/print/(?P<pk>.*)/$',views.print_collected_amount,name='print_collected_amount'),
    url(r'^collected-amounts/print/$',views.print_collected_amounts,name='print_collected_amounts'),

    url(r'^customer-payment/create/$', views.create_customer_payment, name='create_customer_payment'),
    url(r'^customer-payment/view/(?P<pk>.*)/$', views.customer_payment, name='customer_payment'),
    url(r'^customer-payment/edit/(?P<pk>.*)/$', views.edit_customer_payment, name='edit_customer_payment'),
    url(r'^customer-payments/$', views.customer_payments, name='customer_payments'),
    url(r'^customer-payment/delete/(?P<pk>.*)/$', views.delete_customer_payment, name='delete_customer_payment'),
    url(r'^customer-payment/delete-selected/$', views.delete_selected_customer_payments, name='delete_selected_customer_payments'),
    url(r'^customer-payment/print/(?P<pk>.*)/$',views.print_customer_payment,name='print_customer_payment'),
    url(r'^customer-payments/print/$',views.print_customer_payments,name='print_customer_payments'),

    url(r'^create-sale-return/$',views.create_sale_return,name='create_sale_return'),
    url(r'^sale-returns/$',views.sale_returns,name='sale_returns'), 
    url(r'^sale-return/(?P<pk>.*)/$', views.sale_return, name='sale_return'),
    url(r'^edit-sale-return/(?P<pk>.*)/$', views.edit_sale_return, name='edit_sale_return'),
    url(r'^delete-sale-return/(?P<pk>.*)/$', views.delete_sale_return, name='delete_sale_return'),

    url(r'^create-damaged-product/$',views.create_damaged_product,name='create_damaged_product'),
    url(r'^damaged-products/$',views.damaged_products,name='damaged_products'), 
    url(r'^damaged-product/(?P<pk>.*)/$', views.damaged_product, name='damaged_product'),
    url(r'^edit-damaged-product/(?P<pk>.*)/$', views.edit_damaged_product, name='edit_damaged_product'),
    url(r'^delete-damaged-product/(?P<pk>.*)/$', views.delete_damaged_product, name='delete_damaged_product'),

    url(r'^returnable-products/$',views.returnable_products,name='returnable_products'),
    url(r'^returnable-product/view(?P<pk>.*)/$',views.returnable_product,name='returnable_product'),
    url(r'^returnable-product/delete/(?P<pk>.*)/$', views.delete_returnable_product, name='delete_returnable_product'),
    url(r'^returnable-product/delete-selected/$', views.delete_selected_returnable_products, name='delete_selected_returnable_products'),

    url(r'^create-product-return/$',views.create_product_return,name='create_product_return'),
    url(r'^product-return/(?P<pk>.*)/$', views.product_return, name='product_return'),
    url(r'^product-returns/$',views.product_returns,name='product_returns'),
    url(r'^delete-product-return/(?P<pk>.*)/$', views.delete_product_return, name='delete_product_return'),

    url(r'^create-damaged-product/$',views.create_damaged_product,name='create_damaged_product'),
    url(r'^damaged-products/$',views.damaged_products,name='damaged_products'), 
    url(r'^damaged-product/(?P<pk>.*)/$', views.damaged_product, name='damaged_product'),
    url(r'^edit-damaged-product/(?P<pk>.*)/$', views.edit_damaged_product, name='edit_damaged_product'),
    url(r'^delete-damaged-product/(?P<pk>.*)/$', views.delete_damaged_product, name='delete_damaged_product'),

    url(r'^get-sale-items/$', views.get_sale_items,name='get_sale_items'),
    url(r'^get-customer/$', views.get_customer,name='get_customer'),

    url(r'^estimate/create/$',views.create_estimate,name='create_estimate'),
    url(r'^estimate/views/$',views.estimates,name='estimates'),
    url(r'^estimate/edit/(?P<pk>.*)/$',views.edit_estimate,name='edit_estimate'),
    url(r'^estimate/view/(?P<pk>.*)/$',views.estimate,name='estimate'),
    url(r'^estimate/delete/(?P<pk>.*)/$',views.delete_estimate,name='delete_estimate'),
    url(r'^estimate/delete-selected/$', views.delete_selected_estimates, name='delete_selected_estimates'),
    url(r'^print-estimate/(?P<pk>.*)/$',views.print_estimate,name='print_estimate'),
    url(r'^email-estimate/(?P<pk>.*)/$',views.email_estimate,name='email_estimate'),

    url(r'^stock/report/$',views.stock_report, name='stock_report'),
]