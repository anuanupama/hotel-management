from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from sales.models import Sale, SaleItem,CollectAmount,SaleReturn,SaleReturnItem,DamagedProduct,ReturnableProduct,\
    ProductReturn,ProductReturnItem,CustomerPayment,Damaged,Estimate,EstimateItem
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, shop_required, check_account_balance,permissions_required,ajax_required
from sales.forms import SaleForm, SaleItemForm, EmailSaleForm,CollectAmountForm,SaleReturnForm,SaleReturnItemForm,\
    DamagedProductForm,ReturnableProductForm,ProductReturnForm,ProductReturnItemForm,CustomerPaymentForm,DamagedForm,EstimateForm,EstimateItemForm
from main.functions import generate_form_errors, get_auto_id, get_timezone, get_a_id
from finance.functions import add_transaction
from finance.forms import BankAccountForm, CashAccountForm, TransactionCategoryForm, TransactionForm
from finance.models import BankAccount, CashAccount, TransactionCategory, Transaction, TaxCategory
from purchases.models import Purchase,PurchaseItem
import datetime
from django.db.models import Q
from dal import autocomplete
from django.forms.models import inlineformset_factory
from django.forms.widgets import TextInput,Select
from django.forms.formsets import formset_factory
from products.functions import update_sock,get_exact_qty
from customers.functions import update_customer_credit_debit
from vendors.functions import update_vendor_credit_debit
from products.models import Product,Category,Measurement,ProductAlternativeUnitPrice
from main.functions import render_to_pdf 
from django.utils import timezone
import pytz
from users.functions import get_current_shop, send_email,create_notification
from django.template.loader import render_to_string
from users.models import NotificationSubject, Notification
from customers.models import Customer,CustomerCredit
from purchases.models import Purchase
from decimal import Decimal
from django.db.models import Sum
import xlwt
import urllib
from django.conf import settings
from django.core import serializers
import inflect


class SaleAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = Sale.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(a_id__istartswith=self.q) | 
                                 Q(customer__name__istartswith=self.q) |
                                 Q(customer__address__istartswith=self.q) |
                                 Q(customer__email__istartswith=self.q) |
                                 Q(customer__phone__istartswith=self.q)
                                )
    
        return items


class ReturnableProductAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = ReturnableProduct.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(a_id__istartswith=self.q) | 
                                 Q(product__name__istartswith=self.q) | 
                                 Q(product__code__istartswith=self.q)                              
                                )    
        return items
    
    
@check_mode
@login_required
@shop_required
def dashboard(request):
    current_shop = get_current_shop(request)
    today = datetime.date.today()
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    instances = Sale.objects.filter(shop=current_shop,is_deleted=False)
    sale_items = SaleItem.objects.filter(sale__shop=current_shop,sale__is_deleted=False)
    sale_return_instances = SaleReturn.objects.filter(is_deleted=False,sale__shop=current_shop)
    sales_count = 0
    sales_total = 0
    final_discount_amount = 0
    payment_received_total = 0
    balance_total = 0
    total_tax_amount_total = 0
    profit_amount = 0
    special_discount_all = 0
    total_special_discount = 0
    total_discount = 0
    sale_return_amount = 0
    final_profit_amount = 0
    
    title = "Sales Dashboard"
    tax_categories = TaxCategory.objects.filter(shop=current_shop,is_deleted=False)
    counter = 0
    tax_percentage_dict = {}
    filter_date = date

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)
            
    #filter by date created range
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')

    filter_from_date = from_date
    filter_to_date = to_date   

    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
            sale_return_instances = sale_return_instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
            sale_return_instances = sale_return_instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)
            sale_return_instances =sale_return_instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])
            sale_return_instances = sale_return_instances.filter(is_deleted=False, time__range=[from_date, to_date])
            
    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)
            sale_return_instances = sale_return_instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)

    if instances:
        sale_items = sale_items.filter(sale__in=instances)
        sales_dict = instances.aggregate(Sum('total'),Sum('special_discount'),Sum('payment_received'),Sum('balance'),Sum('total_discount_amount'),Sum('total_tax_amount'))
        sale_return_amount = sale_return_instances.aggregate(amount=Sum('amount_returned')).get('amount',0)
        
        sales_total  = sales_dict['total__sum'] 
        balance_total = sales_dict['balance__sum']
        total_tax_amount_total = sales_dict['total_tax_amount__sum']
        sales_count = instances.count()
        
        if sale_items:
            for item in sale_items:
                cost = item.cost
                price = item.price
                quantity = item.qty
                tax_amount = item.tax_amount
                profit = (quantity * (price - cost))- tax_amount
                profit_amount += profit
            print profit_amount
        for tax in tax_categories:
            items = sale_items.filter(product__tax_category=tax)

            tax_amount = 0
            if items :
                tax_amount = items.aggregate(tax_amount=Sum('tax_amount')).get("tax_amount",0)
            tax_percentage_dict[str(tax.tax)] = tax_amount
            counter += 1

        final_discount_amount = sales_dict['special_discount__sum'] + sales_dict['total_discount_amount__sum']
        total_special_discount = sales_dict['special_discount__sum']
        total_discount = sales_dict['total_discount_amount__sum']

        payment_received_total = sales_dict['payment_received__sum']

        final_profit_amount = profit_amount - total_special_discount - total_discount

    context = {
        'title' : title,
        "sales_count" : sales_count,
        "sales_total" : sales_total + total_special_discount + total_discount,
        "special_discount_total" : total_special_discount,
        "payment_received_total" : payment_received_total,
        "balance_total" : balance_total,
        "total_tax_amount_total" : total_tax_amount_total,
        "tax_percentage_dict" : tax_percentage_dict,
        "total_discount" : total_discount,
        "filter_from_date" : filter_from_date,
        "period" : period,
        "filter_to_date" : filter_to_date,
        "filter_date" : filter_date,
        "year" : year,
        "month" : month,
        "profit_amount" : round(final_profit_amount,2),
        "sale_return_amount" : sale_return_amount,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request,'sales/dashboard.html',context)


@login_required
def export_report(request):
    current_shop = get_current_shop(request)
    instances = Product.objects.filter(is_deleted=False,shop=current_shop)

    current_shop = get_current_shop(request)
    today = datetime.date.today()
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    instances = Sale.objects.filter(shop=current_shop,is_deleted=False)
    sale_items = SaleItem.objects.filter(sale__shop=current_shop,sale__is_deleted=False)
    sales_count = 0
    sales_total = 0
    final_discount_amount = 0
    payment_received_total = 0
    balance_total = 0
    total_tax_amount_total = 0
    title = "Report"
    tax_categories = TaxCategory.objects.filter(shop=current_shop,is_deleted=False)
    counter = 0
    tax_percentage_dict = {}
    filter_date = date

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)
            
    #filter by date created range
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')

    filter_from_date = from_date
    filter_to_date = to_date   

    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])
            
    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)

    if instances:
        sale_items = sale_items.filter(sale__in=instances)
        sales_dict = instances.aggregate(Sum('total'),Sum('special_discount'),Sum('payment_received'),Sum('balance'),Sum('total_discount_amount'),Sum('total_tax_amount'))
        payment_received_total = sales_dict['payment_received__sum']
        sales_total  = sales_dict['total__sum']
        balance_total = sales_dict['balance__sum']
        total_tax_amount_total = sales_dict['total_tax_amount__sum']
        sales_count = instances.count()

        for tax in tax_categories:
            items = sale_items.filter(product__tax_category=tax)

            tax_amount = 0
            if items :
                tax_amount = items.aggregate(tax_amount=Sum('tax_amount')).get("tax_amount",0)
            tax_percentage_dict[str(tax.tax)] = tax_amount
            counter += 1

        final_discount_amount = sales_dict['special_discount__sum'] + sales_dict['total_discount_amount__sum']

    wb = xlwt.Workbook()
    ws = wb.add_sheet(title)

    ws.write(0, 0, "Total Sales")
    ws.write(0, 1, sales_count)

    ws.write(1, 0, "Total Sales Amount")
    ws.write(1, 1, sales_total)

    ws.write(2, 0, "Special Discounts")
    ws.write(2, 1, sales_total)

    ws.write(3, 0, "Special Discounts")
    ws.write(3, 1, final_discount_amount)

    ws.write(4, 0, "Payment Recieved")
    ws.write(4, 1, payment_received_total)

    ws.write(5, 0, "Total Balance")
    ws.write(5, 1, balance_total)

    ws.write(6, 0, "Total Tax Amount")
    ws.write(6, 1, total_tax_amount_total)

    ws.write(7, 0, "Sales Count")
    ws.write(7, 1, sales_count)

    if instances :
        counter = 8
        for key, value in tax_percentage_dict.items():
            title = "Tax Amount" + key + "%"
            ws.write(counter, 0, title)
            ws.write(counter, 1, value)
            counter += 1

    media_root = settings.MEDIA_ROOT + '/excel_report.xls'
    wb.save(media_root)

    host_name = request.get_host()
    full_url = 'http://' + host_name + '/media/excel_report.xls'

    response_data = {
        "status" : "true",
        "file_url" : full_url
    }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

@check_mode
@login_required
@shop_required
def taxed_sale_items(request):
    current_shop = get_current_shop(request)
    today = datetime.date.today()
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    tax_category = request.GET.get('tax_category')
    instances = Sale.objects.filter(shop=current_shop,is_deleted=False)
    sale_items = []
    title = "Tax Category : " + tax_category + "%"
    filter_date = date

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)
            
    #filter by date created range
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')

    filter_from_date = from_date
    filter_to_date = to_date   

    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])
            
    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)

    if instances:
        if TaxCategory.objects.filter(tax=tax_category,shop=current_shop,is_deleted=False).exists():
            tax_category = TaxCategory.objects.get(tax=tax_category,shop=current_shop,is_deleted=False)
            sale_items = SaleItem.objects.filter(sale__in=instances,product__tax_category=tax_category)

    context = {
        'title' : title,
        "sale_items" : sale_items,

        "filter_from_date" : filter_from_date,
        "period" : period,
        "filter_to_date" : filter_to_date,
        "filter_date" : filter_date,
        "year" : year,
        "month" : month,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request,'sales/taxed_sale_items.html',context)


@check_mode
@login_required
@shop_required
def print_report(request):
    current_shop = get_current_shop(request)
    today = datetime.date.today()
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    instances = Sale.objects.filter(shop=current_shop,is_deleted=False)
    sale_items = SaleItem.objects.filter(sale__shop=current_shop,sale__is_deleted=False)
    sales_count = 0
    sales_total = 0
    final_discount_amount = 0
    payment_received_total = 0
    balance_total = 0
    total_tax_amount_total = 0
    title = "Sales Report"
    tax_categories = TaxCategory.objects.filter(shop=current_shop,is_deleted=False)
    counter = 0
    tax_percentage_dict = {}
    filter_date = date

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)
            
    #filter by date created range
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')

    filter_from_date = from_date
    filter_to_date = to_date   

    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])
            
    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)

    if instances:
        sale_items = sale_items.filter(sale__in=instances)
        sales_dict = instances.aggregate(Sum('total'),Sum('special_discount'),Sum('payment_received'),Sum('balance'),Sum('total_discount_amount'),Sum('total_tax_amount'))
        payment_received_total = sales_dict['payment_received__sum']
        sales_total  = sales_dict['total__sum']
        balance_total = sales_dict['balance__sum']
        total_tax_amount_total = sales_dict['total_tax_amount__sum']
        sales_count = instances.count()

        for tax in tax_categories:
            items = sale_items.filter(product__tax_category=tax)

            tax_amount = 0
            if items :
                tax_amount = items.aggregate(tax_amount=Sum('tax_amount')).get("tax_amount",0)
            tax_percentage_dict[str(tax.tax)] = tax_amount
            counter += 1

        final_discount_amount = sales_dict['special_discount__sum'] + sales_dict['total_discount_amount__sum']

    context = {
        'title' : title,
        "sales_count" : sales_count,
        "sales_total" : sales_total,
        "special_discount_total" : final_discount_amount,
        "payment_received_total" : payment_received_total,
        "balance_total" : balance_total,
        "total_tax_amount_total" : total_tax_amount_total,
        "tax_percentage_dict" : tax_percentage_dict,

        "filter_from_date" : filter_from_date,
        "period" : period,
        "filter_to_date" : filter_to_date,
        "filter_date" : filter_date,
        "year" : year,
        "month" : month,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request,'sales/print_report.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_sale'])
def create(request):    
    current_shop = get_current_shop(request)
    
    SaleItemFormset = formset_factory(SaleItemForm,extra=1)
    
    if request.method == 'POST':
        form = SaleForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        sale_item_formset = SaleItemFormset(request.POST,prefix='sale_item_formset')
        for sale_form in sale_item_formset:
            sale_form.fields['product'].queryset = Product.objects.filter(shop=current_shop,is_deleted=False) 
        if form.is_valid() and sale_item_formset.is_valid() and transaction_form.is_valid(): 
            
            items = {}
            total_tax_amount = 0
            total_discount_amount = 0

            for f in sale_item_formset:  
                
                discount_amount = f.cleaned_data['discount_amount']
                total_discount_amount += discount_amount 
                product = f.cleaned_data['product']
                unit = f.cleaned_data['unit']
                qty = f.cleaned_data['qty']
                exact_qty = get_exact_qty(qty,unit)
                if unit.is_base == True :
                    cost = product.cost
                else :
                    unit_instanse = get_object_or_404(ProductAlternativeUnitPrice.objects.filter(product=product,unit=unit))
                    cost = unit_instanse.cost
                price = f.cleaned_data['price']                
                product_tax_amount = price * product.tax / 100
                tax_added_price = price + product_tax_amount
                tax_amount = qty * (price - product.discount) * product.tax / 100

                tax_amount = Decimal(format(tax_amount, '.2f'))
                total_tax_amount += tax_amount 
                if str(product.pk) in items:
                    p_unit = items[str(product.pk)]["unit"]
                    p_unit = Measurement.objects.get(pk=p_unit)
                    if p_unit == unit:
                        q = items[str(product.pk)]["qty"]
                        items[str(product.pk)]["qty"] = q + qty
                    else:
                        dic = {
                            "qty" : qty,
                            "cost" : cost,
                            "price" : price,
                            "tax_amount" : tax_amount,
                            "tax_added_price" : tax_added_price,
                            "discount_amount" : discount_amount,
                            "unit" : unit.pk,
                        }
                        items[str(product.pk)] = dic
                else:
                    dic = {
                        "qty" : qty,
                        "cost" : cost,
                        "price" : price,
                        "tax_amount" : tax_amount,
                        "tax_added_price" : tax_added_price,
                        "discount_amount" : discount_amount,
                        "unit" : unit.pk,
                    }
                    items[str(product.pk)] = dic
            
            stock_ok = True
            error_message = ''
            for key, value in items.iteritems():
                product = Product.objects.get(pk=key)
                stock = product.stock
                qty = value['qty']
                unit = value["unit"]
                unit = Measurement.objects.get(pk=unit)
                exact_qty = get_exact_qty(qty,unit)
                if exact_qty > stock:
                    stock_ok = False
                    error_message += "%s has only %s in stock, " %(product.name,str(product.stock))      
            
            if stock_ok:   

                customer_name = form.cleaned_data['customer_name']
                customer_address = form.cleaned_data['customer_address']
                customer_email = form.cleaned_data['customer_email']
                customer_phone = form.cleaned_data['customer_phone']
                customer = form.cleaned_data['customer']
                customer_state = form.cleaned_data['customer_state']
                customer_gsttin = form.cleaned_data['customer_gsttin']

                if not customer:
                    auto_id = get_auto_id(Customer)
                    a_id = get_a_id(Customer,request)

                    customer = Customer(
                        name = customer_name,
                        email = customer_email,
                        phone = customer_phone,
                        address = customer_address,
                        shop = current_shop,
                        first_time_credit = 0,
                        first_time_debit = 0,
                        credit = 0,
                        debit = 0,
                        creator = request.user,
                        updator = request.user,
                        auto_id = auto_id,
                        a_id = a_id,
                        state = customer_state,
                        gst_tin = customer_gsttin
                    )
                    customer.save()

                gst_type = "sgst"
                if not customer.state == current_shop.state:
                    gst_type = "igst"

                auto_id = get_auto_id(Sale)
                a_id = get_a_id(Sale,request)
                
                #create sale
                special_discount = form.cleaned_data['special_discount']
                payment_received = form.cleaned_data['payment_received']
                customer_gstin_invoice_id = None
                customer_non_gstn_invoice_id = None
                if customer:
                    last_sale = Sale.objects.order_by("-date_added")[:1]                   
                    customer_non_gstn_invoice_id = 1
                    for l in last_sale:
                        if l.customer_non_gstn_invoice_id:
                            customer_non_gstn_invoice_id = int(l.customer_non_gstn_invoice_id) + 1
                                
                date = form.cleaned_data['time']                

                data1 = form.save(commit=False)
                data1.creator = request.user
                data1.updator = request.user
                data1.auto_id = auto_id
                data1.shop = current_shop
                data1.total_discount_amount = total_discount_amount
                data1.total_tax_amount = total_tax_amount
                data1.a_id = a_id
                data1.gst_type = gst_type
                data1.customer_gstin_invoice_id = customer_gstin_invoice_id
                data1.customer_non_gstn_invoice_id = customer_non_gstn_invoice_id
                data1.customer = customer
                data1.save()       
                 
                all_subtotal = 0
               
                #save items
                for key, value in items.iteritems():
                    product = Product.objects.get(pk=key)
                    qty = value["qty"]
                    price = value["price"]
                    tax = product.tax
                    tax_amount = value["tax_amount"]
                    unit = value["unit"]                   
                    unit = Measurement.objects.get(pk=unit)
                    exact_qty = get_exact_qty(qty,unit)
                    if unit.is_base:
                        cost = product.cost
                    else :
                        unit_instanse = get_object_or_404(ProductAlternativeUnitPrice.objects.filter(product=product,unit=unit))
                        cost = unit_instanse.cost
                    discount_amount = value["discount_amount"]
                    tax_added_price = value["tax_added_price"]
                    subtotal = (qty * price) - discount_amount + tax_amount
                    
                    all_subtotal += subtotal 
                    
                    SaleItem(
                        sale = data1,
                        product = product,
                        qty = qty,
                        cost = cost,
                        price = price,
                        tax = tax,
                        discount = product.discount,
                        tax_amount = tax_amount,
                        discount_amount = discount_amount,
                        subtotal = subtotal,
                        unit = unit,
                        tax_added_price = tax_added_price
                    ).save()
                    
                    update_sock(product.pk,exact_qty,"decrease")
                    
                for key, value in items.iteritems():
                    product = Product.objects.get(pk=key)
                    stock = product.stock
                    low_stock_limit = product.low_stock_limit
                    if stock < low_stock_limit:
                        create_notification(request,'low_stock_notification',product)

                credit=0
                debit=0
                if customer:
                    if not customer.is_system_generated:
                        credit = customer.credit
                        debit = customer.debit

                # Customer.objects.filter(pk=customer.pk,is_deleted=False).update(credit=0,debit=0)

                # total = all_subtotal - special_discount + credit - debit
                # total_total = total
                # if current_shop.remove_previous_balance_from_bill:
                #     total_total = all_subtotal - special_discount 
                total_total = all_subtotal - special_discount 
                total = all_subtotal - special_discount
                rounded_total = round(total)
                extra = round(total) - float(total)
                round_off = format(extra, '.2f')
                balance = rounded_total - float(payment_received)
                balance = Decimal(balance)
                this_sale_balance = (all_subtotal - special_discount) - payment_received

                if customer:
                    if customer.is_system_generated:
                        balance = 0
                        this_sale_balance = 0
                        data1.payment_received = rounded_total
                        data1.save()
                
                if not customer.is_system_generated:
                    #update credit
                    if balance > 0:
                        CustomerCredit.objects.create(shop=current_shop,sale=data1,customer=customer,amount=balance)
                        balance = balance
                        update_customer_credit_debit(customer.pk,"credit",balance)
                    elif balance < 0:
                        balance = abs(balance)
                        update_customer_credit_debit(customer.pk,"debit",balance)
                        balance = 0

                #update sale total,round_off and subtotal

                Sale.objects.filter(pk=data1.pk).update(subtotal=all_subtotal,total=total_total,balance=this_sale_balance,round_off=round_off,old_debit=debit,old_credit=credit)

                amount = form.cleaned_data['payment_received']
                
                add_transaction(request,transaction_form,data1,amount,"sale_payment","income",debit,credit)

                response_data = {
                    "status" : "true",
                    "title" : "Successfully Created",
                    "message" : "Sale created successfully.",
                    "redirect" : "true",
                    "redirect_url" : reverse('sales:print',kwargs={'pk':data1.pk})
                } 
            else:
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Out of Stock",
                    "message" : error_message
                } 
        
        else:            
            message = generate_form_errors(form,formset=False) 
            message += generate_form_errors(sale_item_formset,formset=True)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        default_customer = Customer.objects.get(name="default",address="default",shop=current_shop)
        sale_form = SaleForm(initial={"customer" : default_customer,"sale_type" : "retail"})
        sale_item_formset = SaleItemFormset(prefix='sale_item_formset')
        for form in sale_item_formset:
            form.fields['product'].queryset = Product.objects.filter(shop=current_shop,is_deleted=False)
            form.fields['unit'].queryset = Measurement.objects.none()
            form.fields['unit'].label_from_instance = lambda obj: "%s" % (obj.code)
        transaction_form = TransactionForm() 
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)  
        context = {
            "title" : "Create Sale ",
            "form" : sale_form,
            "transaction_form" : transaction_form,
            "url" : reverse('sales:create'),
            "sale_item_formset" : sale_item_formset,
            "redirect" : True,
            "is_create_page" : True,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'sales/entry.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_sale'])
def sales(request):
    current_shop = get_current_shop(request)
    instances = Sale.objects.filter(is_deleted=False,shop=current_shop)
    customers = Customer.objects.filter(is_deleted=False,shop=current_shop)
    products = Product.objects.filter(is_deleted=False,shop=current_shop)
    today = datetime.date.today()
    customer = request.GET.get('customer')
    product = request.GET.get('product')
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    payment = request.GET.get('payment')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    gst = request.GET.get('gst')

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    if customer:
        instances = instances.filter(customer_id=customer)
    if customer:
        instances = instances.filter(customer_id=customer)

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)

    if payment :      
        if payment == "full":
            instances = instances.filter(balance=0)
        elif payment == "no":
            for instance in instances:
                payment_received = instance.payment_received
                if payment_received != 0:
                    instances = instances.exclude(pk = instance.pk)
        elif payment == "extra":
            for instance in instances:
                total = instance.total
                payment_received = instance.payment_received
                if total >= payment_received:
                    instances = instances.exclude(pk=instance.pk)
        elif payment == "partial":
            for instance in instances:
                total = instance.total
                payment_received = instance.payment_received
                if total <= payment_received or payment_received == 0:
                    instances = instances.exclude(pk=instance.pk)
                    
    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])
            
    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)

    if gst :
        if gst =="gst":
            instances = instances.filter(customer_gstin_invoice_id__isnull=False)
        elif gst == 'no_gst' :
            instances = instances.filter(customer_non_gstn_invoice_id__isnull=False)

    context = {
        'instances': instances,
        'sales' :sales,
        'customers' : customers,
        'products' : products,
        "title" : 'Sales',

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "sales" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,    
    }
    return render(request,'sales/sales.html',context) 


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_sale'])
def sale_items(request):
    current_shop = get_current_shop(request)
    instances = SaleItem.objects.filter(is_deleted=False) 
    products = Product.objects.filter(is_deleted=False,shop=current_shop)   
    product = request.GET.get('product')    
    if product:
        instances = instances.filter(product_id=product)
    
    context = {
        'instances': instances,
        "title" : 'Sale Items',
        "products" :products,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "sales" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,    
    }
    return render(request,'sales/sale_items.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_sale'])
def sale(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Sale.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    transactions = Transaction.objects.filter(sale=instance,shop=current_shop,collect_amount=None)
    sale_items = SaleItem.objects.filter(sale=instance,is_deleted=False)
    context = {
        "instance" : instance,
        "transactions" : transactions,
        "title" : "Sale : #" + str(instance.auto_id),       
        "sale_items" : sale_items,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/sale.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_sale'])
def edit(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Sale.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    transaction = get_object_or_404(Transaction.objects.filter(sale=instance,shop=current_shop,collect_amount=None))        

    if SaleItem.objects.filter(sale=instance).exists():
        extra = 0
    else:
        extra= 1
    SaleItemFormset = inlineformset_factory(
        Sale, 
        SaleItem, 
        can_delete = True,
        extra = extra,
        exclude=('creator','updator','auto_id','is_deleted','tax','cost','subtotal','sale','tax_amount','tax_added_price'),
        widgets = {
            'product' : autocomplete.ModelSelect2(url='products:product_autocomplete',attrs={'data-placeholder': 'Product','data-minimum-input-length': 1},),
            'qty': TextInput(attrs={'class': 'required form-control number qty','placeholder' : 'Quantity'}),
            'cost': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Cost'}),
            'price': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Price'}),
            'subtotal': TextInput(attrs={'disabled' : 'disabled', 'class': 'required form-control number','placeholder' : 'Sub Total'}),
            'discount_amount' : TextInput(attrs={'class': 'form-control number','placeholder' : 'Discount Amount'}),
            'discount' : TextInput(attrs={'class': 'form-control number','placeholder' : 'Discount'}),
            'unit' : Select(attrs={'class': 'required form-control selectpicker'}),
        }
    )
    
    if request.method == 'POST':
        response_data = {}
        form = SaleForm(request.POST,instance=instance)
        transaction_form = TransactionForm(request.POST,instance=transaction)
        sale_item_formset = SaleItemFormset(request.POST,prefix='sale_item_formset',instance=instance)
        
        if form.is_valid() and sale_item_formset.is_valid() and transaction_form.is_valid():
            
            old_balance = Sale.objects.get(pk=pk).balance
            old_paid = Sale.objects.get(pk=pk).payment_received  
            old_transaction = get_object_or_404(Transaction.objects.filter(sale=pk,collect_amount=None))                
            items = {}
            total_discount_amount = 0
            total_tax_amount = 0

            for f in sale_item_formset:      
                if f not in sale_item_formset.deleted_forms:            
                    product = f.cleaned_data['product']
                    qty = f.cleaned_data['qty']
                    price = f.cleaned_data['price']
                    discount_amount = f.cleaned_data['discount_amount']
                    unit = f.cleaned_data['unit']
                    if unit.is_base:
                        cost = product.cost
                    else :
                        unit_instanse = get_object_or_404(ProductAlternativeUnitPrice.objects.filter(product=product,unit=unit))
                        cost = unit_instanse.cost
                    exact_qty = get_exact_qty(qty,unit)
                    tax_amount = qty * price * product.tax / 100
                    tax_amount = Decimal(format(tax_amount, '.2f'))
                    total_tax_amount += tax_amount 
                    total_discount_amount += discount_amount

                    product_tax_amount = price * product.tax / 100
                    tax_added_price = price + product_tax_amount

                    if str(product.pk) in items:
                        q = items[str(product.pk)]["qty"]
                        items[str(product.pk)]["qty"] = q + qty

                        d = items[str(product.pk)]["discount_amount"]
                        items[str(product.pk)]["discount_amount"] = d + discount_amount

                        t = items[str(product.pk)]["tax_amount"]
                        items[str(product.pk)]["tax_amount"] = t + tax_amount

                    else:
                        dic = {
                            "qty" : qty,
                            "cost" : cost,
                            "price" : price,
                            "tax_amount" : tax_amount,
                            "discount_amount" :discount_amount,
                            "tax_added_price" : tax_added_price,
                            "unit" :unit.pk
                        }
                        items[str(product.pk)] = dic

            stock_ok = True
            error_message = ''
            for key, value in items.iteritems():
                product = Product.objects.get(pk=key)
                prev_qty = 0
                prev_exact_qty = 0
                if SaleItem.objects.filter(sale=instance,product=product).exists():
                    prev_qty = SaleItem.objects.get(sale=instance,product=product).qty
                    prev_unit = SaleItem.objects.get(sale=instance,product=product).unit
                    prev_exact_qty = get_exact_qty(prev_qty,prev_unit)                    
                stock = product.stock + prev_exact_qty

                unit = Measurement.objects.get(pk=value['unit'],shop=current_shop)
                exact_qty = get_exact_qty(value['qty'],unit) 
                if exact_qty > stock:
                    stock_ok = False
                    error_message += "%s has only %s in stock, " %(product.name,str(stock))      
                
            if stock_ok:       
                
                customer_name = form.cleaned_data['customer_name']
                customer_address = form.cleaned_data['customer_address']
                customer_email = form.cleaned_data['customer_email']
                customer_phone = form.cleaned_data['customer_phone']
                customer = form.cleaned_data['customer']
                customer_state = form.cleaned_data['customer_state']
                customer_gsttin = form.cleaned_data['customer_gsttin']

                if not customer:
                    auto_id = get_auto_id(Customer)
                    a_id = get_a_id(Customer,request)

                    customer = Customer(
                        name = customer_name,
                        email = customer_email,
                        phone = customer_phone,
                        address = customer_address,
                        shop = current_shop,
                        first_time_credit = 0,
                        first_time_debit = 0,
                        credit = 0,
                        debit = 0,
                        creator = request.user,
                        updator = request.user,
                        auto_id = auto_id,
                        a_id = a_id,
                        state = customer_state,
                        gst_tin = customer_gsttin
                    )
                    customer.save()

                gst_type = "sgst"
                if not customer.state == current_shop.state:
                    gst_type = "igst"

                #update sale
                special_discount = form.cleaned_data['special_discount']
                payment_received = form.cleaned_data['payment_received']
                date = form.cleaned_data['time']

                #creating GST Invoice id
                customer_gstin_invoice_id = None
                customer_non_gstn_invoice_id = None
                if customer:
                    last_sale = Sale.objects.order_by("-date_added")[:1]
                    customer_non_gstn_invoice_id = 1
                    for l in last_sale:
                        if l.customer_non_gstn_invoice_id:
                            customer_non_gstn_invoice_id = int(l.customer_non_gstn_invoice_id) + 1

                data1 = form.save(commit=False)
                data1.updator = request.user
                data1.date_updated = datetime.datetime.now()
                data1.total_discount_amount = total_discount_amount
                data1.total_tax_amount = total_tax_amount
                data1.gst_type = gst_type
                data1.customer_gstin_invoice_id = customer_gstin_invoice_id
                data1.customer_non_gstn_invoice_id = customer_non_gstn_invoice_id
                data1.save() 
                all_subtotal = 0
                
                #delete previous items and update stock
                previous_sale_items = SaleItem.objects.filter(sale=instance)
                for p in previous_sale_items: 
                    prev_qty = p.qty
                    prev_unit = p.unit
                    prev_exact_qty = get_exact_qty(prev_qty,prev_unit)                        
                    update_sock(p.product.pk,prev_exact_qty,"increase")
                    print prev_exact_qty                    
                previous_sale_items.delete()
                
                #save items
                for key, value in items.iteritems():
                    product = Product.objects.get(pk=key)
                    qty = value["qty"]
                    price = value["price"]
                    cost = value["cost"]
                    tax = product.tax
                    discount = product.discount
                    tax_amount = value["tax_amount"]
                    discount_amount = value["discount_amount"]
                    tax_added_price = value["tax_added_price"]
                    unit = value["unit"]                    
                    subtotal = (qty * price) - discount_amount + tax_amount
                    unit = Measurement.objects.get(pk=unit)
                    exact_qty = get_exact_qty(qty,unit)
                    all_subtotal += subtotal
                    SaleItem(
                        sale = data1,
                        product = product,
                        qty = qty,
                        cost = cost,
                        price = price,
                        tax = tax,
                        discount = discount,
                        tax_amount = tax_amount,
                        discount_amount = discount_amount,
                        subtotal = subtotal,
                        unit = unit,
                        tax_added_price = tax_added_price
                    ).save()
                    
                    update_sock(product.pk,exact_qty,"decrease")

                credit=0
                debit=0
                if customer:
                    if not customer.is_system_generated:
                        credit = customer.credit
                        debit = customer.debit
                    
                # total = all_subtotal - special_discount + credit - debit
                # total_total = total
                # if current_shop.remove_previous_balance_from_bill:
                #     total_total = all_subtotal - special_discount
                total_total = all_subtotal - special_discount
                total = all_subtotal - special_discount
                rounded_total = Decimal(round(total))
                balance = rounded_total - payment_received
                extra = Decimal(round(total)) - total
                round_off = Decimal(format(extra, '.2f'))
                balance = Decimal(balance)
                this_sale_balance = (all_subtotal - special_discount) - payment_received - instance.collected_amount

                Sale.objects.filter(pk=data1.pk).update(subtotal=all_subtotal,total=total_total,balance=this_sale_balance,round_off=round_off)
                
                if not data1.customer.is_system_generated:
                    #update credit
                    update_customer_credit_debit(data1.customer.pk,"debit",old_balance)
                    
                    if balance > 0:
                        balance = balance
                        update_customer_credit_debit(data1.customer.pk,"credit",balance)
                    elif balance < 0:
                        balance = abs(balance)
                        update_customer_credit_debit(data1.customer.pk,"debit",balance)

                #update account balance
                if Transaction.objects.filter(sale=pk).exists(): 
                    if old_transaction.cash_account:
                        balance = old_transaction.cash_account.balance - old_paid
                        CashAccount.objects.filter(pk=old_transaction.cash_account.pk,shop=current_shop).update(balance=balance)
                    else  :
                        balance = old_transaction.bank_account.balance - old_paid
                        BankAccount.objects.filter(pk=old_transaction.bank_account.pk,shop=current_shop).update(balance=balance)
                
                transaction_mode = transaction_form.cleaned_data['transaction_mode']
                payment_to = transaction_form.cleaned_data['payment_to']       
                transaction_category = transaction_form.cleaned_data['transaction_category']
                amount = form.cleaned_data['payment_received']
                transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='sale_payment',category_type="income",is_deleted=False)[:1])
            
                #create Transaction
                data = transaction_form.save(commit=False)
                
                if transaction_mode == "cash":
                    data.payment_mode = None
                    data.payment_to = "cash_account"
                    data.bank_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                    balance = balance + amount
                    CashAccount.objects.filter(pk=data.cash_account.pk,shop=current_shop).update(balance=balance)
                elif transaction_mode == "bank":
                    balance = balance + amount
                    BankAccount.objects.filter(pk=data.bank_account.pk,shop=current_shop).update(balance=balance)
                    payment_mode = transaction_form.cleaned_data['payment_mode'] 
                    if payment_mode == "cheque_payment":
                        is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed'] 
                        data.card_details = None

                        if not is_cheque_withdrawed:
                            data.payment_to = None
                            data.bank_account = None
                            data.cash_account = None

                    elif payment_mode == "internet_banking":
                        data.payment_to = "bank_account"
                        data.cash_account = None
                        data.cheque_details = None
                        data.card_details = None
                        data.is_cheque_withdrawed = False
                    
                    elif payment_mode == "card_payment":
                        data.payment_to = "bank_account"
                        data.cash_account = None
                        data.cheque_details = None
                        data.is_cheque_withdrawed = False
                
                    if payment_to == "cash_account":
                        data.bank_account = None
                    elif payment_to == "bank_account":
                        data.cash_account = None
                    
                if not transaction_category == "credit":
                    data.updator = request.user
                    data.transaction_type = "income"
                    data.transaction_category = transaction_categories
                    data.amount = amount
                    data.date = date
                    data.shop = current_shop
                    data.sale = data1
                    data.save() 

                    response_data = {
                        "status" : "true",
                        "title" : "Successfully Updated",
                        "message" : "Sale Successfully Updated.",
                        "redirect" : "true",
                        "redirect_url" : reverse('sales:sale',kwargs={'pk':data1.pk})
                    }   
            else:
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Out of Stock",
                    "message" : error_message
                } 
        else:
            message = generate_form_errors(form,formset=False)     
            message += generate_form_errors(sale_item_formset,formset=True)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:        
        form = SaleForm(instance=instance)
        transaction_form = TransactionForm(instance=transaction) 
        sale_item_formset = SaleItemFormset(prefix='sale_item_formset',instance=instance)
        for item in sale_item_formset:
            item.fields['unit'].queryset = Measurement.objects.filter(shop=current_shop)
            item.fields['product'].queryset = Product.objects.filter(shop=current_shop)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "title" : "Edit Sale #: " + str(instance.auto_id),
            "instance" : instance,
            "url" : reverse('sales:edit',kwargs={'pk':instance.pk}),
            "sale_item_formset" : sale_item_formset,
            "redirect" : True,
         
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'sales/entry.html', context)


def delete_sale_fun(instance):
    old_balance = instance.balance 
    old_paid = instance.payment_received
    #update credit debit
    if not instance.customer.is_system_generated:
        update_customer_credit_debit(instance.customer.pk,"debit",old_balance)
    
    #update stock
    sale_items = SaleItem.objects.filter(sale=instance)
    for p in sale_items:
        qty = p.qty
        unit = p.unit
        exact_qty = get_exact_qty(qty,unit)                    
        update_sock(p.product.pk,exact_qty,"increase") 

    #update account balance    
    if Transaction.objects.filter(sale=instance,transaction_category__name='sale_payment').exists():
        old_transaction = get_object_or_404(Transaction.objects.filter(sale=instance,transaction_category__name='sale_payment'))
        if old_transaction.cash_account:
            balance = old_transaction.cash_account.balance - old_paid
            CashAccount.objects.filter(pk=old_transaction.cash_account.pk).update(balance=balance)
        else  :
            balance = old_transaction.bank_account.balance - old_paid
            BankAccount.objects.filter(pk=old_transaction.bank_account.pk).update(balance=balance)
        
        old_transaction.is_deleted=True
        old_transaction.save()
        
    instance.is_deleted=True
    instance.save()
    
    
@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_sale'])
def delete(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Sale.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    delete_sale_fun(instance)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Sale Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('sales:sales')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_sale'])
def delete_selected_sales(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Sale.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
            delete_sale_fun(instance)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Sale(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('sales:sales')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def print_sale(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Sale.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    sale_items = SaleItem.objects.filter(sale=instance)

    to_word = inflect.engine()
    total_in_words = to_word.number_to_words(instance.total)
    
    # tax_categories = TaxCategory.objects.filter(shop=current_shop,is_deleted=False)
    # tax_percentage_dict = {}
    # for tax in tax_categories:
    #         items = sale_items.filter(product__tax_category=tax)

    #         tax_amount = 0
    #         if items :
    #             tax_amount = items.aggregate(tax_amount=Sum('tax_amount')).get("tax_amount",0)
    #         if tax_amount > 0:
    #             if instance.gst_type == "sgst":
    #                 each_amount = tax_amount/2
    #                 each_percentage = tax.tax/2
    #                 tax_values_dict = {}
    #                 tax_values_dict[str(each_percentage)] = each_amount
    #                 tax_percentage_dict[str(tax.tax)] = tax_values_dict
    # print tax_percentage_dict

    if instance.sale_type == "wholesale_with_customer":
        sale_items_list = []
        total = 0
        for item in sale_items:
            tax_amount = item.product.price * (item.tax/100)
            subtotal = (item.product.price - item.discount ) * item.qty 
            total += subtotal
            dicts = {
                "product" : item.product.name,
                "qty" : item.qty,
                "price" : item.product.price,
                "tax" : item.tax,
                "discount" : item.discount,
                "subtotal" : subtotal
            }
            sale_items_list.append(dicts)

        saved_amount = instance.special_discount+instance.total_discount_amount
        cash_payment = total - instance.balance
        context = {
            'total_in_words':total_in_words,
            "instance" : instance,
            "title" : "Sale : #" + str(instance.auto_id),
            "single_page" : True,
            "sale_items" : sale_items_list,
            "current_shop" : get_current_shop(request),
            "saved_amount" : saved_amount,
            "total" : total,
            "cash_payment" : cash_payment,
            
            "is_need_bootstrap_growl" : True,
            "is_need_wave_effect" : True,
        }

        template_name = 'sales/' + current_shop.bill_print_type + 'wc.html'
        return render(request,template_name,context)

    else:
        saved_amount = instance.special_discount+instance.total_discount_amount
        context = {
            "total_in_words":total_in_words,
            "instance" : instance,
            "title" : "Sale : #" + str(instance.auto_id),
            "single_page" : True,
            "sale_items" : sale_items,
            "current_shop" : get_current_shop(request),
            "saved_amount" : saved_amount,
            "is_need_wave_effect" : True,

            "is_need_bootstrap_growl" : True,
        }
        template_name = 'sales/' + current_shop.bill_print_type + '.html'
        return render(request,template_name,context)


@check_mode
@login_required
@shop_required
def email_sale(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Sale.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    if request.method == 'POST':
        form = EmailSaleForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            content = form.cleaned_data['content']
            content += "<br />"
            link = request.build_absolute_uri(reverse('sales:print',kwargs={'pk':pk}))
            content += '<a href="%s">%s</a>' %(link,link)
            
            template_name = 'email/email.html'
            subject = "Purchase Details (#%s) | %s" %(str(instance.auto_id),current_shop.name)          
            context = {
                'name' : name,
                'subject' : subject,
                'content' : content,
                'email' : email
            }
            html_content = render_to_string(template_name,context) 
            send_email(email,subject,content,html_content) 
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Sent",
                "message" : "Sale Successfully Sent.",
                "redirect" : "true",
                "redirect_url" : reverse('sales:sale',kwargs={'pk':pk})
            } 
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        email = instance.customer.email
        name = instance.customer.name
        content = "Thanks for your purchase from %s. Please follow the below link for your purchase details." %current_shop.name
        
        form = EmailSaleForm(initial={'name' : name, 'email' : email, 'content' : content})
        
        context = {
            "instance" : instance,
            "title" : "Email Sale : #" + str(instance.auto_id),
            "single_page" : True,
            'form' : form,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'sales/email_sale.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_collect_amount'])
def create_collect_amount(request):  
    current_shop = get_current_shop(request)
    if request.method == "POST":
        
        form = CollectAmountForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        if form.is_valid() and transaction_form.is_valid():

            auto_id = get_auto_id(CollectAmount)
            a_id = get_a_id(CollectAmount,request)
            #get values from form
            collect_amount = form.cleaned_data['collect_amount']
            customer = form.cleaned_data['customer']
            date = form.cleaned_data['date']

            instance = Customer.objects.get(pk=customer.pk,is_deleted=False)
            balance = instance.credit
            
            remaining_balance = balance - collect_amount
            credit = remaining_balance
            debit = customer.debit
            if remaining_balance <= 0:
                credit = 0
                if customer.debit > 0:
                    debit = abs(remaining_balance) + customer.debit
                else:
                    debit = abs(remaining_balance)
                remaining_balance = 0
            
            data1 = form.save(commit=False)
            data1.creator = request.user
            data1.updator = request.user
            data1.auto_id = auto_id
            data1.a_id = a_id
            data1.balance = balance
            data1.remaining_balance = remaining_balance
            data1.shop = current_shop
            data1.save()
            customer = Customer.objects.filter(pk= data1.customer.pk)
            if customer.exists():
                customer.update(credit=credit,debit=debit)

            if Sale.objects.filter(is_deleted=False,customer=data1.customer).exists():                
                latest_sale = Sale.objects.filter(is_deleted=False,customer=data1.customer,shop=current_shop).latest('date_added')
                collected_amount = latest_sale.collected_amount + collect_amount
                latest_sale = Sale.objects.filter(pk=latest_sale.pk)
                if remaining_balance < 0:
                    remaining_balance = -debit
                latest_sale.update(collected_amount=collected_amount)            

            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']       
            transaction_category = transaction_form.cleaned_data['transaction_category']
            amount = form.cleaned_data['collect_amount']
            transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='customer_payment',category_type="income",is_deleted=False)[:1])
        
            #create income
            data = transaction_form.save(commit=False)
            
            if transaction_mode == "cash":
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                balance = data.cash_account.balance
                balance = balance + amount
                CashAccount.objects.filter(pk=data.cash_account.pk,shop=current_shop).update(balance=balance)
            elif transaction_mode == "bank":
                balance = 0
                balance = data.bank_account.balance
                balance = balance + amount
                BankAccount.objects.filter(pk=data.bank_account.pk,shop=current_shop).update(balance=balance)
                payment_mode = transaction_form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed'] 
                    data.card_details = None

                    if not is_cheque_withdrawed:
                        data.payment_to = None
                        data.bank_account = None
                        data.cash_account = None

                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None
                
            if not transaction_category == "credit":
                data.auto_id = get_auto_id(Transaction) 
                data.a_id = get_a_id(Transaction,request)            
                data.creator = request.user
                data.updator = request.user
                data.transaction_type = "income"
                data.transaction_category = transaction_categories
                data.amount = amount
                data.date = date
                if Sale.objects.filter(is_deleted=False,customer=data1.customer).exists():
                    latest_sale = Sale.objects.filter(is_deleted=False,customer=data1.customer,shop=current_shop).latest('date_added')
                    data.sale = latest_sale
                data.collect_amount = data1
                data.shop = current_shop
                data.save()    
           
            response_data = {
                "status" : "true",
                "title" : "Succesfully Created",
                "redirect" : "true",
                "redirect_url" : reverse('sales:collect_amount',kwargs = {'pk' :data1.pk}),
                "message" : "Amount Collected Successfully."
            }
        else:
            message = generate_form_errors(form,formset=False)        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }            
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = CollectAmountForm()
        transaction_form = TransactionForm()
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "title" : "Amount Collection",
            "is_create_page" : True,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            
        }
        return render(request, 'sales/entry_collect_amount.html', context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_modify_collect_amount'],allow_self=True,model=CollectAmount)
def edit_collect_amount(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(CollectAmount.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    transaction = get_object_or_404(Transaction.objects.filter(collect_amount=instance,shop=current_shop)) 

    if request.method == "POST":
        response_data = {}  
        old_collected_amount = instance.collect_amount
        balance = 0
        form = CollectAmountForm(request.POST,instance=instance)
        transaction_form = TransactionForm(request.POST,instance=transaction)
        if form.is_valid() and transaction_form.is_valid(): 
            
            #initialize customer credit
            
            update_customer_credit_debit(instance.customer.pk,"credit",old_collected_amount)
            amount = form.cleaned_data['collect_amount']
            date = form.cleaned_data['date']
            data1 = form.save(commit=False)
            data1.updator = request.user
            data1.date_updated = datetime.datetime.now()
            data1.save()
            #update customer credit
            update_customer_credit_debit(instance.customer.pk,"debit",amount)

            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']       
            transaction_category = transaction_form.cleaned_data['transaction_category']            
            transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='customer_payment',category_type="income",is_deleted=False)[:1])
            
            #update account balance
            if Transaction.objects.filter(collect_amount=pk).exists(): 
                if transaction.cash_account:
                    balance = transaction.cash_account.balance - old_collected_amount
                    CashAccount.objects.filter(pk=transaction.cash_account.pk,shop=current_shop).update(balance=balance)
                else  :
                    balance = transaction.bank_account.balance - old_collected_amount
                    BankAccount.objects.filter(pk=transaction.bank_account.pk,shop=current_shop).update(balance=balance)
            #create transaction
            data = transaction_form.save(commit=False)
            
            if transaction_mode == "cash":
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                balance = balance + amount
                CashAccount.objects.filter(pk=data.cash_account.pk,shop=current_shop).update(balance=balance)
            elif transaction_mode == "bank":
                balance = balance + amount
                BankAccount.objects.filter(pk=data.bank_account.pk,shop=current_shop).update(balance=balance)
                payment_mode = transaction_form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed'] 
                    data.card_details = None

                    if not is_cheque_withdrawed:
                        data.payment_to = None
                        data.bank_account = None
                        data.cash_account = None

                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None
                
            if not transaction_category == "credit":
                data.updator = request.user
                data.transaction_type = "income"
                data.transaction_category = transaction_categories
                data.amount = amount
                data.date = date
                latest_sale = Sale.objects.filter(is_deleted=False,customer=data1.customer,shop=current_shop).latest('date_added')            
                data.sale = latest_sale
                data.collect_amount = data1
                data.shop = current_shop
                data.save()  

            response_data = {
                "status" : "true",
                "title" : "Succesfully Updated",
                "redirect" : "true",
                "redirect_url" : reverse('sales:collect_amount', kwargs = {'pk' :pk}),
                "message" : "Collect Amount Successfully Updated."
            }
        else:
            message = generate_form_errors(form,formset=False)        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = CollectAmountForm(instance=instance)
        transaction_form = TransactionForm(instance=transaction)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "title" : "Edit Collect Amount : " + str(instance.collect_amount),
            "instance" : instance,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'sales/entry_collect_amount.html', context)

    

@check_mode
@login_required
@shop_required
@permissions_required(['can_view_collect_amount'])
def collect_amounts(request):
    current_shop = get_current_shop(request)
    instances = CollectAmount.objects.filter(is_deleted=False,shop=current_shop)

    title = "Collect Amounts"
    
    #filter by query
    query = request.GET.get("q")
    if query:
        title = "Collect Amount (Query - %s)" % query
        instances = instances.filter(Q(collect_amount__icontains=query) | Q(date__icontains=query) | Q(customer__name__icontains=query))
        
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/collect_amounts.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_collect_amount'],allow_self=True,model=CollectAmount)
def collect_amount(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(CollectAmount.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    transaction = get_object_or_404(Transaction.objects.filter(collect_amount=instance,shop=current_shop))
    context = {
        "instance" : instance,
        "transaction" : transaction,
        "title" : "Collect Amount: " + str(instance.collect_amount),
        
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/collect_amount.html',context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_collect_amount'],allow_self=True,model=CollectAmount)
def delete_collect_amount(request,pk):
    current_shop = get_current_shop(request)
    collect_amount_obj = CollectAmount.objects.get(pk=pk)

    if Customer.objects.filter(pk=collect_amount_obj.customer.pk,is_deleted=False):
        credit = Customer.objects.get(pk=collect_amount_obj.customer.pk,is_deleted=False).credit + collect_amount_obj.collect_amount
        new_credit = credit + collect_amount_obj.collect_amount
        print credit
        print new_credit
        Customer.objects.filter(pk=collect_amount_obj.customer.pk,is_deleted=False).update(credit=credit)

        CollectAmount.objects.filter(pk=pk).update(is_deleted=True,shop=current_shop)
    
    response_data = {
        "status" : "true",
        "title" : "Succesfully Deleted",
        "redirect" : "true",
        "redirect_url" : reverse('sales:collect_amounts'),
        "message" : "Collect Amount Successfully Deleted."
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_collect_amount'])
def delete_selected_collect_amounts(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(CollectAmount.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
            CollectAmount.objects.filter(pk=pk).update(is_deleted=True)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected CollectAmount(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('sales:collect_amounts')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some Collect Amount first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def print_collected_amount(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(CollectAmount.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    context = {
        "instance" : instance,
        "title" : "Collected Amount : #" + str(instance.auto_id),
        "single_page" : True,
        "current_shop" : current_shop,

        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
    }
    return render(request,'sales/print_collected_amount.html',context)


def print_collected_amounts(request):
    current_shop = get_current_shop(request)
    today = datetime.date.today()
    title = "Sale payment"
    instances = CollectAmount.objects.filter(is_deleted=False,shop=current_shop)
    query = request.GET.get('q')
    if query :
        instances = instances.filter(Q(auto_id__icontains=query) | Q(customer__name__icontains=query))

    date = request.GET.get('date')
    date_error = "no"

    if date :
        try:
            date = datetime.datetime.strptime(date, '%d/%m/%Y').date()          
        except ValueError:
            date_error = "yes" 

    period = request.GET.get('period')

    filter_period = None
    if period:
        if period == "today" or period == "month" or period == "year":
            filter_period = period
    
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date') 
    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%d/%m/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%d/%m/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True


    if filter_period :
        if period == "today":
            title = "Purchases : Today"

            instances = instances.filter(date__year=today.year, date__month=today.month, date__day=today.day)
            total_purchases_created = instances.count()
                
        elif period == "month":
            title = "Purchases : This Month"
            instances = instances.filter(date__year=today.year, date__month=today.month)
            
        elif period == "year":
            title = "Purchases : This Year"
            instances = purchases.filter(date__year=today.year)
    elif filter_date_period:
        title = "Purchases : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(date__range=[from_date, to_date])
            total_purchases_created = instances.count()
    elif date:
        title = "Purchases : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(date__year=date.year, date__month=date.month, date__day=date.day)
    context = {
        "instances" : instances,
        "title" : title,
        "single_page" : True,
        "current_shop" : current_shop,

        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
    }
    return render(request,'sales/print_collected_amounts.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_customer_payment'])
def create_customer_payment(request):  
    current_shop = get_current_shop(request)
    if request.method == "POST":
        
        form = CustomerPaymentForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        if form.is_valid() and transaction_form.is_valid():

            auto_id = get_auto_id(CustomerPayment)
            a_id = get_a_id(CustomerPayment,request)
            paid_amount = form.cleaned_data['paid_amount']
            customer = form.cleaned_data['customer']
            date = form.cleaned_data['date']
            instance = Customer.objects.get(pk=customer.pk,is_deleted=False,shop=current_shop)
            balance = instance.debit
            
            remaining_balance = balance - paid_amount
            debit = remaining_balance
            credit = customer.credit
            if remaining_balance <= 0:
                debit = 0
                if customer.credit > 0:
                    credit = abs(remaining_balance) + customer.credit
                else:
                    credit = abs(remaining_balance)
                remaining_balance = 0
              
            #create staff
            data1 = form.save(commit=False)
            data1.creator = request.user
            data1.updator = request.user
            data1.auto_id = auto_id
            data1.a_id = a_id
            data1.balance = balance
            data1.remaining_balance = remaining_balance
            data1.shop = current_shop
            data1.save()
            
            customer = Customer.objects.filter(pk= data1.customer.pk)
            if customer.exists():
                customer.update(credit=credit,debit=debit)

            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']       
            transaction_category = transaction_form.cleaned_data['transaction_category']
            amount = form.cleaned_data['paid_amount']
            transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='customer_payment',category_type="expense",is_deleted=False)[:1])
        
            #create income
            data = transaction_form.save(commit=False)
            
            if transaction_mode == "cash":
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                balance = data.cash_account.balance
                balance = balance - amount
                CashAccount.objects.filter(pk=data.cash_account.pk,shop=current_shop).update(balance=balance)
            elif transaction_mode == "bank":
                balance = 0
                balance = data.bank_account.balance
                balance = balance - amount
                BankAccount.objects.filter(pk=data.bank_account.pk,shop=current_shop).update(balance=balance)
                payment_mode = transaction_form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed'] 
                    data.card_details = None

                    if not is_cheque_withdrawed:
                        data.payment_to = None
                        data.bank_account = None
                        data.cash_account = None

                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None
                
            if not transaction_category == "credit":
                data.auto_id = get_auto_id(Transaction) 
                data.a_id = get_a_id(Transaction,request)            
                data.creator = request.user
                data.updator = request.user
                data.transaction_type = "expense"
                data.transaction_category = transaction_categories
                data.amount = amount
                data.date = date
                data.customer_payment = data1                
                data.shop = current_shop
                data.save()    
           
            response_data = {
                "status" : "true",
                "title" : "Succesfully Created",
                "redirect" : "true",
                "redirect_url" : reverse('sales:customer_payments'),
                "message" : "Amount Paid Successfully."
            }
        else:
            message = generate_form_errors(form,formset=False)        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }            
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = CustomerPaymentForm()
        transaction_form = TransactionForm()
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)
        
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "title" : "Customer payment",
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_create_page" : True
            
        }
        return render(request, 'sales/entry_customer_payment.html', context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_modify_customer_payment'],allow_self=True,model=CustomerPayment)
def edit_customer_payment(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(CustomerPayment.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    transaction = get_object_or_404(Transaction.objects.filter(customer_payment=instance,shop=current_shop)) 

    if request.method == "POST":
        response_data = {}
        balance = 0
        old_paid_amount = instance.paid_amount 
        form = CustomerPaymentForm(request.POST,instance=instance)
        transaction_form = TransactionForm(request.POST,instance=transaction)
        if form.is_valid() and transaction_form.is_valid(): 
            
            #initialize customer debit
            update_customer_credit_debit(instance.customer.pk,"debit",old_paid_amount)
            amount = form.cleaned_data['paid_amount']
            date = form.cleaned_data['date']
            data1 = form.save(commit=False)
            data1.updator = request.user
            data1.date_updated = datetime.datetime.now()
            data1.save()
            update_customer_credit_debit(instance.customer.pk,"credit",amount)

            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']       
            transaction_category = transaction_form.cleaned_data['transaction_category']
            
            transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='customer_payment',category_type="expense",is_deleted=False)[:1])
            
            #update account balance
            if Transaction.objects.filter(customer_payment=pk).exists(): 
                if transaction.cash_account:
                    balance = transaction.cash_account.balance + old_paid_amount
                    print balance
                    CashAccount.objects.filter(pk=transaction.cash_account.pk,shop=current_shop).update(balance=balance)
                else  :
                    balance = transaction.bank_account.balance + old_paid_amount
                    BankAccount.objects.filter(pk=transaction.bank_account.pk,shop=current_shop).update(balance=balance)
            #create transaction
            data = transaction_form.save(commit=False)
            
            if transaction_mode == "cash":
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                balance = balance - amount
                CashAccount.objects.filter(pk=data.cash_account.pk,shop=current_shop).update(balance=balance)
            elif transaction_mode == "bank":
                balance = balance - amount
                BankAccount.objects.filter(pk=data.bank_account.pk,shop=current_shop).update(balance=balance)
                payment_mode = transaction_form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed'] 
                    data.card_details = None

                    if not is_cheque_withdrawed:
                        data.payment_to = None
                        data.bank_account = None
                        data.cash_account = None

                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None
                
            if not transaction_category == "credit":
                data.updator = request.user
                data.transaction_type = "expense"
                data.transaction_category = transaction_categories
                data.amount = amount
                data.date = date              
                data.customer_payment = data1
                data.shop = current_shop
                data.save()  

            response_data = {
                "status" : "true",
                "title" : "Succesfully Updated",
                "redirect" : "true",
                "redirect_url" : reverse('sales:customer_payments'),
                "message" : "Customer Payment Successfully Updated."
            }
        else:
            message = generate_form_errors(form,formset=False)        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = CustomerPaymentForm(instance=instance)
        transaction_form = TransactionForm(instance=transaction)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "title" : "Edit customer payment : " + str(instance.paid_amount),
            "instance" : instance,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'sales/entry_customer_payment.html', context)

    
@check_mode
@login_required
@shop_required
@permissions_required(['can_view_customer_payment'])
def customer_payments(request):
    current_shop = get_current_shop(request)
    instances = CustomerPayment.objects.filter(is_deleted=False,shop=current_shop)

    title = "Customer Payment"
    
    #filter by query
    query = request.GET.get("q")
    if query:
        title = "customer payment (Query - %s)" % query
        instances = instances.filter(Q(paid_amount__icontains=query) | Q(date__icontains=query) | Q(customer__name__icontains=query))
        
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/customer_payments.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_customer_payment'],allow_self=True,model=CustomerPayment)
def customer_payment(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(CustomerPayment.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    transaction = get_object_or_404(Transaction.objects.filter(customer_payment=instance,shop=current_shop))
    context = {
        "instance" : instance,
        "transaction" : transaction,
        "title" : "Customer Payment: " + str(instance.paid_amount),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/customer_payment.html',context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_customer_payment'],allow_self=True,model=CustomerPayment)
def delete_customer_payment(request,pk):
    CustomerPayment.objects.filter(pk=pk).update(is_deleted=True)
    
    response_data = {
        "status" : "true",
        "title" : "Succesfully Deleted",
        "redirect" : "true",
        "redirect_url" : reverse('sales:customer_payments'),
        "message" : "Paid Amount Successfully Deleted."
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_customer_payment'])
def delete_selected_customer_payments(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(CustomerPayment.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
            CustomerPayment.objects.filter(pk=pk).update(is_deleted=True)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Customer Payment(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('sales:customer_payments')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some Paid Amount first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def print_customer_payment(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(CustomerPayment.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    context = {
        "instance" : instance,
        "title" : "Customer Payment : #" + str(instance.auto_id),
        "single_page" : True,
        "current_shop" : current_shop,

        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
    }
    return render(request,'sales/print_customer_payment.html',context)


def print_customer_payments(request):
    current_shop = get_current_shop(request)
    today = datetime.date.today()
    title = "Sale payment"
    instances = CollectAmount.objects.filter(is_deleted=False,shop=current_shop)
    query = request.GET.get('q')
    if query :
        instances = instances.filter(Q(auto_id__icontains=query) | Q(customer__name__icontains=query))

    date = request.GET.get('date')
    date_error = "no"

    if date :
        try:
            date = datetime.datetime.strptime(date, '%d/%m/%Y').date()          
        except ValueError:
            date_error = "yes" 

    period = request.GET.get('period')

    filter_period = None
    if period:
        if period == "today" or period == "month" or period == "year":
            filter_period = period
    
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date') 
    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%d/%m/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%d/%m/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True


    if filter_period :
        if period == "today":
            title = "Purchases : Today"

            instances = instances.filter(date__year=today.year, date__month=today.month, date__day=today.day)
            total_purchases_created = instances.count()
                
        elif period == "month":
            title = "Purchases : This Month"
            instances = instances.filter(date__year=today.year, date__month=today.month)
            
        elif period == "year":
            title = "Purchases : This Year"
            instances = purchases.filter(date__year=today.year)
    elif filter_date_period:
        title = "Purchases : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(date__range=[from_date, to_date])
            total_purchases_created = instances.count()
    elif date:
        title = "Purchases : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(date__year=date.year, date__month=date.month, date__day=date.day)
    context = {
        "instances" : instances,
        "title" : title,
        "single_page" : True,
        "current_shop" : current_shop,

        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
    }
    return render(request,'sales/print_customer_payments.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_sale_return'])
def create_sale_return(request): 
    current_shop = get_current_shop(request)
    if request.method == "POST":
        response_data = {} 
        form = SaleReturnForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        if form.is_valid() and transaction_form.is_valid(): 
            message = ""
            is_ok = True
            products = request.POST.getlist('product_pk')
            units = request.POST.getlist('returned_product_unit')
            qtys = request.POST.getlist('returned_qty')
            status = request.POST.getlist('status')
            sale = form.cleaned_data['sale']
            date = form.cleaned_data['time']
            items = zip(products,units,qtys,status)
            returned_items = []
            for item in items:
                product_pk = item[0]
                try:
                    product_instance=Product.objects.get(pk=product_pk,shop=current_shop)
                except:
                    product_instance = None
                    message += "Invalid product selection"
                    is_ok = False
                unit_pk = item[1]
                try:
                    unit_instance=Measurement.objects.get(pk=unit_pk,shop=current_shop)
                except:
                    unit_instance = None
                    message += "Invalid unit selection"
                    is_ok = False
                qty = item[2]
                status = item[3]

                if SaleItem.objects.filter(sale=sale,product=product_instance,unit=unit_instance).exists():
                    sale_item = SaleItem.objects.get(sale=sale,product=product_instance,unit=unit_instance)
                    if Decimal(sale_item.actual_qty() ) >= Decimal(qty):
                        pr_ins = {
                            "product" : product_instance,
                            "unit" : unit_instance,
                            "qty" : qty,
                            "price" : sale_item.price,
                            "status" : status,
                        }
                        returned_items.append(pr_ins)
                        
                    else:
                        message += "Qty is greater than sold quantity."
                        is_ok = False
                else:
                    message += "Product with this unit is not in this sale. Please don't edit hidden values."
                    is_ok = False
                
            error_messages = ""
            title = ""

            if is_ok: 
                amount_returned = form.cleaned_data['amount_returned']
                #Save Sale Return
                customer =  sale.customer
                data = form.save(commit=False)
                data.creator = request.user
                data.updator = request.user
                data.shop = current_shop
                data.time = datetime.datetime.now()
                data.a_id = get_a_id(SaleReturn,request)
                data.auto_id = get_auto_id(SaleReturn)
                data.sale = sale
                data.customer = customer
                data.save()
                
                #Save Sale Return Item
                for f in returned_items:
                    
                    product = f['product']
                    unit = f['unit']
                    qty = f['qty']
                    status = f['status']
                    exact_qty = Decimal(get_exact_qty(qty,unit))
                    if unit.is_base == True :
                        cost = product.cost
                    else :
                        unit_instanse = get_object_or_404(ProductAlternativeUnitPrice.objects.filter(product=product,unit=unit))
                        cost = unit_instanse.cost
                    price = f['price']
                    SaleReturnItem(      
                        shop = current_shop,
                        sale_return = data,
                        product = product,
                        qty = qty,
                        unit = unit,
                        price = price,
                        cost = cost,
                        status = status
                    ).save()

                    returned_sale_items = SaleItem.objects.filter(sale=sale,is_deleted=False)
                    for item in returned_sale_items:
                        if item.product == product:
                            quantity = Decimal(qty)
                            saleitem_return_qty = SaleItem.objects.get(sale=sale,is_deleted=False,product=product)
                            retuned_item_quantity = saleitem_return_qty.return_qty
                            retuned_item_quantity += quantity
                            sale_return_amount = saleitem_return_qty.subtotal
                            SaleItem.objects.filter(sale=sale,is_deleted=False,product=product).update(return_qty=retuned_item_quantity)
                    
                    if status=='damaged':
                        if DamagedProduct.objects.filter(shop=current_shop,product=product,is_deleted=False).exists():
                            damaged = DamagedProduct.objects.get(product=product,unit=unit)
                            quantity = damaged.qty + Decimal(qty) 
                            DamagedProduct.objects.filter(product=product,unit=unit).update(qty=quantity)
                        else:
                            DamagedProduct(
                                shop = current_shop,
                                product = product,
                                qty = qty,
                                unit = unit,
                                price = price,
                                cost = cost,
                                sale_return = data,
                                status = status
                            ).save()

                    if status=='returnable' :  
                        update_sock(product.pk,exact_qty,"increase")

                #customer credit and debit updation
                returned_sale_items = SaleItem.objects.filter(sale=sale,is_deleted=False)
                returnable_amount = 0
                for f in returned_items:
                    product = f['product']
                    for item in returned_sale_items:
                        if product == item.product:
                            returnable_amount += item.subtotal
                            print returnable_amount
                credit = 0
                debit = 0
                if customer:
                    if not customer.is_system_generated:
                        credit = customer.credit
                        debit = customer.debit
                        returnable_amount = returnable_amount - credit + debit
                        #update credit
                        Customer.objects.filter(pk=customer.pk,is_deleted=False).update(credit=0,debit=0)
                        if returnable_amount == amount_returned:
                            re_amount = 0
                            update_customer_credit_debit(customer.pk,"credit",re_amount)
                        elif returnable_amount > amount_returned:
                            re_amount = returnable_amount - amount_returned
                            update_customer_credit_debit(customer.pk,"debit",re_amount)
                        else:
                            re_amount = returnable_amount - amount_returned
                            update_customer_credit_debit(customer.pk,"credit",abs(re_amount))

                            
                #create income
                transaction_mode = transaction_form.cleaned_data['transaction_mode']
                payment_to = transaction_form.cleaned_data['payment_to']       
                amount = form.cleaned_data['amount_returned']
                transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='salereturn_payment',category_type="expense",is_deleted=False)[:1])
                transaction_category = transaction_categories.name

                data1 = transaction_form.save(commit=False)

                if transaction_mode == "cash":
                    data1.payment_mode = None
                    data1.payment_to = "cash_account"
                    data1.bank_account = None
                    data1.cheque_details = None
                    data1.card_details = None
                    data1.is_cheque_withdrawed = False
                    balance = data1.cash_account.balance
                    balance = balance - amount
                    CashAccount.objects.filter(pk=data1.cash_account.pk,shop=current_shop).update(balance=balance)
                elif transaction_mode == "bank":
                    balance = 0
                    balance = data1.bank_account.balance
                    balance = balance - amount
                    BankAccount.objects.filter(pk=data1.bank_account.pk,shop=current_shop).update(balance=balance)
                    payment_mode = transaction_form.cleaned_data1['payment_mode'] 
                    if payment_mode == "cheque_payment":
                        is_cheque_withdrawed = transaction_form.cleaned_data1['is_cheque_withdrawed'] 
                        data1.card_details = None

                        if not is_cheque_withdrawed:
                            data1.payment_to = None
                            data1.bank_account = None
                            data1.cash_account = None

                    elif payment_mode == "internet_banking":
                        data1.payment_to = "bank_account"
                        data1.cash_account = None
                        data1.cheque_details = None
                        data1.card_details = None
                        data1.is_cheque_withdrawed = False
                    
                    elif payment_mode == "card_payment":
                        data1.payment_to = "bank_account"
                        data1.cash_account = None
                        data1.cheque_details = None
                        data1.is_cheque_withdrawed = False
                
                    if payment_to == "cash_account":
                        data1.bank_account = None
                    elif payment_to == "bank_account":
                        data1.cash_account = None

                if not transaction_category == "credit":
                    data1.auto_id = get_auto_id(Transaction) 
                    data1.a_id = get_a_id(Transaction,request)            
                    data1.creator = request.user
                    data1.updator = request.user
                    data1.transaction_type = "expense"
                    data1.transaction_category = transaction_categories
                    data1.amount = amount
                    data1.date = date
                    data1.sale_return = data
                    data1.shop = current_shop
                    data1.save()

                response_data['status'] = 'true'  
                response_data['title'] = "Successfully Created"       
                response_data['redirect'] = 'true' 
                response_data['redirect_url'] = reverse('sales:sale_return', kwargs = {'pk' : data.pk})
                response_data['message'] = "Sale Return Successfully Created."
            else:
                response_data['status'] = 'false'     
                response_data['title'] = "Error in input values"
                response_data['stable'] = "true"  
                response_data['message'] = message     
        else:
            response_data['status'] = 'false'
            response_data['stable'] = 'true'
            response_data['title'] = "Form validation error"
            
            message = ''            
            message += generate_form_errors(form,formset=False)                
            response_data['message'] = message
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = SaleReturnForm()
        transaction_form = TransactionForm()
        context = {
            "form" : form,
            "transaction_form" :transaction_form,
            "title" : "Create Sale Return",
            "redirect" : True,
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,

            "is_create_page" : True
        }
        return render(request, 'sales/returns/entry.html', context)
    
    
@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_sale_return'])
def edit_sale_return(request,pk): 
    current_shop = get_current_shop(request)
    if request.method == "POST":
        response_data = {} 
        form = SaleReturnForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        if form.is_valid() and transaction_form.is_valid(): 
            message = ""
            is_ok = True
            products = request.POST.getlist('product_pk')
            units = request.POST.getlist('returned_product_unit')
            qtys = request.POST.getlist('returned_qty')
            status = request.POST.getlist('status')
            sale = form.cleaned_data['sale']
            date = form.cleaned_data['time']
            items = zip(products,units,qtys,status)
            returned_items = []
            for item in items:
                product_pk = item[0]
                try:
                    product_instance=Product.objects.get(pk=product_pk,shop=current_shop)
                except:
                    product_instance = None
                    message += "Invalid product selection"
                    is_ok = False
                unit_pk = item[1]
                try:
                    unit_instance=Measurement.objects.get(pk=unit_pk,shop=current_shop)
                except:
                    unit_instance = None
                    message += "Invalid unit selection"
                    is_ok = False
                qty = item[2]
                status = item[3]

                if SaleItem.objects.filter(sale=sale,product=product_instance,unit=unit_instance).exists():
                    sale_item = SaleItem.objects.get(sale=sale,product=product_instance,unit=unit_instance)
                    if Decimal(sale_item.actual_qty() ) >= Decimal(qty):
                        pr_ins = {
                            "product" : product_instance,
                            "unit" : unit_instance,
                            "qty" : qty,
                            "price" : sale_item.price,
                            "status" : status,
                        }
                        returned_items.append(pr_ins)
                        
                    else:
                        message += "Qty is greater than sold quantity."
                        is_ok = False
                else:
                    message += "Product with this unit is not in this sale. Please don't edit hidden values."
                    is_ok = False
                
            error_messages = ""
            title = ""

            if is_ok: 
                amount_returned = form.cleaned_data['amount_returned']
                #Save Sale Return
                customer =  sale.customer
                data = form.save(commit=False)
                data.creator = request.user
                data.updator = request.user
                data.shop = current_shop
                data.time = datetime.datetime.now()
                data.a_id = get_a_id(SaleReturn,request)
                data.auto_id = get_auto_id(SaleReturn)
                data.sale = sale
                data.customer = customer
                data.save()
                
                #Save Sale Return Item
                for f in returned_items:
                    
                    product = f['product']
                    unit = f['unit']
                    qty = f['qty']
                    status = f['status']
                    exact_qty = Decimal(get_exact_qty(qty,unit))
                    if unit.is_base == True :
                        cost = product.cost
                    else :
                        unit_instanse = get_object_or_404(ProductAlternativeUnitPrice.objects.filter(product=product,unit=unit))
                        cost = unit_instanse.cost
                    price = f['price']
                    SaleReturnItem(      
                        shop = current_shop,
                        sale_return = data,
                        product = product,
                        qty = qty,
                        unit = unit,
                        price = price,
                        cost = cost,
                        status = status
                    ).save()
                    returned_sale_items = SaleItem.objects.filter(sale=sale,is_deleted=False)
                    returned_sale_total = 0
                    for item in returned_sale_items:
                        if item.product == product:
                            quantity = Decimal(qty)
                            saleitem_return_qty = SaleItem.objects.get(sale=sale,is_deleted=False,product=product)
                            retuned_item_quantity = saleitem_return_qty.return_qty
                            retuned_item_quantity += quantity
                            SaleItem.objects.filter(sale=sale,is_deleted=False,product=product).update(return_qty=retuned_item_quantity)
                    if status=='damaged':
                        DamagedProduct(
                            shop = current_shop,
                            product = product,
                            qty = qty,
                            unit = unit,
                            price = price,
                            cost = cost,
                            sale_return = data,
                        ).save()

                    if status=='returnable' :  
                        update_sock(product.pk,exact_qty,"increase")

                # update_customer_credit_debit(customer.pk,"debit",amount_returned)
                #create income
                transaction_mode = transaction_form.cleaned_data['transaction_mode']
                payment_to = transaction_form.cleaned_data['payment_to']       
                amount = form.cleaned_data['amount_returned']
                transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='salereturn_payment',category_type="expense",is_deleted=False)[:1])
                transaction_category = transaction_categories.name

                data1 = transaction_form.save(commit=False)

                if transaction_mode == "cash":
                    data1.payment_mode = None
                    data1.payment_to = "cash_account"
                    data1.bank_account = None
                    data1.cheque_details = None
                    data1.card_details = None
                    data1.is_cheque_withdrawed = False
                    balance = data1.cash_account.balance
                    balance = balance - amount
                    CashAccount.objects.filter(pk=data1.cash_account.pk,shop=current_shop).update(balance=balance)
                elif transaction_mode == "bank":
                    balance = 0
                    balance = data1.bank_account.balance
                    balance = balance - amount
                    BankAccount.objects.filter(pk=data1.bank_account.pk,shop=current_shop).update(balance=balance)
                    payment_mode = transaction_form.cleaned_data1['payment_mode'] 
                    if payment_mode == "cheque_payment":
                        is_cheque_withdrawed = transaction_form.cleaned_data1['is_cheque_withdrawed'] 
                        data1.card_details = None

                        if not is_cheque_withdrawed:
                            data1.payment_to = None
                            data1.bank_account = None
                            data1.cash_account = None

                    elif payment_mode == "internet_banking":
                        data1.payment_to = "bank_account"
                        data1.cash_account = None
                        data1.cheque_details = None
                        data1.card_details = None
                        data1.is_cheque_withdrawed = False
                    
                    elif payment_mode == "card_payment":
                        data1.payment_to = "bank_account"
                        data1.cash_account = None
                        data1.cheque_details = None
                        data1.is_cheque_withdrawed = False
                
                    if payment_to == "cash_account":
                        data1.bank_account = None
                    elif payment_to == "bank_account":
                        data1.cash_account = None

                if not transaction_category == "credit":
                    data1.auto_id = get_auto_id(Transaction) 
                    data1.a_id = get_a_id(Transaction,request)            
                    data1.creator = request.user
                    data1.updator = request.user
                    data1.transaction_type = "expense"
                    data1.transaction_category = transaction_categories
                    data1.amount = amount
                    data1.date = date
                    data1.sale_return = data
                    data1.shop = current_shop
                    data1.save()

                response_data['status'] = 'true'  
                response_data['title'] = "Successfully Created"       
                response_data['redirect'] = 'true' 
                response_data['redirect_url'] = reverse('sales:sale_return', kwargs = {'pk' : data.pk})
                response_data['message'] = "Sale Return Successfully Created."
            else:
                response_data['status'] = 'false'     
                response_data['title'] = "Error in input values"
                response_data['stable'] = "true"  
                response_data['message'] = message     
        else:
            response_data['status'] = 'false'
            response_data['stable'] = 'true'
            response_data['title'] = "Form validation error"
            
            message = ''            
            message += generate_form_errors(form,formset=False)                
            response_data['message'] = message 
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = SaleReturnForm()
        transaction_form = TransactionForm()
        context = {
            "form" : form,
            "transaction_form" :transaction_form,
            "title" : "Create Sale Return",
            "redirect" : True,
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'sales/returns/entry.html', context)
    
    
@check_mode
@login_required
@shop_required
@permissions_required(['can_view_sale_return'])
def sale_returns(request):
    current_shop = get_current_shop(request)
    instances = SaleReturn.objects.filter(shop=current_shop,is_deleted=False)
    
    customer = request.GET.get("customer")
    if customer:
        instances = instances.filter(Q(customer=customer))
        
    title = "Sale Returns"
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/returns/returns.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_sale_return'])
def sale_return(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(SaleReturn.objects.filter(pk=pk,shop=current_shop,is_deleted=False))
    sale_return_items=SaleReturnItem.objects.filter(shop=current_shop,sale_return=instance,is_deleted=False)
    
    context = {
        "instance" : instance,
        "sale_return_items" : sale_return_items,
        "title" : "Sale Return ",
        
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/returns/return.html',context)


def delete_sale_return_fun(instance):
    old_balance = instance.amount_returned 
    sale = instance.sale
    #update credit debit
    update_customer_credit_debit(instance.customer.pk,"credit",old_balance)
    
    #update stock 
    sale_return_items = SaleReturnItem.objects.filter(sale_return=instance)
    for p in sale_return_items:
        qty = p.qty
        unit = p.unit
        exact_qty = get_exact_qty(qty,unit)
        sale_item = get_object_or_404(SaleItem.objects.filter(sale=sale,product=p.product,unit=p.unit))
        return_qty = sale_item.return_qty
        return_qty = return_qty + exact_qty
        SaleItem.objects.filter(sale=sale,product=p.product,unit=p.unit).update(return_qty=return_qty)
        if p.is_damaged == False and p.is_returnable == True:                   
            update_sock(p.product.pk,exact_qty,"decrease")
        elif p.is_damaged ==True and p.is_returnable == True:
            DamagedProduct.objects.filter(sale_return=instance).update(is_deleted=True)
            ReturnableProduct.objects.filter(sale_return=instance).update(is_deleted=True)
        elif p.is_damaged == True:
            DamagedProduct.objects.filter(sale_return=instance).update(is_deleted=True)

    instance.is_deleted=True
    instance.save()
    
    
@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_sale_return'])
def delete_sale_return(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(SaleReturn.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    delete_sale_return_fun(instance)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "SaleReturn Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('sales:sale_returns')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_damaged_product'])
def create_damaged_product(request): 

    current_shop = get_current_shop(request)
    DamagedProductFormset = formset_factory(DamagedProductForm)
    
    if request.method == "POST":
        response_data = {} 
        damaged_form = DamagedForm(request.POST)
        damaged_product_formset = DamagedProductFormset(request.POST,prefix='damaged_product_formset')
        for f in damaged_product_formset:
            f.fields['product'].queryset = Product.objects.filter(shop=current_shop,is_deleted=False)
            
        if damaged_form.is_valid() and damaged_product_formset.is_valid():           
             
            data = damaged_form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.shop = current_shop
            data.a_id = get_a_id(Damaged,request)
            data.auto_id = get_auto_id(Damaged)
            data.save()

            for f in damaged_product_formset:
                
                product = f.cleaned_data['product']
                unit = f.cleaned_data['unit']
                qty = f.cleaned_data['qty']
                exact_qty = get_exact_qty(qty,unit)                
                price = f.cleaned_data['price']
                cost = f.cleaned_data['cost']

                if DamagedProduct.objects.filter(product=product,unit=unit).exists():
                    damaged = DamagedProduct.objects.get(product=product,unit=unit)
                    quantity = damaged.qty + qty 
                    DamagedProduct.objects.filter(product=product,unit=unit).update(qty=quantity)
                else:
                    dp=DamagedProduct(
                        shop = current_shop,
                        product = product,
                        qty = qty,
                        unit = unit,
                        price = price,
                        cost = cost,                   
                        )
                    dp.save()

                
                update_sock(product.pk,exact_qty,"decrease")
            
            response_data['status'] = 'true'     
            response_data['title'] = "Successfully Created"       
            response_data['redirect'] = 'true' 
            response_data['redirect_url'] = reverse('sales:damaged_products')
            response_data['message'] = "Damaged Product Successfully Created."
        else:
            response_data['status'] = 'false'
            response_data['stable'] = 'true'
            response_data['title'] = "Form validation error"
            
            message = ''            
            message += generate_form_errors(damaged_product_formset,formset=False)                
            response_data['message'] = message
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        damaged_form = DamagedForm()
        damaged_product_formset = DamagedProductFormset(prefix='damaged_product_formset')
        for form in damaged_product_formset:
            form.fields['product'].queryset = Product.objects.filter(shop=current_shop,is_deleted=False)
            form.fields['unit'].queryset = Measurement.objects.none()
            form.fields['unit'].label_from_instance = lambda obj: "%s" % (obj.code)
            
        context = {
            "form" : damaged_form,
            "damaged_product_formset" : damaged_product_formset,
            "title" : "Create Damaged Product",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "purchase_page" : True
        }
        return render(request, 'sales/returns/create_damaged_product.html', context)

    
    
    
@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_damaged_product'])
def edit_damaged_product(request,pk): 
    current_shop = get_current_shop(request)    
    damaged_product_instances = get_object_or_404(DamagedProduct.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    instance = damaged_product_instances.damaged
    damaged_products = DamagedProduct.objects.filter(damaged = instance,is_deleted=False,shop=current_shop)
    if DamagedProduct.objects.filter(is_deleted=False).exists():        
        extra = 0
    else:
        extra= 1  
        
    DamagedProductFormset = inlineformset_factory(
                                    Damaged, 
                                    DamagedProduct, 
                                    can_delete=True,
                                    extra=extra,
                                    fields = ['product','unit','qty','cost','price'],
                                    widgets = {
                                        'product': autocomplete.ModelSelect2(url='products:product_autocomplete',attrs={'data-placeholder': 'Product','data-minimum-input-length': 1},),
                                        'unit': Select(attrs={'class': 'required form-control selectpicker'}),
                                        'qty': TextInput(attrs={'class': 'required form-control number qty','placeholder' : 'Quantity'}),
                                        'cost': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Cost'}),
                                        'price': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Price'}),
                                        
                                    }
                                            )
    
    if request.method == "POST":
        response_data = {} 
        damaged_form = DamagedForm(request.POST,instance=instance)        
        damaged_product_formset = DamagedProductFormset(request.POST,prefix='damaged_product_formset',instance=instance)
        for f in damaged_product_formset:
            f.fields['product'].queryset = Product.objects.filter(shop=current_shop,is_deleted=False)
           
        if damaged_form.is_valid() and damaged_product_formset.is_valid(): 
            data = damaged_form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save

            #Deleting Initial Data

            for item in damaged_products:
                product = item.product
                unit = item.unit
                product_qty = item.qty
                exact_qty = get_exact_qty(product_qty,unit)
                damaged = item.damaged
                update_sock(product.pk,exact_qty,"increase")

                DamagedProduct.objects.filter(damaged=damaged, product=product,unit=unit,is_deleted=False).delete()
                
            # Save damaged product 
            for form in damaged_product_formset:
                product = form.cleaned_data['product']
                qty = form.cleaned_data['qty']
                unit = form.cleaned_data['unit']
                exact_qty = get_exact_qty(qty,unit)
                price = form.cleaned_data['price']
                cost = form.cleaned_data['cost']        
                form.save()
                damaged_product = get_object_or_404(DamagedProduct.objects.filter(damaged=data, product=product,unit=unit,is_deleted=False))                                
                update_sock(product.pk,exact_qty,"decrease")
            
            response_data['status'] = 'true'     
            response_data['title'] = "Successfully Updated"       
            response_data['redirect'] = 'true' 
            response_data['redirect_url'] = reverse('sales:damaged_product', kwargs = {'pk' : data.pk})
            response_data['message'] = "Damaged Product Successfully Updated."
        else:
            response_data['status'] = 'false'
            response_data['stable'] = 'true'
            response_data['title'] = "Form validation error"
            
            message = ''            
            message += generate_form_errors(form,formset=False)                
            response_data['message'] = form.errors
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        damaged_form = DamagedForm(instance=instance)        
        damaged_product_formset = DamagedProductFormset(prefix='damaged_product_formset',instance=instance)
        for form in damaged_product_formset:
            form.fields['product'].queryset = Product.objects.filter(shop=current_shop,is_deleted=False)
            form.fields['unit'].queryset = Measurement.objects.none()
            form.fields['unit'].label_from_instance = lambda obj: "%s" % (obj.code)
            
        context = {
            "form" : damaged_form,
            "damaged_product_formset" : damaged_product_formset,
            "title" : "Edit Damaged Product" ,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "purchase_page" : True           
        }
   
        return render(request, 'sales/returns/create_damaged_product.html', context)
    
    
@check_mode
@login_required
@shop_required
@permissions_required(['can_view_damaged_product'])
def damaged_products(request):
    current_shop = get_current_shop(request)
    instances = DamagedProduct.objects.filter(is_deleted=False,shop=current_shop)
    direct_added = DamagedProduct.objects.filter(is_deleted=False,sale_return__isnull=True ,product_return__isnull=True,shop=current_shop)
    
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(product__icontains=query))
        
    title = "Damaged products"
    context = {
        'title' : title,
        "instances" : instances,
        "direct_added" : direct_added,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_animations": True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/returns/damaged_products.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_damaged_product'])
def damaged_product(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(DamagedProduct.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    direct_added = get_object_or_404(DamagedProduct.objects.filter(pk=pk,is_deleted=False,sale_return__isnull=True,product_return__isnull=True,shop=current_shop))
    
    context = {
        "instance" : instance,
        "title" : "Damaged Product" + " - " + instance.product.name,
        "single_page" : True,
        "direct_added" : direct_added,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/returns/damaged_product.html',context)   


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_damaged_product'])
def delete_damaged_product(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(DamagedProduct.objects.filter(pk=pk,shop=current_shop))
    product = instance.product
    unit = instance.unit
    qty = instance.qty
    if Product.objects.filter(is_deleted=False,pk=product.pk).exists():
        product = get_object_or_404(Product.objects.filter(is_deleted=False,pk=product.pk))
        stock = qty + product.stock
        Product.objects.filter(is_deleted=False,pk=product.pk).update(stock=stock)
    DamagedProduct.objects.filter(pk=pk,shop=current_shop).update(is_deleted=True)
    
    
    response_data = {}
    response_data['status'] = 'true'        
    response_data['title'] = "Successfully Deleted"       
    response_data['redirect'] = 'true' 
    response_data['redirect_url'] = reverse('sales:damaged_products')
    response_data['message'] = "Damaged Product Successfully Deleted."
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_returnable_product'])
def returnable_product(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(ReturnableProduct.objects.filter(pk=pk,shop=current_shop,is_deleted=False))
   
    context = {
        "instance" : instance,
        "title" : "Returnable Product",
        
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,    
    }
    return render(request,'sales/returns/returnable_product.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_returnable_product'])
def returnable_products(request): 
    current_shop = get_current_shop(request)
    instances = ReturnableProduct.objects.filter(shop=current_shop,is_deleted=False,is_returned=False)
    
    context = {
        "instances" : instances,
        "title" : "Returnable Products",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'sales/returns/returnable_products.html', context)


@check_mode
@ajax_required
@login_required
@permissions_required(['can_delete_returnable_product'])
def delete_returnable_product(request,pk):
    current_shop = get_current_shop(request)
    ReturnableProduct.objects.filter(pk=pk).update(is_deleted=True,shop=current_shop)
    
    response_data = {
        "status" : "true",
        "title" : "Succesfully Deleted",
        "redirect" : "true",
        "redirect_url" : reverse('sales:returnable_products'),
        "message" : "Returnable Product Successfully Deleted."
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_returnable_product'])
def delete_selected_returnable_products(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(ReturnableProduct.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
            ReturnableProduct.objects.filter(pk=pk).update(is_deleted=True)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected ReturnableProduct(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('sales:returnable_products')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some ReturnableProduct first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_product_return'])
def create_product_return(request):
    current_shop = get_current_shop(request)
    damaged_products = DamagedProduct.objects.filter(is_deleted=False,is_returned=False)
    if request.method == "POST":
        response_data = {} 
        form = ProductReturnForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        if form.is_valid() and transaction_form.is_valid(): 
            message = ""
            is_ok = True
            products = request.POST.getlist('product_pk')
            units = request.POST.getlist('returned_product_unit')
            qtys = request.POST.getlist('returned_qty')
            vendor = form.cleaned_data['vendor']
            date = form.cleaned_data['time']
            items = zip(products,units,qtys)
            returned_items = []
            for item in items:
                product_pk = item[0]
                try:
                    product_instance= Product.objects.get(pk=product_pk,shop=current_shop)
                except:
                    product_instance = None
                    message += "Invalid product selection"
                    is_ok = False
                unit_pk = item[1]
                try:
                    unit_instance=Measurement.objects.get(pk=unit_pk,shop=current_shop)
                except:
                    unit_instance = None
                    message += "Invalid unit selection"
                    is_ok = False
                qty = item[2]
                if DamagedProduct.objects.filter(product=product_instance,unit=unit_instance).exists():
                    return_item_product = DamagedProduct.objects.get(product=product_instance,unit=unit_instance)
                    print qty
                    print return_item_product.qty
                    if return_item_product.qty >= Decimal(qty):
                        pr_ins = {
                            "product" : return_item_product,
                            "unit" : unit_instance,
                            "qty" : qty,
                            "price" : return_item_product.price,
                        }
                        returned_items.append(pr_ins)
                        
                    else:
                        message += "Qty is greater than damaged quantity."
                        is_ok = False
                else:
                    message += "Product with this unit is not in list. Please don't edit hidden values."
                    is_ok = False
            error_messages = ""
            title = ""

            if is_ok: 
                amount_returned = form.cleaned_data['amount_returned']
                #Save Product Return
                data = form.save(commit=False)
                data.creator = request.user
                data.updator = request.user
                data.shop = current_shop
                data.time = datetime.datetime.now()
                data.a_id = get_a_id(ProductReturn,request)
                data.auto_id = get_auto_id(ProductReturn)
                data.vendor = vendor
                data.save()
                #Save Sale Return Item
                for f in returned_items:
                    product = f['product']
                    unit = f['unit']
                    qty = f['qty']
                    exact_qty = Decimal(get_exact_qty(qty,unit))
                    if unit.is_base == True :
                        cost = product.cost
                    else :
                        unit_instanse = get_object_or_404(ProductAlternativeUnitPrice.objects.filter(product=product,unit=unit))
                        cost = unit_instanse.cost
                    price = f['price']
                    ProductReturnItem(      
                        shop = current_shop,
                        product_return = data,
                        product = product,
                        qty = qty,
                        unit = unit,
                        price = price,
                        cost = cost
                    ).save()
                    returned_product = get_object_or_404(DamagedProduct.objects.filter(is_deleted=False,product=product_instance))
                    quantity = Decimal(qty)
                    if returned_product.qty == quantity:
                        DamagedProduct.objects.filter(is_deleted=False,product=product_instance).update(is_returned=True)
                    else:
                        get_dproduct = get_object_or_404(DamagedProduct.objects.filter(is_deleted=False,product=product_instance))
                        quan = get_dproduct.qty - quantity
                        DamagedProduct.objects.filter(is_deleted=False,product=product_instance).update(qty=quantity)
                # update_customer_credit_debit(customer.pk,"debit",amount_returned)
                #create income
                transaction_mode = transaction_form.cleaned_data['transaction_mode']
                payment_to = transaction_form.cleaned_data['payment_to']       
                amount = form.cleaned_data['amount_returned']
                transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='purchase_payment',category_type="income",is_deleted=False)[:1])
                print transaction_categories
                transaction_category = transaction_categories.name

                data1 = transaction_form.save(commit=False)

                if transaction_mode == "cash":
                    data1.payment_mode = None
                    data1.payment_to = "cash_account"
                    data1.bank_account = None
                    data1.cheque_details = None
                    data1.card_details = None
                    data1.is_cheque_withdrawed = False
                    balance = data1.cash_account.balance
                    balance = balance - amount
                    CashAccount.objects.filter(pk=data1.cash_account.pk,shop=current_shop).update(balance=balance)
                elif transaction_mode == "bank":
                    balance = 0
                    balance = data1.bank_account.balance
                    balance = balance - amount
                    BankAccount.objects.filter(pk=data1.bank_account.pk,shop=current_shop).update(balance=balance)
                    payment_mode = transaction_form.cleaned_data1['payment_mode'] 
                    if payment_mode == "cheque_payment":
                        is_cheque_withdrawed = transaction_form.cleaned_data1['is_cheque_withdrawed'] 
                        data1.card_details = None

                        if not is_cheque_withdrawed:
                            data1.payment_to = None
                            data1.bank_account = None
                            data1.cash_account = None

                    elif payment_mode == "internet_banking":
                        data1.payment_to = "bank_account"
                        data1.cash_account = None
                        data1.cheque_details = None
                        data1.card_details = None
                        data1.is_cheque_withdrawed = False
                    
                    elif payment_mode == "card_payment":
                        data1.payment_to = "bank_account"
                        data1.cash_account = None
                        data1.cheque_details = None
                        data1.is_cheque_withdrawed = False
                
                    if payment_to == "cash_account":
                        data1.bank_account = None
                    elif payment_to == "bank_account":
                        data1.cash_account = None

                if not transaction_category == "credit":
                    data1.auto_id = get_auto_id(Transaction) 
                    data1.a_id = get_a_id(Transaction,request)            
                    data1.creator = request.user
                    data1.updator = request.user
                    data1.transaction_type = "income"
                    data1.transaction_category = transaction_categories
                    data1.amount = amount
                    data1.date = date
                    data1.purchase_return = data
                    data1.shop = current_shop
                    data1.save()                
                response_data['status'] = 'true'  
                response_data['title'] = "Successfully Created"       
                response_data['redirect'] = 'true' 
                response_data['redirect_url'] = reverse('sales:product_return', kwargs = {'pk' : data.pk})
                response_data['message'] = "Sale Return Successfully Created."
            else:
                response_data['status'] = 'false'     
                response_data['title'] = "Error in input values"
                response_data['stable'] = "true"  
                response_data['message'] = message     
        else:
            print form.errors
            response_data['status'] = 'false'
            response_data['stable'] = 'true'
            response_data['title'] = "Form validation error"
            
            message = ''            
            message += generate_form_errors(form,formset=False)                
            response_data['message'] = message 
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = ProductReturnForm()
        transaction_form = TransactionForm()
        context = {
            "form" : form,
            "transaction_form" :transaction_form,
            "damaged_products" : damaged_products,
            "title" : "Create Product Return",
            "redirect" : True,
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'sales/returns/entry_product_return.html', context)
    

@check_mode
@login_required
@shop_required
@permissions_required(['can_view_product_return'])
def product_returns(request):
    current_shop = get_current_shop(request)
    instances = ProductReturn.objects.filter(shop=current_shop,is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(product__icontains=query))
        
    title = "Product Returns"
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/returns/product_returns.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_product_return'])
def product_return(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(ProductReturn.objects.filter(pk=pk,shop=current_shop,is_deleted=False))
    product_returns = ProductReturnItem.objects.filter(product_return=instance,shop=current_shop,is_deleted=False)
    
    context = {
        "instance" : instance,
        "title" : "Product Return",
        "product_returns" : product_returns,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/returns/product_return.html',context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_product_return'])
def delete_product_return(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(ProductReturn.objects.filter(pk=pk,shop=current_shop))
    return_items = ProductReturnItem.objects.filter(shop=current_shop,is_deleted=False,product_return=instance)

    for r in return_items:
        product = r.product
        unit = r.unit
        qty=r.qty
        if DamagedProduct.objects.filter(pk=product.pk,unit=unit,shop=current_shop,is_deleted=False).exists():
            old_damaged = get_object_or_404(DamagedProduct.objects.filter(pk=product.pk,unit=unit,shop=current_shop,is_deleted=False))
            quantity = old_damaged.qty + qty
            DamagedProduct.objects.filter(pk=product.pk,unit=unit,shop=current_shop,is_deleted=False).update(qty=quantity)
        else:
            DamagedProduct.objects.filter(pk=product.pk,unit=unit,shop=current_shop,is_deleted=False).update(is_returned=False)

    ProductReturn.objects.filter(pk=pk,shop=current_shop).update(is_deleted=True)
    response_data = {}
    response_data['status'] = 'true'        
    response_data['title'] = "Successfully Deleted"       
    response_data['redirect'] = 'true' 
    response_data['redirect_url'] = reverse('sales:product_returns')
    response_data['message'] = "Product Return Successfully Deleted."
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
def get_sale_items(request):
    current_shop = get_current_shop(request)
    pk = request.GET.get('id')
    template_name = 'sales/includes/sale_items.html'
    instances = SaleItem.objects.filter(sale__pk=pk)
    if instances :  
        context = {
            'sale_items' : instances,
        }
        html_content = render_to_string(template_name,context)
        response_data = {
            "status" : "true",
            'template' : html_content,
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Product not found"
        }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
def get_customer(request):
    pk = request.GET.get('id')
    instance = Sale.objects.get(pk=pk,is_deleted=False)

    if instance.customer:
        if instance.customer.is_system_generated == True:
            response_data = {
                "status" : "true",
                'credit' : float(0.00), 
                'debit' : float(0.00), 
            }
        else:
            response_data = {
                "status" : "true",
                'credit' : float(instance.customer.credit), 
                'debit' : float(instance.customer.debit), 
            }
    else:
        response_data = {
            "status" : "false",
            "message" : "Credit Error"
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_estimate'])
def create_estimate(request):    
    current_shop = get_current_shop(request)
    
    EstimateItemFormset = formset_factory(EstimateItemForm,extra=1)
    
    if request.method == 'POST':
        form = EstimateForm(request.POST)
        estimate_item_formset = EstimateItemFormset(request.POST,prefix='estimate_item_formset')
        for estimate_form in estimate_item_formset:
            estimate_form.fields['product'].queryset = Product.objects.filter(shop=current_shop,is_deleted=False) 
        if form.is_valid() and estimate_item_formset.is_valid():            
            items = {}
            total_tax_amount = 0
            total_discount_amount = 0

            for f in estimate_item_formset:  
                
                discount_amount = f.cleaned_data['discount_amount']
                discount = f.cleaned_data['discount']
                total_discount_amount += discount_amount 
                product = f.cleaned_data['product']
                unit = f.cleaned_data['unit']
                qty = f.cleaned_data['qty']
                exact_qty = get_exact_qty(qty,unit)
                if unit.is_base == True :
                    cost = product.cost
                else :
                    unit_instanse = get_object_or_404(ProductAlternativeUnitPrice.objects.filter(product=product,unit=unit))
                    cost = unit_instanse.cost
                price = f.cleaned_data['price']                
                product_tax_amount = price * product.tax / 100
                tax_added_price = price + product_tax_amount
                tax_amount = qty * (price - product.discount) * product.tax / 100

                tax_amount = Decimal(format(tax_amount, '.2f'))
                total_tax_amount += tax_amount 
                if str(product.pk) in items:
                    p_unit = items[str(product.pk)]["unit"]
                    p_unit = Measurement.objects.get(pk=p_unit)
                    if p_unit == unit:
                        q = items[str(product.pk)]["qty"]
                        items[str(product.pk)]["qty"] = q + qty
                    else:
                        dic = {
                            "qty" : qty,
                            "cost" : cost,
                            "price" : price,
                            "tax_amount" : tax_amount,
                            "tax_added_price" : tax_added_price,
                            "discount_amount" : discount_amount,
                            "unit" : unit.pk,
                        }
                        items[str(product.pk)] = dic
                else:
                    dic = {
                        "qty" : qty,
                        "cost" : cost,
                        "price" : price,
                        "tax_amount" : tax_amount,
                        "tax_added_price" : tax_added_price,
                        "discount_amount" : discount_amount,
                        "unit" : unit.pk,
                    }
                    items[str(product.pk)] = dic            
            
            customer_name = form.cleaned_data['customer_name']
            customer_address = form.cleaned_data['customer_address']
            customer_email = form.cleaned_data['customer_email']
            customer_phone = form.cleaned_data['customer_phone']
            customer = form.cleaned_data['customer']
            customer_state = form.cleaned_data['customer_state']

            if not customer:
                auto_id = get_auto_id(Customer)
                a_id = get_a_id(Customer,request)

                customer = Customer(
                    name = customer_name,
                    email = customer_email,
                    phone = customer_phone,
                    address = customer_address,
                    shop = current_shop,
                    first_time_credit = 0,
                    first_time_debit = 0,
                    credit = 0,
                    debit = 0,
                    creator = request.user,
                    updator = request.user,
                    auto_id = auto_id,
                    a_id = a_id,
                    state = customer_state
                )
                customer.save()

            gst_type = "sgst"
            if not customer.state == current_shop.state:
                gst_type = "igst"

            auto_id = get_auto_id(Estimate)
            a_id = get_a_id(Estimate,request)
            
            #create Estimate
            date = form.cleaned_data['time']

            data1 = form.save(commit=False)
            data1.creator = request.user
            data1.updator = request.user
            data1.auto_id = auto_id
            data1.shop = current_shop
            data1.total_discount_amount = total_discount_amount
            data1.total_tax_amount = total_tax_amount
            data1.a_id = a_id
            data1.gst_type = gst_type
            data1.customer = customer
            data1.save()       
             
            all_subtotal = 0
           
            #save items
            for key, value in items.iteritems():
                product = Product.objects.get(pk=key)
                qty = value["qty"]
                price = value["price"]
                tax = product.tax
                discount = discount
                tax_amount = value["tax_amount"]
                unit = value["unit"]
                unit = Measurement.objects.get(pk=unit)
                if unit.is_base == True :
                    cost = product.cost
                else :
                    unit_instanse = get_object_or_404(ProductAlternativeUnitPrice.objects.filter(product=product,unit=unit))
                    cost = unit_instanse.cost
                discount_amount = value["discount_amount"]
                tax_added_price = value["tax_added_price"]
                subtotal = (qty * price) - discount_amount + tax_amount
                
                all_subtotal += subtotal 
                
                EstimateItem(
                    estimate = data1,
                    product = product,
                    qty = qty,
                    cost = cost,
                    price = price,
                    tax = tax,
                    discount = discount,
                    tax_amount = tax_amount,
                    discount_amount = discount_amount,
                    subtotal = subtotal,
                    unit = unit,
                    tax_added_price = tax_added_price
                ).save()  
             
            total = all_subtotal
            total_total = total
            print total_total
            if current_shop.remove_previous_balance_from_bill:
                total_total = all_subtotal 

            rounded_total = round(total)
            extra = round(total) - float(total)
            round_off = format(extra, '.2f')
            

            #update sale total,round_off and subtotal

            Estimate.objects.filter(pk=data1.pk).update(subtotal=all_subtotal,total=total_total,round_off=round_off)

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Estimate created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('sales:estimates')
            } 
            
        else:            
            message = generate_form_errors(form,formset=False) 
            message += generate_form_errors(estimate_item_formset,formset=True)     
            print estimate_item_formset.errors        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        default_customer = Customer.objects.get(name="default",address="default",shop=current_shop)
        estimate_form = EstimateForm(initial={"customer" : default_customer,"sale_type" : "retail"})
        estimate_item_formset = EstimateItemFormset(prefix='estimate_item_formset')
        for form in estimate_item_formset:
            form.fields['product'].queryset = Product.objects.filter(shop=current_shop,is_deleted=False)
            form.fields['unit'].queryset = Measurement.objects.none()
            form.fields['unit'].label_from_instance = lambda obj: "%s" % (obj.code)        
        context = {
            "title" : "Create Estimate ",
            "form" : estimate_form,
            "url" : reverse('sales:create_estimate'),
            "estimate_item_formset" : estimate_item_formset,
            "redirect" : True,
            "is_create_page" : True,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'sales/estimate/entry_estimate.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_estimate'])
def estimates(request):
    current_shop = get_current_shop(request)
    instances = Estimate.objects.filter(is_deleted=False,shop=current_shop)
    customers = Customer.objects.filter(is_deleted=False,shop=current_shop)
    today = datetime.date.today()
    customer = request.GET.get('customer')
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    payment = request.GET.get('payment')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    if customer:
        instances = instances.filter(customer_id=customer)

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)

    if payment :      
        if payment == "full":
            instances = instances.filter(balance=0)
        elif payment == "no":
            for instance in instances:
                payment_received = instance.payment_received
                if payment_received != 0:
                    instances = instances.exclude(pk = instance.pk)
        elif payment == "extra":
            for instance in instances:
                total = instance.total
                payment_received = instance.payment_received
                if total >= payment_received:
                    instances = instances.exclude(pk=instance.pk)
        elif payment == "partial":
            for instance in instances:
                total = instance.total
                payment_received = instance.payment_received
                if total <= payment_received or payment_received == 0:
                    instances = instances.exclude(pk=instance.pk)
                    
    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])
            
    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)
    context = {
        'instances': instances,
        'estimates' :estimates,
        'customers' : customers,
        "title" : 'Estimates',

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "estimates" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,    
    }
    return render(request,'sales/estimate/estimates.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_estimate'])
def estimate(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Estimate.objects.filter(pk=pk,is_deleted=False,shop=current_shop))    
    estimate_items = EstimateItem.objects.filter(estimate=instance,is_deleted=False)
    context = {
        "instance" : instance,
        "title" : "estimate : #" + str(instance.auto_id),       
        "estimate_items" : estimate_items,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'sales/estimate/estimate.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_estimate'])
def edit_estimate(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Estimate.objects.filter(pk=pk,is_deleted=False,shop=current_shop))        

    if EstimateItem.objects.filter(estimate=instance).exists():
        extra = 0
    else:
        extra= 1
    EstimateItemFormset = inlineformset_factory(
        Estimate, 
        EstimateItem, 
        can_delete = True,
        extra = extra,
        exclude=('creator','updator','auto_id','is_deleted','tax','cost','subtotal','estimate','tax_amount','tax_added_price'),
        widgets = {
            'product' : autocomplete.ModelSelect2(url='products:product_autocomplete',attrs={'data-placeholder': 'Product','data-minimum-input-length': 1},),
            'qty': TextInput(attrs={'class': 'required form-control number qty','placeholder' : 'Quantity'}),
            'cost': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Cost'}),
            'price': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Price'}),
            'subtotal': TextInput(attrs={'disabled' : 'disabled', 'class': 'required form-control number','placeholder' : 'Sub Total'}),
            'discount_amount' : TextInput(attrs={'class': 'form-control number','placeholder' : 'Discount Amount'}),
            'discount' : TextInput(attrs={'class': 'form-control number','placeholder' : 'Discount'}),
            'unit' : Select(attrs={'class': 'required form-control selectpicker'}),
        }
    )
    
    if request.method == 'POST':
        response_data = {}
        form = EstimateForm(request.POST,instance=instance)
        estimate_item_formset = EstimateItemFormset(request.POST,prefix='estimate_item_formset',instance=instance)
        
        if form.is_valid() and estimate_item_formset.is_valid():
            
            old_balance = Estimate.objects.get(pk=pk).balance
            old_paid = Estimate.objects.get(pk=pk).payment_received                  
            items = {}
            total_discount_amount = 0
            total_tax_amount = 0

            for f in estimate_item_formset:      
                if f not in estimate_item_formset.deleted_forms:            
                    product = f.cleaned_data['product']
                    
                    qty = f.cleaned_data['qty']
                    price = f.cleaned_data['price']
                    discount_amount = f.cleaned_data['discount_amount']
                    unit = f.cleaned_data['unit']
                    if unit.is_base == True :
                        cost = product.cost
                    else :
                        unit_instanse = get_object_or_404(ProductAlternativeUnitPrice.objects.filter(product=product,unit=unit))
                        cost = unit_instanse.cost
                    exact_qty = get_exact_qty(qty,unit)
                    tax_amount = qty * price * product.tax / 100
                    tax_amount = Decimal(format(tax_amount, '.2f'))
                    total_tax_amount += tax_amount 
                    total_discount_amount += discount_amount

                    product_tax_amount = price * product.tax / 100
                    tax_added_price = price + product_tax_amount

                    if str(product.pk) in items:
                        q = items[str(product.pk)]["qty"]
                        items[str(product.pk)]["qty"] = q + qty

                        d = items[str(product.pk)]["discount_amount"]
                        items[str(product.pk)]["discount_amount"] = d + discount_amount

                        t = items[str(product.pk)]["tax_amount"]
                        items[str(product.pk)]["tax_amount"] = t + tax_amount

                    else:
                        dic = {
                            "qty" : qty,
                            "cost" : cost,
                            "price" : price,
                            "tax_amount" : tax_amount,
                            "discount_amount" :discount_amount,
                            "tax_added_price" : tax_added_price,
                            "unit" :unit.pk
                        }
                        items[str(product.pk)] = dic                  
                
            customer_name = form.cleaned_data['customer_name']
            customer_address = form.cleaned_data['customer_address']
            customer_email = form.cleaned_data['customer_email']
            customer_phone = form.cleaned_data['customer_phone']
            customer = form.cleaned_data['customer']
            customer_state = form.cleaned_data['customer_state']

            print customer
            if not customer:
                auto_id = get_auto_id(Customer)
                a_id = get_a_id(Customer,request)

                customer = Customer(
                    name = customer_name,
                    email = customer_email,
                    phone = customer_phone,
                    address = customer_address,
                    shop = current_shop,
                    first_time_credit = 0,
                    first_time_debit = 0,
                    credit = 0,
                    debit = 0,
                    creator = request.user,
                    updator = request.user,
                    auto_id = auto_id,
                    a_id = a_id,
                    state = customer_state
                )
                customer.save()

            gst_type = "sgst"
            if not customer.state == current_shop.state:
                gst_type = "igst"

            #update Estimate
            date = form.cleaned_data['time']
            data1 = form.save(commit=False)
            data1.updator = request.user
            data1.date_updated = datetime.datetime.now()
            data1.total_discount_amount = total_discount_amount
            data1.total_tax_amount = total_tax_amount
            data1.gst_type = gst_type
            data1.save() 
            all_subtotal = 0
            
            #delete previous items and update stock
            previous_estimate_items = EstimateItem.objects.filter(estimate=instance)                    
            previous_estimate_items.delete()
            
            #save items
            for key, value in items.iteritems():
                product = Product.objects.get(pk=key)
                qty = value["qty"]
                price = value["price"]
                tax = product.tax
                discount = product.discount
                tax_amount = value["tax_amount"]
                discount_amount = value["discount_amount"]
                tax_added_price = value["tax_added_price"]
                unit = value["unit"]
                subtotal = (qty * price) - discount_amount + tax_amount
                unit = Measurement.objects.get(pk=unit)
                
                all_subtotal += subtotal 
                
                EstimateItem(
                    estimate = data1,
                    product = product,
                    qty = qty,
                    cost = cost,
                    price = price,
                    tax = tax,
                    discount = discount,
                    tax_amount = tax_amount,
                    discount_amount = discount_amount,
                    subtotal = subtotal,
                    unit = unit,
                    tax_added_price = tax_added_price
                ).save()
            
            total = all_subtotal
            rounded_total = Decimal(round(total))
            extra = Decimal(round(total)) - total
            round_off = Decimal(format(extra, '.2f'))

            Estimate.objects.filter(pk=data1.pk).update(subtotal=all_subtotal,total=total,round_off=round_off)            
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Estimate Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('sales:estimate',kwargs={'pk':data1.pk})
            }            
        else:
            message = generate_form_errors(form,formset=False)     
            message += generate_form_errors(estimate_item_formset,formset=True)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:        
        form = EstimateForm(instance=instance) 
        estimate_item_formset = EstimateItemFormset(prefix='estimate_item_formset',instance=instance)
        for item in estimate_item_formset:
            item.fields['unit'].queryset = Measurement.objects.filter(shop=current_shop)
            item.fields['product'].queryset = Product.objects.filter(shop=current_shop)        
        context = {
            "form" : form,
            "title" : "Edit Estimate #: " + str(instance.auto_id),
            "instance" : instance,
            "url" : reverse('sales:edit_estimate',kwargs={'pk':instance.pk}),
            "estimate_item_formset" : estimate_item_formset,
            "redirect" : True,
         
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'sales/estimate/entry_estimate.html', context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_estimate'])
def delete_estimate(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Estimate.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    instance.is_deleted=True
    instance.save()

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Estimate Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('sales:estimates')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_estimate'])
def delete_selected_estimates(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Estimate.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
            instance.is_deleted=True
            instance.save()
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Estimate(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('sales:estimates')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def print_estimate(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Estimate.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    estimate_items = EstimateItem.objects.filter(estimate=instance)

    to_word = inflect.engine()
    total_in_words = to_word.number_to_words(instance.total)
    
    # tax_categories = TaxCategory.objects.filter(shop=current_shop,is_deleted=False)
    # tax_percentage_dict = {}
    # for tax in tax_categories:
    #         items = sale_items.filter(product__tax_category=tax)

    #         tax_amount = 0
    #         if items :
    #             tax_amount = items.aggregate(tax_amount=Sum('tax_amount')).get("tax_amount",0)
    #         if tax_amount > 0:
    #             if instance.gst_type == "sgst":
    #                 each_amount = tax_amount/2
    #                 each_percentage = tax.tax/2
    #                 tax_values_dict = {}
    #                 tax_values_dict[str(each_percentage)] = each_amount
    #                 tax_percentage_dict[str(tax.tax)] = tax_values_dict
    # print tax_percentage_dict

    if instance.sale_type == "wholesale_with_customer":
        estimate_items_list = []
        total = 0
        for item in estimate_items:
            tax_amount = item.product.price * (item.tax/100)
            subtotal = (item.product.price - item.discount ) * item.qty 
            total += subtotal
            dicts = {
                "product" : item.product.name,
                "qty" : item.qty,
                "price" : item.product.price,
                "tax" : item.tax,
                "discount" : item.discount,
                "subtotal" : subtotal
            }
            estimate_items_list.append(dicts)

        saved_amount = instance.special_discount+instance.total_discount_amount
        cash_payment = total - instance.balance
        context = {
            'total_in_words':total_in_words,
            "instance" : instance,
            "title" : "Estimate : #" + str(instance.auto_id),
            "single_page" : True,
            "sale_items" : estimate_items_list,
            "current_shop" : current_shop,
            "saved_amount" : saved_amount,
            "total" : total,
            "cash_payment" : cash_payment,
            "is_need_wave_effect" : True,
        }

        template_name = 'sales/estimate/' + current_shop.bill_print_type + 'wc.html'
        return render(request,template_name,context)

    else:
        saved_amount = instance.special_discount+instance.total_discount_amount
        context = {
            "total_in_words":total_in_words,
            "instance" : instance,
            "title" : "Estimate : #" + str(instance.auto_id),
            "single_page" : True,
            "sale_items" : estimate_items,
            "current_shop" : get_current_shop(request),
            "saved_amount" : saved_amount,
            "is_need_wave_effect" : True,
        }
        template_name = 'sales/estimate/' + current_shop.bill_print_type + '.html'
        return render(request,template_name,context)


@check_mode
@login_required
@shop_required
def email_estimate(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Estimate.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    if request.method == 'POST':
        form = EmailSaleForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            content = form.cleaned_data['content']
            content += "<br />"
            link = request.build_absolute_uri(reverse('sales:print_estimate',kwargs={'pk':pk}))
            content += '<a href="%s">%s</a>' %(link,link)
            
            template_name = 'email/email.html'
            subject = "Purchase Details (#%s) | %s" %(str(instance.auto_id),current_shop.name)          
            context = {
                'name' : name,
                'subject' : subject,
                'content' : content,
                'email' : email
            }
            html_content = render_to_string(template_name,context) 
            send_email(email,subject,content,html_content) 
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Sent",
                "message" : "Estimate Successfully Sent.",
                "redirect" : "true",
                "redirect_url" : reverse('sales:estimate',kwargs={'pk':pk})
            } 
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        email = instance.customer.email
        name = instance.customer.name
        content = "Thanks for your purchase from %s. Please follow the below link for your purchase details." %current_shop.name
        
        form = EmailSaleForm(initial={'name' : name, 'email' : email, 'content' : content})
        
        context = {
            "instance" : instance,
            "title" : "Email Sale : #" + str(instance.auto_id),
            "single_page" : True,
            'form' : form,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
        }
        return render(request,'sales/estimate/email_estimate.html',context)


def stock_report(request):
    current_shop = get_current_shop(request)
    today = datetime.date.today()
    instances = SaleItem.objects.filter(is_deleted=False)
    categories = Category.objects.filter(is_deleted=False)
    products = Product.objects.filter(is_deleted=False)
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    category = request.GET.get('category')
    product = request.GET.get('product')
    title = "Stock Movement"
    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"
    if category:

        instances = instances.filter(product__category_id=category)

    if product:
        instances = instances.filter(product_id=product)

    if year:
        instances = instances.filter(sale__time__year=year)

    if month:
        instances = instances.filter(sale__time__month=month)


    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(sale__time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(sale__time__year=today.year,sale__time__month=today.month)
        elif period == "today" :
            instances = instances.filter(sale__time__year=today.year,sale__time__month=today.month,sale__time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, sale__time__range=[from_date, to_date])
            
    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(sale__time__month=date.month,sale__time__year=date.year,sale__time__day=date.day)


    items = {}
    for f in instances:        
        product = f.product
        qty = f.qty
        stock = f.product.stock
        if str(product.pk) in items:
            q = items[str(product.pk)]["qty"]
            items[str(product.pk)]["qty"] = q + qty
        else:
            dic = {
                "product":product,
                "qty" : qty,
                "stock" : stock,
            }
            items[str(product.pk)] = dic

    context = {
        "items" : items,
        "instances" :instances,
        'categories' :categories ,
        "products":products,
        "title" : title,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_select_picker" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_popup_box" : True,
        "is_need_datetime_picker" : True,
        }
    return render(request, "sales/stock_report.html", context)
