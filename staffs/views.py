from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponse, HttpResponseRedirect
from staffs.forms import DesignationForm, StaffForm, StaffSalaryForm
from staffs.models import Designation, Staff, StaffSalary
from main.models import Shop
import json
import datetime
from users.functions import get_current_shop
from main.functions import get_auto_id, generate_form_errors, get_a_id
from django.core.urlresolvers import reverse
from dal import autocomplete
from django.db.models import Q, Sum
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, shop_required, check_account_balance,permissions_required,ajax_required
from django.utils import timezone
from datetime import date, time
import datetime
from django.forms.formsets import formset_factory
from finance.models import Transaction, CashAccount, BankAccount


class DesignationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Designation.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q)
                                 )

        return items

    def create_object(self, text):
        current_shop = get_current_shop(self.request)
        auto_id = get_auto_id(Designation)
        a_id = get_a_id(Designation,self.request)

        return Designation.objects.create(
            auto_id=auto_id,
            a_id=a_id,
            name=text,
            shop=current_shop,
            creator=self.request.user,
            updator=self.request.user
        )


class StaffAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        staffs = Staff.objects.filter(is_deleted=False)

        if self.q:
            staff = staffs.filter(Q(first_name__istartswith=self.q)
                                 )

        return staff


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_designation'])
def create_designation(request):
    current_shop = get_current_shop(request)
    if request.method == 'POST':
        form = DesignationForm(request.POST)

        if form.is_valid():
            auto_id = get_auto_id(Designation)
            a_id = get_a_id(Designation, request)
            data = form.save(commit=False)
            data.auto_id = auto_id
            data.a_id = a_id
            data.shop = current_shop
            data.creator = request.user
            data.updator = request.user
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully Created",
                "message": "Designation created successfully.",
                "redirect": "true",
                "redirect_url": reverse('staffs:designations')
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = DesignationForm()

        context = {
            "title": "Create Designation",
            "form": form,
            "redirect": "true",

            "is_need_select_picker": True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_chosen_select": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_datetime_picker": True,
            
        }
        return render(request, 'staffs/designations/entry.html', context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_designation'])
def designations(request):
    current_shop = get_current_shop(request)
    instances = Designation.objects.filter(is_deleted=False,shop=current_shop)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query)) 

    context = {
        'instances': instances,
        "title": 'designations',

        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_select_picker" : True,
        "is_need_grid_system": True,
        "is_need_chosen_select": True,
        "is_need_animations": True,
        "is_need_popup_box": True,
        "staffs" : True,
    }
    return render(request, "staffs/designations/designations.html", context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_designation'])
def edit_designation(request, pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Designation.objects.filter(pk=pk, is_deleted=False,shop=current_shop))
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query)) 


    if request.method == "POST":
        form = DesignationForm(request.POST, instance=instance)

        if form.is_valid():
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully Updated",
                "message": "Designation updated successfully.",
                "redirect": "true",
                "redirect_url": reverse('staffs:designations')
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = DesignationForm(instance=instance)

        context = {
            "instance": instance,
            "title": "Edit Designation :" + instance.name,
            "form": form,
            "redirect": "true",
            "url": reverse('staffs:edit_designation', kwargs={'pk': instance.pk}),

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_select_picker" : True,
            "is_need_bootstrap_growl": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            
        }
        return render(request, 'staffs/designations/entry.html', context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_designation'])
def designation(request, pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Designation.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    context = {
        'instance': instance,
        'title': 'Designation',

        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_grid_system": True,
        "is_need_select_picker" : True,
        "is_need_chosen_select": True,
        "is_need_animations": True,
        "is_need_popup_box": True,
        
    }
    return render(request, "staffs/designations/designation.html", context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_designation'])
def delete_designation(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Designation.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    Designation.objects.filter(pk=pk,shop=current_shop).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Designation Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('staffs:designations')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_designation'])
def delete_selected_designations(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(Designation.objects.filter(pk=pk, is_deleted=False,shop=current_shop))
            Designation.objects.filter(pk=pk).update(
                is_deleted=True, name=instance.name + "_deleted_" + str(instance.auto_id))

        response_data = {
            "status": "true",
            "title": "Successfully Deleted",
            "message": "Selected Designation Successfully Deleted.",
            "redirect": "true",
            "redirect_url": reverse('staffs:designations')
        }
    else:
        response_data = {
            "status": "false",
            "title": "Nothing selected",
            "message": "Please select some items first.",
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_staff'])
def create_staff(request):
    current_shop = get_current_shop(request)
    if request.method == 'POST':
        form = StaffForm(request.POST, request.FILES)

        if form.is_valid():
            auto_id = get_auto_id(Staff)
            a_id = get_a_id(Staff, request)

            data = form.save(commit=False)
            data.auto_id = auto_id
            data.a_id = a_id
            data.shop = current_shop 
            data.creator = request.user
            data.updator = request.user
            data.save()

            
            return HttpResponseRedirect(reverse('staffs:staff', kwargs={'pk': data.pk}))
        else:
            message = generate_form_errors(form, formset=False)
            form = StaffForm()

            context = {
            "title": "Create Staff",
            "form": form,
            "message" : message,

            "is_need_select_picker": True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_chosen_select": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_datetime_picker": True,
            "is_need_fileinput" : True
          
        }
        return render(request, 'staffs/entry.html', context)
    else:
        form = StaffForm()

        context = {
            "title": "Create Staff",
            "form": form,
      
            "is_need_select_picker": True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_chosen_select": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_datetime_picker": True,
            "is_need_fileinput" : True

        }
        return render(request, 'staffs/entry.html', context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_staff'])
def staffs(request):
    current_shop = get_current_shop(request)
    instances = Staff.objects.filter(is_deleted=False,shop=current_shop) 
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(first_name__icontains=query)|Q(last_name__icontains=query)|Q(designation__name=query)|Q(salary__icontains=query)|Q(phone__icontains=query)|Q(sex__icontains=query)|Q(dob__icontains=query)) 
         

    context = {
        'instances': instances,
        "title": 'Staffs',

        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_grid_system": True,
        "is_need_select_picker" : True,
        "is_need_chosen_select": True,
        "is_need_animations": True,
        "is_need_popup_box": True,
        "is_need_datetime_picker": True,
      
    }
    return render(request, "staffs/staffs.html", context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_staff'])
def edit_staff(request, pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Staff.objects.filter(pk=pk, is_deleted=False,shop=current_shop))
    query = request.GET.get("q")
    if query:
        instance = instance.filter(Q(first_name__icontains=query)|Q(last_name__icontains=query)|Q(designation__icontains=query)|Q(salary__icontains=query)|Q(phone__icontains=query)|Q(sex__icontains=query)|Q(dob__icontains=query)) 
    

    if request.method == "POST":
        form = StaffForm(request.POST, request.FILES,instance=instance)

        if form.is_valid():
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()
            
            return HttpResponseRedirect(reverse('staffs:staff', kwargs={'pk': data.pk}))

        else:
            message = generate_form_errors(form, formset=False)
            form = StaffForm(instance=instance)
            context = {
            "title": "Edit Staff",
            "form": form,
            "message" : message,

            "is_need_select_picker": True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_chosen_select": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_datetime_picker": True,
         
        }
        return render(request, 'staffs/entry.html', context)
    else:
        form = StaffForm(instance=instance)

        context = {
            "instance": instance,
            "form": form,
            "title": 'Edit Staffs',
            "redirect": "true",
            "url": reverse('staffs:edit_staff', kwargs={'pk': instance.pk}),

            "is_need_select_picker": True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_chosen_select": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_datetime_picker": True,
     
        }
        return render(request, 'staffs/entry.html', context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_staff'])
def staff(request, pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Staff.objects.filter(pk=pk, is_deleted=False,shop=current_shop))

    print instance.permissionlist
    query = request.GET.get("q")
    if query:
        instance = instance.filter(Q(first_name__icontains=query)|Q(last_name__icontains=query)|Q(designation__icontains=query)|Q(salary__icontains=query)|Q(phone__icontains=query)|Q(sex__icontains=query)|Q(dob__icontains=query)) 
    
    context = {
        'instance': instance,
        'title': 'Staff',

        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_select_picker" : True,
        "is_need_bootstrap_growl": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_popup_box": True,
        "is_need_datetime_picker": True,
      
    }
    return render(request, "staffs/staff.html", context)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_staff'])
def delete_staff(request,pk):
    Staff.objects.filter(id=pk).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Staff deleted successfully.",
        "redirect": "true",
        "redirect_url": reverse('staffs:staffs')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_staff'])
def delete_selected_staffs(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(Staff.objects.filter(pk=pk, is_deleted=False,shop=current_shop))
            Staff.objects.filter(pk=pk).update(
                is_deleted=True, first_name=instance.first_name + "_deleted_" + str(instance.auto_id))

        response_data = {
            "status": "true",
            "title": "Successfully Deleted",
            "message": "Selected Staffs Successfully Deleted.",
            "redirect": "true",
            "redirect_url": reverse('staffs:staffs')

        }
    else:
        response_data = {
            "status": "false",
            "title": "Nothing selected",
            "message": "Please select some items first.",
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_staff_salary'])
def create_staff_salary(request):    
    current_shop = get_current_shop(request)
    SalaryFormset = formset_factory(StaffSalaryForm)

    if request.method == 'POST':
        salary_formset = SalaryFormset(request.POST, prefix='salary_formset')
        
        if salary_formset.is_valid():
            is_ok = True
            message = ""

            for form in salary_formset:
                staff = form.cleaned_data['staff']
                year = form.cleaned_data['year']
                month = form.cleaned_data['month']

                if StaffSalary.objects.filter(staff=staff,month=month,year=year,shop=current_shop,is_deleted=False).exists():
                    is_ok = False
                    message += "Staff Salary for %s in %s , %s is already exist. <br />" %(staff,month,year)

            if is_ok:
                for form in salary_formset:                                
                    staff = form.cleaned_data['staff']
                    year = form.cleaned_data['year']
                    month = form.cleaned_data['month']
                    allowance = form.cleaned_data['allowance']
                    deduction = form.cleaned_data['deduction']

                    auto_id = get_auto_id(StaffSalary)
                    a_id = get_a_id(StaffSalary,request)

                    staff_salaries = 0
                    if StaffSalary.objects.filter(staff=staff,shop=current_shop,is_deleted=False).exists():
                        staff_salaries = StaffSalary.objects.filter(staff=staff,shop=current_shop,is_deleted=False).aggregate(amount=Sum('amount')).get("amount",0)

                    salary_payments = 0
                    if Transaction.objects.filter(staff=staff,shop=current_shop,transaction_category__name="staff_salary",is_deleted=False).exists():
                        salary_payments = Transaction.objects.filter(staff=staff,transaction_category__name="staff_salary",shop=current_shop,is_deleted=False).aggregate(amount=Sum('amount')).get("amount",0)
                
                    additional_payments = salary_payments - staff_salaries
                    if Staff.objects.filter(pk=staff.pk,shop=current_shop,is_deleted=False).exists():
                        instance = Staff.objects.get(pk=staff.pk,shop=current_shop,is_deleted=False)
                    
                    salary=instance.salary
                    amount = salary + allowance - deduction
                    paid_amount = 0
                    balance = amount
                    advance=0
                    if additional_payments > 0:
                        if additional_payments > amount:
                            balance  = 0
                            paid_amount = amount
                        else:
                            paid_amount = additional_payments
                            balance = balance - additional_payments        

                    StaffSalary(
                            staff=staff,
                            date = datetime.date.today(),
                            amount=amount,
                            year=year,
                            month=month,
                            balance=balance,
                            advance=advance,
                            allowance=allowance,
                            deduction=deduction,
                            basic_salary=salary,
                            paid=paid_amount,
                            creator=request.user,
                            updator=request.user,
                            auto_id=auto_id,
                            a_id=a_id,
                            shop=current_shop
                        ).save()  
            
                response_data = {
                    "status" : "true",
                    "title" : "Successfully Created",
                    "message" : "Staff Salary created successfully.",
                    "redirect" : "true",
                    "redirect_url" : reverse('staffs:staff_salaries')
                } 

            else:                
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Data Already exist.",
                    "message": message
                }   
        
        else:            
            message = generate_form_errors(salary_formset,formset=True)     
            print salary_formset.errors 
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        initial = []

        staffs = Staff.objects.filter(is_deleted=False,shop=current_shop)
        for staff in staffs:
            staff_salaries = 0
            join_date = staff.joined_date
            month = join_date.month
            year = join_date.year

            if StaffSalary.objects.filter(staff=staff,shop=current_shop,is_deleted=False).exists():
                staff_salaries = StaffSalary.objects.filter(staff=staff,shop=current_shop,is_deleted=False).aggregate(amount=Sum('amount')).get("amount",0)
                latest = StaffSalary.objects.filter(staff=staff,shop=current_shop,is_deleted=False).latest('date_added')
                latest_month = latest.month
                latest_year = latest.year
                if int(latest_month) == 12:
                    month = 1
                    year = latest_year + 1
                else:
                    month = int(latest_month) + 1
                    year = latest_year
            basic_salary = staff.salary
            if basic_salary > 0:
                staff_dict = {
                    'staff': staff,
                    'month': month,
                    'year' : year,
                    'basic_salary':int(basic_salary),
                }
                initial.append(staff_dict)
        salary_formset = SalaryFormset(prefix='salary_formset',initial=initial)

        context = {
            "title" : "Create Staff Salary",
            "salary_formset" : salary_formset,
            'redirect' : "true",
            "url" : reverse('staffs:create_staff_salary'),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_formset" : True
        }
        return render(request,'staffs/create_staff_salary.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_staff_salary'])
def staff_salaries(request):
    current_shop = get_current_shop(request)
    instances = StaffSalary.objects.filter(is_deleted=False,shop=current_shop)
    title = "Staff Salaries"

    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    month = request.GET.get("month_filter")
    year = request.GET.get("year")
    query = request.GET.get("q")
    date = request.GET.get("date")
    status = request.GET.get("status")

    if status:
        if status == "balance":
            title = "Staff Salaries: Balance"
            instances = instances.filter(Q(balance__gt=0))

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(staff__name__icontains=query) | Q(date__icontains=query) | Q(amount__icontains=query))
        title = "Staff Salaris - %s" %query 

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    filter_period = False
    if from_date and to_date:
        filter_period = True
    if filter_period:
        from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
        to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1) 
        instances = instances.filter(Q(date__range=[from_date, to_date]))
    elif month and year:
        instances = instances.filter(date__month=month, date__year=year) 
    elif month:
        instances = instances.filter(date__month=month, date__year=today.year) 
    elif year:
        instances = instances.filter(Q(date__year=year))

    
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_chosen_select" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'staffs/staff_salaries.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_staff_salary'])
def staff_salary(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(StaffSalary.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    context = {
        "instance" : instance,
        "title" : "Staff Salary : " + instance.staff.first_name,
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_chosen_select" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'staffs/staff_salary.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_staff_salary'])
def edit_staff_salary(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(StaffSalary.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    staff=instance.staff
    basic=staff.salary
    balance = instance.balance
    paid = instance.paid
    advance = instance.advance
    salary_amount = instance.amount
    allowance = instance.allowance
    deduction = instance.deduction
    if request.method == 'POST':
            
        response_data = {}
        form = StaffSalaryForm(request.POST,instance=instance)

        if form.is_valid():  
            is_ok = True
            message = ""
            staff= form.cleaned_data['staff']
            month = form.cleaned_data['month']
            year = form.cleaned_data['year']
            allowance = form.cleaned_data['allowance']
            deduction=form.cleaned_data['deduction']
            amount = basic + allowance - deduction
            if StaffSalary.objects.filter(staff=staff,month=month,year=year,deduction=deduction,allowance=allowance,amount=amount,shop=current_shop,is_deleted=False).exists():
                is_ok = False
                message += "Staff Salary for %s in %s , %s is already exist. <br />" %(staff,month,year)

            if is_ok:

                if salary_amount != amount:
                    if paid > 0:
                        if advance >0:
                            if (paid+advance) == amount:
                                balance = 0
                                advance=0
                        else:
                            balance = amount - paid
                    else:
                        balance = form.cleaned_data['amount']

                data = form.save(commit=False)
                data.updator = request.user
                data.date_updated = datetime.datetime.now()  
                data.balance = balance
                data.advance = advance
                data.save()      
            
                response_data = {
                    "status" : "true",
                    "title" : "Successfully Updated",
                    "message" : "Staff Salary Successfully Updated.",
                    "redirect" : "true",
                    "redirect_url" : reverse('staffs:staff_salary',kwargs={'pk':data.pk})
                }  
            else:                
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Data Already exist.",
                    "message": message
                }  
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = StaffSalaryForm(instance=instance,initial={'shop':current_shop})
        
        context = {
            "form" : form,
            "title" : "Edit Staff Salary : " + instance.staff.name,
            "instance" : instance,
            "is_edit" : True,
            "url" : reverse('staffs:edit_staff_salary',kwargs={'pk':instance.pk}),
            "redirect" : True,
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'staffs/edit_staff_salary.html', context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_delete_staff_salary'])
def delete_staff_salary(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(StaffSalary.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    transactions = Transaction.objects.filter(is_deleted=False,shop=current_shop,staff_salary=instance)
    for transaction in transactions:
        if transaction.transaction_mode == "cash":
            cash_account = transaction.cash_account
            if cash_account:
                balance = cash_account.balance
                balance = balance + transaction.amount
                CashAccount.objects.filter(pk=transaction.cash_account.pk,shop=current_shop).update(balance=balance)
        elif transaction.transaction_mode == "bank":
            bank_account = transaction.bank_account
            if bank_account:
                balance = bank_account.balance
                balance = balance + transaction.amount
                BankAccount.objects.filter(pk=transaction.bank_account.pk,shop=current_shop).update(balance=balance)
        advance = instance.advance
        if advance>0:
            latest_staff_salary = StaffSalary.objects.filter(shop=current_shop,is_deleted=False,staff=instance.staff).latest('date_added')
            latest_paid = latest_staff_salary.paid
            latest_balance = latest_staff_salary.balance
            salary_amount = latest_paid + latest_balance
            latest_amount = latest_staff_salary.amount
            if (latest_balance < latest_amount) & (salary_amount == latest_amount):
                latest_staff_salary.balance = latest_balance + latest_paid
                latest_staff_salary.paid =latest_paid - advance
                latest_staff_salary.save()
        Transaction.objects.filter(staff_salary=transaction.staff_salary,shop=current_shop).update(is_deleted=True)

    StaffSalary.objects.filter(pk=pk,shop=current_shop).update(is_deleted=True,staff=instance.staff)
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Staff Salary Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('staffs:staff_salaries')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_delete_staff_salary'])
def delete_selected_staff_salaries(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(StaffSalary.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 

            transactions = Transaction.objects.filter(is_deleted=False,shop=current_shop,staff_salary=instance)
            for transaction in transactions:
                if transaction.transaction_mode == "cash":
                    cash_account = transaction.cash_account
                    if cash_account:
                        balance = cash_account.balance
                        balance = balance + transaction.amount
                        CashAccount.objects.filter(pk=transaction.cash_account.pk,shop=current_shop).update(balance=balance)
                elif transaction.transaction_mode == "bank":
                    bank_account = transaction.bank_account
                    if bank_account:
                        balance = bank_account.balance
                        balance = balance + transaction.amount
                        BankAccount.objects.filter(pk=transaction.bank_account.pk,shop=current_shop).update(balance=balance)
                advance = instance.advance
                if advance>0:
                    latest_staff_salary = StaffSalary.objects.filter(shop=current_shop,is_deleted=False,staff=instance.staff).latest('date_added')
                    latest_paid = latest_staff_salary.paid
                    latest_balance = latest_staff_salary.balance
                    salary_amount = latest_paid + latest_balance
                    latest_amount = latest_staff_salary.amount
                    if (latest_balance < latest_amount) & (salary_amount == latest_amount):
                        latest_staff_salary.balance = latest_balance + latest_paid
                        latest_staff_salary.paid =latest_paid - advance
                        latest_staff_salary.save()
                Transaction.objects.filter(staff_salary=transaction.staff_salary,shop=current_shop).update(is_deleted=True)

            StaffSalary.objects.filter(pk=pk).update(is_deleted=True,staff=instance.staff)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Staff Salaries Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('staffs:staff_salaries')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required 
def get_salary(request):
    pk = request.GET.get('id')
    current_shop = get_current_shop(request)
    instance = Staff.objects.get(pk=pk,shop=current_shop,is_deleted=False)

    staff_salaries = StaffSalary.objects.filter(staff=instance,shop=current_shop,is_deleted=False)

    salary_amounts = 0
    if staff_salaries:
        salary_amounts = staff_salaries.aggregate(balance=Sum('balance')).get("balance",0)

    balance = salary_amounts
    if instance:
        response_data = {
            "status" : "true", 
            'balance' : str(balance), 
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Balance Error"
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')