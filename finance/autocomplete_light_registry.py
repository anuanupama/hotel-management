from dal import autocomplete
from django.db.models import Q
from staffs.models import Staff
from users.functions import get_current_shop
from finance.models import TaxCategory
from main.functions import get_auto_id,get_a_id
from decimal import Decimal


class StaffAutocomplete(autocomplete.Select2QuerySetView):
	
    def get_queryset(self):
    	current_shop = get_current_shop(self.request)
        staff = Staff.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            staff = staff.filter(name__istartswith=self.q)

        return staff


class TaxCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = TaxCategory.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q)
                                 )
        return items

    def create_object(self, text):
        current_shop = get_current_shop(self.request)
        if not TaxCategory.objects.filter(is_deleted=False,shop=current_shop,name=text).exists():
            auto_id = get_auto_id(TaxCategory)
            a_id = get_a_id(TaxCategory,self.request)

            return TaxCategory.objects.create(
                auto_id=auto_id,
                a_id=a_id,
                name = text + " %",
                tax=Decimal(text),
                shop=current_shop,
                creator=self.request.user,
                updator=self.request.user
            )