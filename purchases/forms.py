from django import forms
from django.forms.widgets import TextInput, Select, Textarea
from purchases.models import Purchase, PurchaseItem,NewPurchaseItem,PaidAmount,CollectAmounts,PurchaseInvoice,\
    PurchaseInvoiceItem,AssetPurchaseItem,AssetPurchase
from dal import autocomplete
from django.utils.translation import ugettext_lazy as _



class PurchaseForm(forms.ModelForm):

    
    class Meta:
        model = Purchase
        exclude = ['creator','updator','auto_id','is_deleted','a_id','shop','subtotal','total','balance','paid','credit_amount_added','paid_amount_added']
        widgets = {
            'time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'Time'}),
            'vendor': autocomplete.ModelSelect2(url='vendors:vendor_autocomplete', attrs={'data-placeholder': 'Vendor', 'data-minimum-input-length': 1},), 
            'subtotal' : TextInput(attrs={'disabled' : 'disabled','class': 'required form-control','placeholder' : 'Subtotal'}),
            'special_discount': TextInput(attrs={'class': 'required form-control','placeholder' : 'Special discount'}),
            'total': TextInput(attrs={'class': 'required form-control','placeholder' : 'Total'}),
            'balance': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Balance'}),
            'invoice_id': TextInput(attrs={'class': 'form-control','placeholder' : 'Invoice Id'}),
            'paid_amount': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Paid Amount'}),
        }
        error_messages = {
            'time' : {
                'required' : _("Time field is required."),
            },
			'vendor' : {
                'required' : _("Vendor field is required."),
            },
            'subtotal' : {
                'required' : _("Subtotal field is required."),
            },
            'special_discount' : {
                'required' : _("Special discount field is required."),
            },
            'total' : {
                'required' : _("Total field is required."),
            },
            
        }


class PurchaseItemForm(forms.ModelForm):
    
    class Meta:
        model = PurchaseItem
        exclude = ['creator','updator','auto_id','is_deleted','a_id','purchase','subtotal','tax']
        widgets = {
            'product': autocomplete.ModelSelect2(url='products:product_autocomplete', attrs={'data-placeholder': 'Product', 'data-minimum-input-length': 1},), 
            'qty' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Qty'}),
            'unit' : Select(attrs={'class': 'required form-control selectpicker'}),
            'product': autocomplete.ModelSelect2(url='products:product_autocomplete', attrs={'data-placeholder': 'Product', 'data-minimum-input-length': 1},), 
            'qty' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Qty'}),
            'price' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Price'}),
            'mrp' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Price'}),
            'wholesale_price' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Price'}),
            'best_before' : TextInput(attrs={'class': 'form-control','placeholder' : 'Best-before'}),
            'manufacture_date': TextInput(attrs={'class': 'form-control datepickerold','placeholder' : 'Date'}),
            'period' : Select(attrs={'class': 'form-control selectpicker'}),
        }
        error_messages = {
            'product' : {
                'required' : _("Product field is required."),
            },
			'qty' : {
                'required' : _("Qty field is required."),
            },
            'price' : {
                'required' : _("Price field is required."),
            },
            'discount' : {
                'required' : _("Discount field is required."),
            },
            'subtotal' : {
                'required' : _("Subtotal field is required."),
            },
            'unit' : {
                'required' : _("Unit field is required."),
            },
            
        }


class NewPurchaseItemForm(forms.ModelForm):
    
    class Meta:
        model = NewPurchaseItem
        exclude = ['creator','updator','auto_id','is_deleted','a_id','purchase','subtotal','tax']
        widgets = {
            'category': autocomplete.ModelSelect2(url='products:category_autocomplete', attrs={'data-placeholder': 'Category', 'data-minimum-input-length': 1},), 
            'subcategory': autocomplete.ModelSelect2(url='products:subcategory_autocomplete', attrs={'data-placeholder': 'Subcategory', 'data-minimum-input-length': 1},),
            'brand' : autocomplete.ModelSelect2(url='products:brand_autocomplete',attrs={'data-placeholder': 'Brand','data-minimum-input-length': 1},),
            'unit_type' : Select(attrs={'class': 'required form-control selectpicker'}),            
            'code': TextInput(attrs={'class': 'required form-control','placeholder' : 'Code'}),
            'cost': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Cost'}),
            'product': TextInput(attrs={'class': 'required form-control','placeholder' : 'Product'}),
            'stock': TextInput(attrs={'class': 'required form-control','placeholder' : 'Stock'}),
            'price' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Price'}),
            'tax_category': Select(attrs={'class': 'required form-control selectpicker'}),
            'discount': TextInput(attrs={'class': 'required form-control','placeholder' : 'Discount'}),
            'subtotal': TextInput(attrs={'disabled' : 'disabled','class': 'required form-control','placeholder' : 'Subtotal'}),
            'low_stock_limit': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Low stock limit'}),
        }
        error_messages = {
            'category' : {
                'required' : _("Category field is required."),
            },
            'unit' : {
                'required' : _("Unit field is required."),
            },
            'low_stock_limit' : {
                'required' : _("Low stock limit field is required."),
            },
            'code' : {
                'required' : _("Code field is required."),
            },
            'cost' : {
                'required' : _("Cost field is required."),
            },
            'product' : {
                'required' : _("Product field is required."),
            },
            'stock' : {
                'required' : _("Stock field is required."),
            },
            'price' : {
                'required' : _("Price field is required."),
            },
            'tax_category' : {
                'required' : _("Tax category field is required."),
            },
            'discount' : {
                'required' : _("Discount field is required."),
            },
            'subtotal' : {
                'required' : _("Subtotal field is required."),
            },
            
        }


class EmailPurchaseForm(forms.Form):
    email = forms.EmailField(widget=TextInput(attrs={'class': 'required form-control email','placeholder' : 'Email'}))
    name = forms.CharField(widget=TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}))
    content = forms.CharField(
        help_text="Link will be added automatically. You do not need to insert manually.",
        widget=Textarea(attrs={
             'class': 'required form-control limit-hieght',
             'placeholder' : 'Content'
            }
        )
    )


class PaidAmountForm(forms.ModelForm):
    
    class Meta:
        model = PaidAmount
        exclude = ['creator','updator','auto_id','is_deleted','a_id','shop','balance']
        widgets = {
            'vendor' : autocomplete.ModelSelect2(url='vendors:vendor_autocomplete',attrs={'data-placeholder': 'Vendor','data-minimum-input-length': 1},),
            'date' : TextInput(attrs={'class': 'required form-control date-picker','placeholder' : 'Date'}),
            'paid' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Paided Cash'}),
            'balance' : TextInput(attrs={'disabled' : 'disabled','class': 'required form-control','placeholder' : 'Balance'}),
            'remaining_balance' :TextInput(attrs={'class': 'required form-control','placeholder' : 'Remaining Balance'}),
            
        }
        error_messages = {
            'date' : {
                'required' : _("Date field is required."),
            },
            'paid' : {
                'required' : _("Paid Amount field is required."),
            },
            'balance' : {
                'required' : _("Balance is required."),
            },
            'vendor' : {
                'required' : _("vendor field is required."),
            },
            'remaining_balance' : {
                'required' : _("Remaining Balance field is required."),
            },
        }


class CollectAmountsForm(forms.ModelForm):
    
    class Meta:
        model = CollectAmounts
        exclude = ['creator','updator','auto_id','is_deleted','a_id','shop','balance']
        widgets = {
            'vendor' : autocomplete.ModelSelect2(url='vendors:vendor_autocomplete',attrs={'data-placeholder': 'Vendor','data-minimum-input-length': 1},),
            'date' : TextInput(attrs={'class': 'required form-control date-picker','placeholder' : 'Date'}),
            'collect_amount' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Collected Cash'}),
            'balance' : TextInput(attrs={'disabled' : 'disabled','class': 'required form-control','placeholder' : 'Balance'}),
            'remaining_balance' :TextInput(attrs={'class': 'required form-control','placeholder' : 'Remaining Balance'}),
            
        }
        error_messages = {
            'date' : {
                'required' : _("Date field is required."),
            },
            'collect_amount' : {
                'required' : _("Collect Amount field is required."),
            },
            'balance' : {
                'required' : _("Balance is required."),
            },
            'vendor' : {
                'required' : _("Vendor field is required."),
            },
            'remaining_balance' : {
                'required' : _("Remaining Balance field is required."),
            },
        }


class PurchaseInvoiceForm(forms.ModelForm):
    
    class Meta:
        model = PurchaseInvoice
        exclude = ['creator','updator','auto_id','is_deleted','a_id','shop']
        widgets = {
            'time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'Time'}),
            'vendor': autocomplete.ModelSelect2(url='vendors:vendor_autocomplete', attrs={'data-placeholder': 'Vendor', 'data-minimum-input-length': 1},), 
            'total' : TextInput(attrs={'disabled' : 'disabled','class': 'required form-control','placeholder' : 'Total'}), 
            'invoice_type' : Select(attrs={'class': 'required form-control selectpicker'}),           
        }
        error_messages = {
            'time' : {
                'required' : _("Time field is required."),
            },
            'vendor' : {
                'required' : _("Vendor field is required."),
            },           
            
        }


class PurchaseInvoiceItemForm(forms.ModelForm):
    
    class Meta:
        model = PurchaseInvoiceItem
        exclude = ['creator','updator','auto_id','is_deleted','a_id','invoice']
        widgets = {
            'product' : autocomplete.ModelSelect2(url='products:product_autocomplete',attrs={'data-placeholder': 'Product','data-minimum-input-length': 1},),
            'qty' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Qty'}),
            'unit' : Select(attrs={'class': 'required form-control selectpicker'}),           
        }
        error_messages = {
            'product' : {
                'required' : _("Product field is required."),
            },
            'qty' : {
                'required' : _("Qty field is required."),
            },
            'unit' : {
                'required' : _("Unit field is required."),
            },
            
        }


class AssetPurchaseForm(forms.ModelForm):
    
    class Meta:
        model = AssetPurchase
        exclude = ['creator','updator','auto_id','is_deleted','a_id','shop','subtotal','total','balance','paid','round_off']
        widgets = {
            'time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'Time'}),
            'vendor': autocomplete.ModelSelect2(url='vendors:vendor_autocomplete', attrs={'data-placeholder': 'Vendor', 'data-minimum-input-length': 1},), 
            'subtotal' : TextInput(attrs={'disabled' : 'disabled','class': 'required form-control','placeholder' : 'Subtotal'}),
            'special_discount': TextInput(attrs={'class': 'required form-control','placeholder' : 'Special discount'}),
            'total': TextInput(attrs={'class': 'required form-control','placeholder' : 'Total'}),
            'balance': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Balance'}),
            'paid_amount': TextInput(attrs={'class': 'number required form-control','placeholder' : 'Paid Amount'}),
        }
        error_messages = {
            'time' : {
                'required' : _("Time field is required."),
            },
            'vendor' : {
                'required' : _("Vendor field is required."),
            },
            'subtotal' : {
                'required' : _("Subtotal field is required."),
            },
            'special_discount' : {
                'required' : _("Special discount field is required."),
            },
            'total' : {
                'required' : _("Total field is required."),
            },
            
        }


class AssetPurchaseItemForm(forms.ModelForm):
    
    class Meta:
        model = AssetPurchaseItem
        exclude = ['creator','updator','auto_id','is_deleted','a_id','purchase','subtotal','tax']
        widgets = {
            'asset': autocomplete.ModelSelect2(url='products:asset_autocomplete', attrs={'data-placeholder': 'Asset', 'data-minimum-input-length': 1},), 
            'qty' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Qty'}),
            'price' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Price'}),
            'discount': TextInput(attrs={'class': 'required form-control','placeholder' : 'Discount'}),
            'subtotal': TextInput(attrs={'disabled' : 'disabled','class': 'required form-control','placeholder' : 'Subtotal'}),
        }
        error_messages = {
            'asset' : {
                'required' : _("Asset field is required."),
            },
            'qty' : {
                'required' : _("Qty field is required."),
            },
            'price' : {
                'required' : _("Price field is required."),
            },
            'discount' : {
                'required' : _("Discount field is required."),
            },
            'subtotal' : {
                'required' : _("Subtotal field is required."),
            },
            'unit' : {
                'required' : _("Unit field is required."),
            },
            
        }
        

class EmailInvoiceForm(forms.Form):
    email = forms.EmailField(widget=TextInput(attrs={'class': 'required form-control email','placeholder' : 'Email'}))
    name = forms.CharField(widget=TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}))
    content = forms.CharField(
        help_text="Link will be added automatically. You do not need to insert manually.",
        widget=Textarea(attrs={
             'class': 'required form-control',
             'placeholder' : 'Content'
            }
        )
    )