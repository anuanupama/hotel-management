from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from bookings.models import Booking,BookingItem,Checkin,CheckinItem,CheckinCustomer,Checkout
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, shop_required, check_account_balance,permissions_required,ajax_required
from bookings.forms import BookingForm,BookingItemForm,CheckinForm,CheckinItemForm,CheckinCustomerForm,CustomerForm,CheckoutForm
from main.functions import generate_form_errors, get_auto_id, get_timezone, get_a_id,get_current_role
from finance.functions import add_transaction
from finance.forms import BankAccountForm, CashAccountForm, TransactionCategoryForm, TransactionForm
from finance.models import BankAccount, CashAccount, TransactionCategory, Transaction, TaxCategory
from purchases.models import Purchase,PurchaseItem
import datetime
from django.db.models import Q
from dal import autocomplete
from django.forms.models import inlineformset_factory
from django.forms.widgets import TextInput,Select
from django.forms.formsets import formset_factory
from products.functions import update_sock,get_exact_qty
from customers.functions import update_customer_credit_debit
from vendors.functions import update_vendor_credit_debit
from products.models import Product,Category,Measurement,ProductAlternativeUnitPrice
from main.functions import render_to_pdf 
from django.utils import timezone
import pytz
from users.functions import get_current_shop, send_email,create_notification
from django.template.loader import render_to_string
from users.models import NotificationSubject, Notification
from customers.models import Customer,CustomerCredit
from purchases.models import Purchase
from decimal import Decimal
from django.db.models import Sum
import xlwt
import urllib
from django.conf import settings
from django.core import serializers
import inflect
from rooms.models import Room,RoomStatus
from datetime import datetime as date_time


class BookingAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = Booking.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(a_id__istartswith=self.q) | 
                                 Q(customer__name__istartswith=self.q) |
                                 Q(customer__address__istartswith=self.q) |
                                 Q(customer__email__istartswith=self.q) |
                                 Q(customer__phone__istartswith=self.q)
                                )    
        return items 


class CheckinAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_shop = get_current_shop(self.request)
        items = Checkin.objects.filter(is_deleted=False,shop=current_shop)

        if self.q:
            items = items.filter(Q(a_id__istartswith=self.q) | 
                                 Q(customer__name__istartswith=self.q) |
                                 Q(customer__address__istartswith=self.q) |
                                 Q(customer__email__istartswith=self.q) |
                                 Q(customer__phone__istartswith=self.q)
                                )    
        return items 

@check_mode
@login_required
@shop_required
@permissions_required(['can_create_booking'])
def create_booking(request):    
    current_shop = get_current_shop(request)
    today = datetime.datetime.today()
    if request.method == 'POST':
        form = BookingForm(request.POST)
        transaction_form = TransactionForm(request.POST)       
        if form.is_valid() and transaction_form.is_valid():
            is_ok = True
            from_time = form.cleaned_data['from_time']
            to_time = form.cleaned_data['to_time']
            if today.date() > from_time.date() or today.date() > to_time.date():
                message = "From date and To date must me greater than or equals to today"
                is_ok = False
            if is_ok:
                items = {}
                customer_name = form.cleaned_data['customer_name']
                customer_address = form.cleaned_data['customer_address']
                customer_email = form.cleaned_data['customer_email']
                customer_phone = form.cleaned_data['customer_phone']
                customer = form.cleaned_data['customer']
                customer_state = form.cleaned_data['customer_state']
                customer_age = form.cleaned_data['customer_age']

                if not customer:
                    auto_id = get_auto_id(Customer)
                    a_id = get_a_id(Customer,request)

                    customer = Customer(
                        name = customer_name,
                        email = customer_email,
                        phone = customer_phone,
                        address = customer_address,
                        shop = current_shop,
                        first_time_credit = 0,
                        first_time_debit = 0,
                        credit = 0,
                        debit = 0,
                        creator = request.user,
                        updator = request.user,
                        auto_id = auto_id,
                        a_id = a_id,
                        state = customer_state,
                        age = customer_age
                    )
                    customer.save()
                
                auto_id = get_auto_id(Booking)
                a_id = get_a_id(Booking,request)            
                rate_type = current_shop.rate_type
                from_time = form.cleaned_data['from_time']
                to_time = form.cleaned_data['to_time'] 
                total_days = form.cleaned_data['total_days']
                advance = form.cleaned_data['advance'] 
                discount = form.cleaned_data['discount']

                # time calculation based on day type
                if current_shop.day_choice == 'noon_to_noon':
                    from_time = date_time(from_time.year, from_time.month, from_time.day, 12, 0, 0, 0)
                    to_time = date_time(to_time.year, to_time.month, from_time.day+total_days, 12, 0, 0, 0)
                if current_shop.day_choice == 'midnight_to_midnight':
                    from_time = date_time(from_time.year, from_time.month, from_time.day, 0, 0, 0, 0)
                    to_time = date_time(to_time.year, to_time.month, to_time.day, 0, 0, 0, 0)

                data = form.save(commit=False)
                data.creator = request.user
                data.updator = request.user
                data.auto_id = auto_id
                data.shop = current_shop
                data.a_id = a_id
                data.rate_type = rate_type
                data.customer = customer
                data.from_time = from_time
                data.to_time = to_time
                data.save()  
                all_subtotal = 0
                rooms = request.POST.getlist('room') 
                for i in range(len(rooms)):        
                    code =  rooms[i]
                    room = Room.objects.get(code=code)              
                    if rate_type == "normal" :
                        rate = room.normal_rate
                    elif rate_type == "offtime":
                        rate = room.offtime_rate
                    elif rate_type == "peektime":
                        rate = room.peaktime_rate  
                    all_subtotal += rate * total_days
                    BookingItem(
                        booking = data,
                        room = room,
                        rate = rate,                   
                    ).save()
                    RoomStatus.objects.create(from_time=from_time,to_time=to_time,is_booked=True,room=room,booking=data)
                    
                data.total = all_subtotal
                data.discount = discount
                balance = all_subtotal - advance - discount
                data.balance = balance
                data.save() 
                if advance > 0 :
                    update_customer_credit_debit(customer.pk,"credit",balance)
                    add_transaction(request,transaction_form,data,advance,"booking","income",0,0)   

                response_data = {
                    "status" : "true",
                    "title" : "Successfully Created",
                    "message" : "Booking created successfully.",
                    "redirect" : "true",
                    "redirect_url" : reverse('bookings:bookings')
                }
            else:
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Form validation error",
                    "message" : message
                } 
        else:            
            message = generate_form_errors(form,formset=False)  
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        booking_form = BookingForm()
        transaction_form = TransactionForm()     
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)     
        context = {
            "title" : "Create Booking ",
            "form" : booking_form,
            "url" : reverse('bookings:create_booking'),
            "redirect" : True,
            "is_create_page" : True,
            "transaction_form" : transaction_form,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'bookings/entry.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_booking'])
def bookings(request):
    current_shop = get_current_shop(request)
    role = get_current_role(request)
    instances = Booking.objects.filter(is_deleted=False,shop=current_shop)
    customers = Customer.objects.filter(is_deleted=False,shop=current_shop)
    products = Product.objects.filter(is_deleted=False,shop=current_shop)
    today = datetime.date.today()
    customer = request.GET.get('customer')
    product = request.GET.get('product')
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    payment = request.GET.get('payment')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    gst = request.GET.get('gst')

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    if customer:
        instances = instances.filter(customer_id=customer)
    if customer:
        instances = instances.filter(customer_id=customer)

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)

    if payment :      
        if payment == "full":
            instances = instances.filter(balance=0)
        elif payment == "no":
            for instance in instances:
                payment_received = instance.payment_received
                if payment_received != 0:
                    instances = instances.exclude(pk = instance.pk)
        elif payment == "extra":
            for instance in instances:
                total = instance.total
                payment_received = instance.payment_received
                if total >= payment_received:
                    instances = instances.exclude(pk=instance.pk)
        elif payment == "partial":
            for instance in instances:
                total = instance.total
                payment_received = instance.payment_received
                if total <= payment_received or payment_received == 0:
                    instances = instances.exclude(pk=instance.pk)
                    
    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])
            
    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)

    if gst :
        if gst =="gst":
            instances = instances.filter(customer_gstin_invoice_id__isnull=False)
        elif gst == 'no_gst' :
            instances = instances.filter(customer_non_gstn_invoice_id__isnull=False)

    context = {
        'instances': instances,
        'bookings' :bookings,
        'customers' : customers,
        'products' : products,
        "title" : 'Bookings',

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "bookings" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,    
    }
    return render(request,'bookings/bookings.html',context) 


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_booking'])
def booking_items(request):
    current_shop = get_current_shop(request)
    instances = BookingItem.objects.filter(is_deleted=False) 
    products = Product.objects.filter(is_deleted=False,shop=current_shop)   
    product = request.GET.get('product')    
    if product:
        instances = instances.filter(product_id=product)
    
    context = {
        'instances': instances,
        "title" : 'Booking Items',
        "products" :products,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "bookings" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,    
    }
    return render(request,'bookings/booking_items.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_booking'])
def booking(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Booking.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    transactions = Transaction.objects.filter(booking=instance,shop=current_shop,collect_amount=None)
    booking_items = BookingItem.objects.filter(booking=instance,is_deleted=False)
    context = {
        "instance" : instance,
        "transactions" : transactions,
        "title" : "Booking : #" + str(instance.auto_id),       
        "booking_items" : booking_items,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'bookings/booking.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_booking'])
def edit_booking(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Booking.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    transaction = get_object_or_404(Transaction.objects.filter(booking=instance,shop=current_shop,collect_amount=None))        

    if request.method == 'POST':
        response_data = {}
        form = BookingForm(request.POST,instance=instance)
        transaction_form = TransactionForm(request.POST,instance=transaction)        
        if form.is_valid() and transaction_form.is_valid():            
            old_balance = Booking.objects.get(pk=pk).balance
            old_advance = Booking.objects.get(pk=pk).advance  
            old_transaction = get_object_or_404(Transaction.objects.filter(booking=pk,collect_amount=None))                
            items = {}
            total_discount_amount = 0
            total_tax_amount = 0                 
                
            customer_name = form.cleaned_data['customer_name']
            customer_address = form.cleaned_data['customer_address']
            customer_email = form.cleaned_data['customer_email']
            customer_phone = form.cleaned_data['customer_phone']
            customer = form.cleaned_data['customer']
            customer_state = form.cleaned_data['customer_state']
            customer_gsttin = form.cleaned_data['customer_gsttin']

            if not customer:
                auto_id = get_auto_id(Customer)
                a_id = get_a_id(Customer,request)

                customer = Customer(
                    name = customer_name,
                    email = customer_email,
                    phone = customer_phone,
                    address = customer_address,
                    shop = current_shop,
                    first_time_credit = 0,
                    first_time_debit = 0,
                    credit = 0,
                    debit = 0,
                    creator = request.user,
                    updator = request.user,
                    auto_id = auto_id,
                    a_id = a_id,
                    state = customer_state,
                    gst_tin = customer_gsttin
                )
                customer.save()

            
            #update booking
            advance = form.cleaned_data['advance']
            total_days = form.cleaned_data['total_days']
            date = form.cleaned_data['time']
            
            data1 = form.save(commit=False)
            data1.updator = request.user
            data1.date_updated = datetime.datetime.now()
            data1.save() 
            all_subtotal = 0
            
            #delete previous items and update stock
            previous_booking_items = BookingItem.objects.filter(booking=instance)
            for p in previous_booking_items: 
                room = p.room
                RoomStatus.objects.filter(room=room,booking=instance).update(is_deleted=True)                
            previous_booking_items.delete()
            
            #save items
            for key, value in items.iteritems():
                product = Product.objects.get(pk=key)
                qty = value["qty"]
                price = value["price"]
                cost = value["cost"]
                tax = product.tax
                discount = product.discount
                tax_amount = value["tax_amount"]
                discount_amount = value["discount_amount"]
                tax_added_price = value["tax_added_price"]
                unit = value["unit"]                    
                subtotal = (qty * price) - discount_amount + tax_amount
                unit = Measurement.objects.get(pk=unit)
                exact_qty = get_exact_qty(qty,unit)
                all_subtotal += subtotal
                BookingItem(
                    booking = data1,
                    product = product,
                    qty = qty,
                    cost = cost,
                    price = price,
                    tax = tax,
                    discount = discount,
                    tax_amount = tax_amount,
                    discount_amount = discount_amount,
                    subtotal = subtotal,
                    unit = unit,
                    tax_added_price = tax_added_price
                ).save()
                
                update_sock(product.pk,exact_qty,"decrease")

            credit=0
            debit=0
            if customer:
                if not customer.is_system_generated:
                    credit = customer.credit
                    debit = customer.debit
                
            total = all_subtotal - special_discount + credit - debit
            total_total = total
            if current_shop.remove_previous_balance_from_bill:
                total_total = all_subtotal - special_discount

            total = all_subtotal - special_discount
            rounded_total = Decimal(round(total))
            balance = rounded_total - payment_received
            extra = Decimal(round(total)) - total
            round_off = Decimal(format(extra, '.2f'))
            balance = Decimal(balance)
            this_booking_balance = (all_subtotal - special_discount) - payment_received - instance.collected_amount

            Booking.objects.filter(pk=data1.pk).update(subtotal=all_subtotal,total=total_total,balance=this_booking_balance,round_off=round_off)
            
            if not data1.customer.is_system_generated:
                #update credit
                update_customer_credit_debit(data1.customer.pk,"debit",old_balance)
                
                if balance > 0:
                    balance = balance
                    update_customer_credit_debit(data1.customer.pk,"credit",balance)
                elif balance < 0:
                    balance = abs(balance)
                    update_customer_credit_debit(data1.customer.pk,"debit",balance)

            #update account balance
            if Transaction.objects.filter(booking=pk).exists(): 
                if old_transaction.cash_account:
                    balance = old_transaction.cash_account.balance - old_paid
                    CashAccount.objects.filter(pk=old_transaction.cash_account.pk,shop=current_shop).update(balance=balance)
                else  :
                    balance = old_transaction.bank_account.balance - old_paid
                    BankAccount.objects.filter(pk=old_transaction.bank_account.pk,shop=current_shop).update(balance=balance)
            
            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']       
            transaction_category = transaction_form.cleaned_data['transaction_category']
            amount = form.cleaned_data['payment_received']
            transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='booking_payment',category_type="income",is_deleted=False)[:1])
        
            #create Transaction
            data = transaction_form.save(commit=False)
            
            if transaction_mode == "cash":
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                balance = balance + amount
                CashAccount.objects.filter(pk=data.cash_account.pk,shop=current_shop).update(balance=balance)
            elif transaction_mode == "bank":
                balance = balance + amount
                BankAccount.objects.filter(pk=data.bank_account.pk,shop=current_shop).update(balance=balance)
                payment_mode = transaction_form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed'] 
                    data.card_details = None

                    if not is_cheque_withdrawed:
                        data.payment_to = None
                        data.bank_account = None
                        data.cash_account = None

                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None
                
            if not transaction_category == "credit":
                data.updator = request.user
                data.transaction_type = "income"
                data.transaction_category = transaction_categories
                data.amount = amount
                data.date = date
                data.shop = current_shop
                data.booking = data1
                data.save() 

                response_data = {
                    "status" : "true",
                    "title" : "Successfully Updated",
                    "message" : "Booking Successfully Updated.",
                    "redirect" : "true",
                    "redirect_url" : reverse('bookings:booking',kwargs={'pk':data1.pk})
                }   
        else:
            message = generate_form_errors(form,formset=False)     
            message += generate_form_errors(booking_item_formset,formset=True)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:        
        form = BookingForm(instance=instance)
        transaction_form = TransactionForm(instance=transaction) 
        booking_item_formset = BookingItemFormset(prefix='booking_item_formset',instance=instance)
        for item in booking_item_formset:
            item.fields['unit'].queryset = Measurement.objects.filter(shop=current_shop)
            item.fields['product'].queryset = Product.objects.filter(shop=current_shop)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "title" : "Edit Booking #: " + str(instance.auto_id),
            "instance" : instance,
            "url" : reverse('bookings:edit',kwargs={'pk':instance.pk}),
            "booking_item_formset" : booking_item_formset,
            "redirect" : True,
         
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'bookings/entry.html', context)


def delete_booking_fun(instance):
    old_balance = instance.balance 
    old_paid = instance.advance
    #update credit debit
    if not instance.customer.is_system_generated:
        update_customer_credit_debit(instance.customer.pk,"debit",old_balance)
    
    #update stock
    booking_items = BookingItem.objects.filter(booking=instance)
    for p in booking_items:
        room = p.room
        if RoomStatus.objects.filter(booking=instance,room=room).exists():
            RoomStatus.objects.filter(booking=instance,room=room).update(is_deleted=True) 
        p.is_deleted=True
        p.save()       

    #update account balance    
    if Transaction.objects.filter(booking=instance,transaction_category__name='booking_payment').exists():
        old_transaction = get_object_or_404(Transaction.objects.filter(booking=instance,transaction_category__name='booking_payment'))
        if old_transaction.cash_account:
            balance = old_transaction.cash_account.balance - old_paid
            CashAccount.objects.filter(pk=old_transaction.cash_account.pk).update(balance=balance)
        else  :
            balance = old_transaction.bank_account.balance - old_paid
            BankAccount.objects.filter(pk=old_transaction.bank_account.pk).update(balance=balance)
        
        old_transaction.is_deleted=True
        old_transaction.save()
        
    instance.is_deleted=True
    instance.save()
    
    
@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_booking'])
def delete_booking(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Booking.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    delete_booking_fun(instance)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Booking Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('bookings:bookings')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_booking'])
def delete_selected_bookings(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Booking.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
            delete_booking_fun(instance)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Booking(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('bookings:bookings')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def print_booking(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Booking.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    booking_items = BookingItem.objects.filter(booking=instance)

    to_word = inflect.engine()
    total_in_words = to_word.number_to_words(instance.total)
    
    # tax_categories = TaxCategory.objects.filter(shop=current_shop,is_deleted=False)
    # tax_percentage_dict = {}
    # for tax in tax_categories:
    #         items = booking_items.filter(product__tax_category=tax)

    #         tax_amount = 0
    #         if items :
    #             tax_amount = items.aggregate(tax_amount=Sum('tax_amount')).get("tax_amount",0)
    #         if tax_amount > 0:
    #             if instance.gst_type == "sgst":
    #                 each_amount = tax_amount/2
    #                 each_percentage = tax.tax/2
    #                 tax_values_dict = {}
    #                 tax_values_dict[str(each_percentage)] = each_amount
    #                 tax_percentage_dict[str(tax.tax)] = tax_values_dict
    # print tax_percentage_dict
    saved_amount = instance.discount
    context = {
        "total_in_words":total_in_words,
        "instance" : instance,
        "title" : "Booking : #" + str(instance.auto_id),
        "single_page" : True,
        "booking_items" : booking_items,
        "current_shop" : get_current_shop(request),
        "saved_amount" : saved_amount,
        "is_need_wave_effect" : True,

        "is_need_bootstrap_growl" : True,
    }
    template_name = 'bookings/' + current_shop.bill_print_type + '.html'
    return render(request,template_name,context)


@check_mode
@login_required
@shop_required
def email_booking(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Booking.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    if request.method == 'POST':
        form = EmailBookingForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            content = form.cleaned_data['content']
            content += "<br />"
            link = request.build_absolute_uri(reverse('bookings:print',kwargs={'pk':pk}))
            content += '<a href="%s">%s</a>' %(link,link)
            
            template_name = 'email/email.html'
            subject = "Purchase Details (#%s) | %s" %(str(instance.auto_id),current_shop.name)          
            context = {
                'name' : name,
                'subject' : subject,
                'content' : content,
                'email' : email
            }
            html_content = render_to_string(template_name,context) 
            send_email(email,subject,content,html_content) 
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Sent",
                "message" : "Booking Successfully Sent.",
                "redirect" : "true",
                "redirect_url" : reverse('bookings:booking',kwargs={'pk':pk})
            } 
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        email = instance.customer.email
        name = instance.customer.name
        content = "Thanks for your purchase from %s. Please follow the below link for your purchase details." %current_shop.name
        
        form = EmailBookingForm(initial={'name' : name, 'email' : email, 'content' : content})
        
        context = {
            "instance" : instance,
            "title" : "Email Booking : #" + str(instance.auto_id),
            "single_page" : True,
            'form' : form,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'bookings/email_booking.html',context)

        
@check_mode
@ajax_required
@login_required
@shop_required
def get_vacant_rooms(request):
    current_shop = get_current_shop(request)
    from_time = request.GET.get('from_time')
    to_time = request.GET.get('to_time')
    rate_type = current_shop.rate_type
    is_from_booking = request.GET.get('is_from_booking')
    customer = request.GET.get('customer')
    booking_pk = request.GET.get('booking_pk')
    template_name = 'bookings/includes/rooms.html'
    instances = Room.objects.filter(is_deleted=False)
    booked_rooms = []
    full_rooms = {}
    occupied_rooms = []
    self_booked_rooms = []
    if is_from_booking:
        booking = Booking.objects.get(pk=booking_pk)
        rate_type = booking.rate_type
        self_booked_rooms = BookingItem.objects.filter(booking__customer__pk=customer,booking__pk=booking_pk).values('room')     
    if from_time and to_time :
        from_time = datetime.datetime.strptime(from_time, '%m/%d/%Y %H:%M:%S').date()
        to_time = datetime.datetime.strptime(to_time, '%m/%d/%Y %H:%M:%S').date()        
        booked_rooms = RoomStatus.objects.filter(to_time__date__range=[from_time,to_time],is_booked=True,is_deleted=False).exclude(room__in=self_booked_rooms).values('room')
        occupied_rooms = RoomStatus.objects.filter(to_time__date__range=[from_time,to_time],is_occupied=True,is_deleted=False).values('room')
    full_rooms = instances
    instances = instances.exclude(pk__in=booked_rooms).exclude(pk__in=occupied_rooms).exclude(pk__in=self_booked_rooms)
    rooms = []
    #rooms with customer booking
    self_booked_rooms = full_rooms.filter(pk__in=self_booked_rooms)
    for instance in self_booked_rooms :
        if rate_type == 'normal' :
            rate = instance.normal_rate
        elif rate_type == "offtime" :
            rate = instance.offtime_rate
        elif rate_type == "peektime" :
            rate = instance.peaktime_rate
        room = {
            "pk" : str(instance.pk),
            "room_type" : instance.room_type,
            "code" :instance.code,
            "name" : instance.name,
            "rate" : rate,
            "category" : instance.category,
            "subcategory" : instance.subcategory,
            "total_adult" : instance.total_adult,
            "total_child" : instance.total_child,
            "self_booked" : True
        }
        rooms.append(room)
    #all booked rooms except customer
    full_booked_rooms = full_rooms.filter(pk__in=booked_rooms)
    for instance in full_booked_rooms :
        if rate_type == 'normal' :
            rate = instance.normal_rate
        elif rate_type == "offtime" :
            rate = instance.offtime_rate
        elif rate_type == "peektime" :
            rate = instance.peaktime_rate
        room = {
            "pk" : str(instance.pk),
            "room_type" : instance.room_type,
            "code" :instance.code,
            "name" : instance.name,
            "rate" : rate,
            "category" : instance.category,
            "subcategory" : instance.subcategory,
            "total_adult" : instance.total_adult,
            "total_child" : instance.total_child,
            "booked" : True
        }
        rooms.append(room)

    #all occupied rooms
    full_occupied_rooms = full_rooms.filter(pk__in=occupied_rooms)
    for instance in full_occupied_rooms :
        if rate_type == 'normal' :
            rate = instance.normal_rate
        elif rate_type == "offtime" :
            rate = instance.offtime_rate
        elif rate_type == "peektime" :
            rate = instance.peaktime_rate
        room = {
            "pk" : str(instance.pk),
            "room_type" : instance.room_type,
            "code" :instance.code,
            "name" : instance.name,
            "rate" : rate,
            "category" : instance.category,
            "subcategory" : instance.subcategory,
            "total_adult" : instance.total_adult,
            "total_child" : instance.total_child,
            "occupied" : True
        }
        rooms.append(room)

    #all vacant rooms
    for instance in instances :
        if rate_type == 'normal' :
            rate = instance.normal_rate
        elif rate_type == "offtime" :
            rate = instance.offtime_rate
        elif rate_type == "peektime" :
            rate = instance.peaktime_rate
        room = {
            "pk" : str(instance.pk),
            "room_type" : instance.room_type,
            "code" :instance.code,
            "name" : instance.name,
            "rate" : rate,
            "category" : instance.category,
            "subcategory" : instance.subcategory,
            "total_adult" : instance.total_adult,
            "total_child" : instance.total_child,
            "booked" : False
        }
        rooms.append(room)

    if rooms:        
        context = {
            'rooms' : rooms,
        }
        html_content = render_to_string(template_name,context)
        response_data = {
            "status" : "true",
            'template' : html_content,
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Rooms not found"
        }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_checkin'])
def create_checkin(request):    
    current_shop = get_current_shop(request)
    CheckinCustomerFormset = formset_factory(CheckinCustomerForm,extra=1)
    if request.method == 'POST':
        form = CheckinForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        customer_form = CustomerForm(request.POST,request.FILES) 
        checkin_customers_formset = CheckinCustomerFormset(request.POST,prefix='checkin_customers_formset')      
        if form.is_valid() and transaction_form.is_valid() and checkin_customers_formset.is_valid() and customer_form.is_valid(): 
            
            items = {}           

            customer_name = customer_form.cleaned_data['name']
            customer_address = customer_form.cleaned_data['address']
            customer_email = customer_form.cleaned_data['email']
            customer_phone = customer_form.cleaned_data['phone']
            customer = form.cleaned_data['customer']
            customer_state = customer_form.cleaned_data['state']
            customer_age = customer_form.cleaned_data['age']
            customer_document_category = customer_form.cleaned_data['document_category']
            customer_document_no = customer_form.cleaned_data['document_no'] 
            photo = customer_form.cleaned_data['photo']        
            document = customer_form.cleaned_data['document']
            signature = customer_form.cleaned_data['signature']

            if not customer:
                auto_id = get_auto_id(Customer)
                a_id = get_a_id(Customer,request)

                customer = Customer(
                    name = customer_name,
                    email = customer_email,
                    phone = customer_phone,
                    address = customer_address,
                    shop = current_shop,
                    first_time_credit = 0,
                    first_time_debit = 0,
                    credit = 0,
                    debit = 0,
                    creator = request.user,
                    updator = request.user,
                    auto_id = auto_id,
                    a_id = a_id,
                    state = customer_state,
                    photo = photo,
                    document = document,
                    age = customer_age,
                    document_category = customer_document_category,
                    document_no = customer_document_no
                )
                customer.save()
            else :
                if customer_document_no :                    
                    customer.document_no = customer_document_no
                    customer.document_category = customer_document_category
                    customer.save()
                if photo :
                    customer.photo = photo
                    customer.save()
                if document :
                    customer.document = document
                    customer.save()
                if signature :
                    customer.signature = signature
                    customer.save()
            
            auto_id = get_auto_id(Checkin)
            a_id = get_a_id(Checkin,request)            
            rate_type = current_shop.rate_type
            from_time = form.cleaned_data['from_time']
            to_time = form.cleaned_data['to_time'] 
            total_days = form.cleaned_data['total_days']
            payment = form.cleaned_data['payment']
            discount = form.cleaned_data['discount']

            # d1 = from_time.date()
            # d2 = to_time.date()
            # total_days =  abs(d1 - d2).days
            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.shop = current_shop
            data.a_id = a_id
            data.customer = customer
            data.rate_type = rate_type
            data.save()  
            all_subtotal = 0
            # Add Rooms
            rooms = request.POST.getlist('room') 
            for i in range(len(rooms)):        
                code =  rooms[i]
                room = Room.objects.get(code=code)              
                if rate_type == "normal" :
                    rate = room.normal_rate
                elif rate_type == "offtime":
                    rate = room.offtime_rate
                elif rate_type == "peektime":
                    rate = room.peaktime_rate  
                all_subtotal += rate * total_days
                CheckinItem(
                    checkin = data,
                    room = room,
                    rate = rate,                   
                ).save()
                RoomStatus.objects.create(from_time=from_time,to_time=to_time,is_occupied=True,room=room,checkin=data)
            #Add customers
            if checkin_customers_formset:
                for f in checkin_customers_formset:                
                    name = f.cleaned_data['name'] 
                    age = f.cleaned_data['age']
                    address = f.cleaned_data['address'] 
                    phone = f.cleaned_data['phone']
                    email = f.cleaned_data['email']
                    CheckinCustomer(
                        checkin = data,
                        name = name,
                        age = age,
                        address = address,
                        phone = phone,
                        email = email,                    
                    ).save()

            data.total = all_subtotal
            balance = all_subtotal - payment - discount
            data.balance = balance
            data.save() 
            if payment > 0 :
                update_customer_credit_debit(customer.pk,"credit",balance)
            add_transaction(request,transaction_form,data,payment,"checkin","income",0,0)   
            
            return HttpResponseRedirect(reverse('bookings:checkins'))                        
        
        else:            
            checkin_form = CheckinForm()
            transaction_form = TransactionForm()    
            customer_form = CustomerForm()  
            transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
            transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)     
            checkin_customers_formset = CheckinCustomerFormset(prefix='checkin_customers_formset')
            context = {
                "title" : "Create Checkin ",
                "form" : checkin_form,
                "url" : reverse('bookings:create_checkin'),
                "redirect" : True,
                "is_create_page" : True,
                "transaction_form" : transaction_form,
                "checkin_customers_formset" : checkin_customers_formset,
                "customer_form" : customer_form,
                
                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,

            }
            return render(request,'bookings/entry_checkin.html',context)
    
    else:
        default_customer = Customer.objects.get(name="default",address="default",shop=current_shop)
        checkin_form = CheckinForm(initial={"customer" : default_customer})
        transaction_form = TransactionForm()    
        customer_form = CustomerForm()  
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)     
        checkin_customers_formset = CheckinCustomerFormset(prefix='checkin_customers_formset')
        context = {
            "title" : "Create Checkin ",
            "form" : checkin_form,
            "url" : reverse('bookings:create_checkin'),
            "redirect" : True,
            "is_create_page" : True,
            "transaction_form" : transaction_form,
            "checkin_customers_formset" : checkin_customers_formset,
            "customer_form" : customer_form,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,

        }
        return render(request,'bookings/entry_checkin.html',context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_checkin'])
def checkins(request):
    current_shop = get_current_shop(request)
    instances = Checkin.objects.filter(is_deleted=False,shop=current_shop)
    customers = Customer.objects.filter(is_deleted=False,shop=current_shop)
    products = Product.objects.filter(is_deleted=False,shop=current_shop)
    today = datetime.date.today()
    customer = request.GET.get('customer')
    product = request.GET.get('product')
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    payment = request.GET.get('payment')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    gst = request.GET.get('gst')

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
        except ValueError:
            date_error = "yes"

    if customer:
        instances = instances.filter(customer_id=customer)
    if customer:
        instances = instances.filter(customer_id=customer)

    if year:
        instances = instances.filter(time__year=year)

    if month:
        instances = instances.filter(time__month=month)

    if payment :      
        if payment == "full":
            instances = instances.filter(balance=0)
        elif payment == "no":
            for instance in instances:
                payment_received = instance.payment_received
                if payment_received != 0:
                    instances = instances.exclude(pk = instance.pk)
        elif payment == "extra":
            for instance in instances:
                total = instance.total
                payment_received = instance.payment_received
                if total >= payment_received:
                    instances = instances.exclude(pk=instance.pk)
        elif payment == "partial":
            for instance in instances:
                total = instance.total
                payment_received = instance.payment_received
                if total <= payment_received or payment_received == 0:
                    instances = instances.exclude(pk=instance.pk)
                    
    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True

    if period :
        if period =="year":
            instances = instances.filter(time__year=today.year)
        elif period == 'month' :
            instances = instances.filter(time__year=today.year,time__month=today.month)
        elif period == "today" :
            instances = instances.filter(time__year=today.year,time__month=today.month,time__day=today.day)

    elif filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, time__range=[from_date, to_date])
            
    elif date:
        title = "Report : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(time__month=date.month,time__year=date.year,time__day=date.day)

    if gst :
        if gst =="gst":
            instances = instances.filter(customer_gstin_invoice_id__isnull=False)
        elif gst == 'no_gst' :
            instances = instances.filter(customer_non_gstn_invoice_id__isnull=False)

    context = {
        'instances': instances,
        'checkins' :checkins,
        'customers' : customers,
        'products' : products,
        "title" : 'Checkins',

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "checkins" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,    
    }
    return render(request,'bookings/checkins.html',context) 


@check_mode
@login_required 
@shop_required
@permissions_required(['can_view_checkin'])
def checkin_items(request):
    current_shop = get_current_shop(request)
    instances = CheckinItem.objects.filter(is_deleted=False) 
    products = Product.objects.filter(is_deleted=False,shop=current_shop)   
    product = request.GET.get('product')    
    if product:
        instances = instances.filter(product_id=product)
    
    context = {
        'instances': instances,
        "title" : 'Checkin Items',
        "products" :products,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "checkins" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,    
    }
    return render(request,'bookings/checkin_items.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_checkin'])
def checkin(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Checkin.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    transactions = Transaction.objects.filter(checkin=instance,shop=current_shop,collect_amount=None)
    checkin_items = CheckinItem.objects.filter(checkin=instance,is_deleted=False)
    context = {
        "instance" : instance,
        "transactions" : transactions,
        "title" : "Checkin : #" + str(instance.auto_id),       
        "checkin_items" : checkin_items,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'bookings/checkin.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_modify_checkin'])
def edit_checkin(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Checkin.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
    transaction = get_object_or_404(Transaction.objects.filter(checkin=instance,shop=current_shop,collect_amount=None))        

    if request.method == 'POST':
        response_data = {}
        form = CheckinForm(request.POST,instance=instance)
        transaction_form = TransactionForm(request.POST,instance=transaction)        
        if form.is_valid() and transaction_form.is_valid():            
            old_balance = Checkin.objects.get(pk=pk).balance
            old_advance = Checkin.objects.get(pk=pk).advance  
            old_transaction = get_object_or_404(Transaction.objects.filter(checkin=pk,collect_amount=None))                
            items = {}
            total_discount_amount = 0
            total_tax_amount = 0                 
                
            customer_name = form.cleaned_data['customer_name']
            customer_address = form.cleaned_data['customer_address']
            customer_email = form.cleaned_data['customer_email']
            customer_phone = form.cleaned_data['customer_phone']
            customer = form.cleaned_data['customer']
            customer_state = form.cleaned_data['customer_state']
            customer_gsttin = form.cleaned_data['customer_gsttin']

            if not customer:
                auto_id = get_auto_id(Customer)
                a_id = get_a_id(Customer,request)

                customer = Customer(
                    name = customer_name,
                    email = customer_email,
                    phone = customer_phone,
                    address = customer_address,
                    shop = current_shop,
                    first_time_credit = 0,
                    first_time_debit = 0,
                    credit = 0,
                    debit = 0,
                    creator = request.user,
                    updator = request.user,
                    auto_id = auto_id,
                    a_id = a_id,
                    state = customer_state,
                    gst_tin = customer_gsttin
                )
                customer.save()

            
            #update checkin
            advance = form.cleaned_data['advance']
            total_days = form.cleaned_data['total_days']
            date = form.cleaned_data['time']
            
            data1 = form.save(commit=False)
            data1.updator = request.user
            data1.date_updated = datetime.datetime.now()
            data1.save() 
            all_subtotal = 0
            
            #delete previous items and update stock
            previous_checkin_items = CheckinItem.objects.filter(checkin=instance)
            for p in previous_checkin_items: 
                room = p.room
                RoomStatus.objects.filter(room=room,checkin=instance).update(is_deleted=True)               
            previous_checkin_items.delete()
            
            #save items
            for key, value in items.iteritems():
                product = Product.objects.get(pk=key)
                qty = value["qty"]
                price = value["price"]
                cost = value["cost"]
                tax = product.tax
                discount = product.discount
                tax_amount = value["tax_amount"]
                discount_amount = value["discount_amount"]
                tax_added_price = value["tax_added_price"]
                unit = value["unit"]                    
                subtotal = (qty * price) - discount_amount + tax_amount
                unit = Measurement.objects.get(pk=unit)
                exact_qty = get_exact_qty(qty,unit)
                all_subtotal += subtotal
                CheckinItem(
                    checkin = data1,
                    product = product,
                    qty = qty,
                    cost = cost,
                    price = price,
                    tax = tax,
                    discount = discount,
                    tax_amount = tax_amount,
                    discount_amount = discount_amount,
                    subtotal = subtotal,
                    unit = unit,
                    tax_added_price = tax_added_price
                ).save()
                
                update_sock(product.pk,exact_qty,"decrease")

            credit=0
            debit=0
            if customer:
                if not customer.is_system_generated:
                    credit = customer.credit
                    debit = customer.debit
                
            total = all_subtotal - special_discount + credit - debit
            total_total = total
            if current_shop.remove_previous_balance_from_bill:
                total_total = all_subtotal - special_discount

            total = all_subtotal - special_discount
            rounded_total = Decimal(round(total))
            balance = rounded_total - payment_received
            extra = Decimal(round(total)) - total
            round_off = Decimal(format(extra, '.2f'))
            balance = Decimal(balance)
            this_checkin_balance = (all_subtotal - special_discount) - payment_received - instance.collected_amount

            Checkin.objects.filter(pk=data1.pk).update(subtotal=all_subtotal,total=total_total,balance=this_checkin_balance,round_off=round_off)
            
            if not data1.customer.is_system_generated:
                #update credit
                update_customer_credit_debit(data1.customer.pk,"debit",old_balance)
                
                if balance > 0:
                    balance = balance
                    update_customer_credit_debit(data1.customer.pk,"credit",balance)
                elif balance < 0:
                    balance = abs(balance)
                    update_customer_credit_debit(data1.customer.pk,"debit",balance)

            #update account balance
            if Transaction.objects.filter(checkin=pk).exists(): 
                if old_transaction.cash_account:
                    balance = old_transaction.cash_account.balance - old_paid
                    CashAccount.objects.filter(pk=old_transaction.cash_account.pk,shop=current_shop).update(balance=balance)
                else  :
                    balance = old_transaction.bank_account.balance - old_paid
                    BankAccount.objects.filter(pk=old_transaction.bank_account.pk,shop=current_shop).update(balance=balance)
            
            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']       
            transaction_category = transaction_form.cleaned_data['transaction_category']
            amount = form.cleaned_data['payment_received']
            transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='checkin',category_type="income",is_deleted=False)[:1])
        
            #create Transaction
            data = transaction_form.save(commit=False)
            
            if transaction_mode == "cash":
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                balance = balance + amount
                CashAccount.objects.filter(pk=data.cash_account.pk,shop=current_shop).update(balance=balance)
            elif transaction_mode == "bank":
                balance = balance + amount
                BankAccount.objects.filter(pk=data.bank_account.pk,shop=current_shop).update(balance=balance)
                payment_mode = transaction_form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed'] 
                    data.card_details = None

                    if not is_cheque_withdrawed:
                        data.payment_to = None
                        data.bank_account = None
                        data.cash_account = None

                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None
                
            if not transaction_category == "credit":
                data.updator = request.user
                data.transaction_type = "income"
                data.transaction_category = transaction_categories
                data.amount = amount
                data.date = date
                data.shop = current_shop
                data.checkin = data1
                data.save() 

                response_data = {
                    "status" : "true",
                    "title" : "Successfully Updated",
                    "message" : "Checkin Successfully Updated.",
                    "redirect" : "true",
                    "redirect_url" : reverse('bookings:checkin',kwargs={'pk':data1.pk})
                }   
        else:
            message = generate_form_errors(form,formset=False)     
            message += generate_form_errors(checkin_item_formset,formset=True)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:        
        form = CheckinForm(instance=instance)
        transaction_form = TransactionForm(instance=transaction)   
        customer_form = CustomerForm(instance=instance.customer)     
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "customer_form" : customer_form,
            "title" : "Edit Checkin #: " + str(instance.auto_id),
            "instance" : instance,
            "url" : reverse('bookings:edit_checkin',kwargs={'pk':instance.pk}),
            "redirect" : True,
         
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'bookings/entry_checkin.html', context)


def delete_checkin_fun(instance):
    old_balance = instance.balance 
    old_paid = instance.payment
    #update credit debit
    if not instance.customer.is_system_generated:
        update_customer_credit_debit(instance.customer.pk,"debit",old_balance)
    
    #update stock
    checkin_items = CheckinItem.objects.filter(checkin=instance)
    for p in checkin_items:
        room = p.room
        if RoomStatus.objects.filter(checkin=instance,room=room).exists():
            RoomStatus.objects.filter(checkin=instance,room=room).update(is_deleted=True)
        p.is_deleted=True
        p.save()

    #update account balance    
    if Transaction.objects.filter(checkin=instance,transaction_category__name='checkin').exists():
        old_transaction = get_object_or_404(Transaction.objects.filter(checkin=instance,transaction_category__name='checkin'))
        if old_transaction.cash_account:
            balance = old_transaction.cash_account.balance - old_paid
            CashAccount.objects.filter(pk=old_transaction.cash_account.pk).update(balance=balance)
        else  :
            balance = old_transaction.bank_account.balance - old_paid
            BankAccount.objects.filter(pk=old_transaction.bank_account.pk).update(balance=balance)
        
        old_transaction.is_deleted=True
        old_transaction.save()
        
    instance.is_deleted=True
    instance.save()
    
    
@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_checkin'])
def delete_checkin(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Checkin.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    delete_checkin_fun(instance)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Checkin Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('bookings:checkins')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_checkin'])
def delete_selected_checkins(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Checkin.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
            delete_checkin_fun(instance)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Checkin(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('bookings:checkins')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def print_checkin(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Checkin.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    checkin_items = CheckinItem.objects.filter(checkin=instance)
    customers = CheckinCustomer.objects.filter(checkin=instance)

    to_word = inflect.engine()
    total_in_words = to_word.number_to_words(instance.total)    
    context = {
        "total_in_words":total_in_words,
        "instance" : instance,
        "title" : "Checkin : #" + str(instance.auto_id),
        "single_page" : True,
        "checkin_items" : checkin_items,
        "customers" : customers,
        "current_shop" : get_current_shop(request),
        "is_need_wave_effect" : True,

        "is_need_bootstrap_growl" : True,
    }
    template_name = 'bookings/print_checkin.html'
    return render(request,template_name,context)


@check_mode
@login_required
@shop_required
def email_checkin(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Checkin.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    
    if request.method == 'POST':
        form = EmailCheckinForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            content = form.cleaned_data['content']
            content += "<br />"
            link = request.build_absolute_uri(reverse('bookings:print',kwargs={'pk':pk}))
            content += '<a href="%s">%s</a>' %(link,link)
            
            template_name = 'email/email.html'
            subject = "Purchase Details (#%s) | %s" %(str(instance.auto_id),current_shop.name)          
            context = {
                'name' : name,
                'subject' : subject,
                'content' : content,
                'email' : email
            }
            html_content = render_to_string(template_name,context) 
            send_email(email,subject,content,html_content) 
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Sent",
                "message" : "Checkin Successfully Sent.",
                "redirect" : "true",
                "redirect_url" : reverse('bookings:checkin',kwargs={'pk':pk})
            } 
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        email = instance.customer.email
        name = instance.customer.name
        content = "Thanks for your purchase from %s. Please follow the below link for your purchase details." %current_shop.name
        
        form = EmailCheckinForm(initial={'name' : name, 'email' : email, 'content' : content})
        
        context = {
            "instance" : instance,
            "title" : "Email Checkin : #" + str(instance.auto_id),
            "single_page" : True,
            'form' : form,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'bookings/email_checkin.html',context)

@check_mode
@login_required
@shop_required
@permissions_required(['can_create_checkout'])
def create_checkout(request,pk):  
    current_shop = get_current_shop(request)
    checkin = Checkin.objects.get(pk=pk)
    checkin_items = CheckinItem.objects.filter(checkin=checkin)
    customer_credits = CustomerCredit.objects.filter(customer=checkin.customer,sale__isnull=False,is_paid=False)

    if request.method == "POST": 
        form = CheckoutForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        if form.is_valid() and transaction_form.is_valid():

            auto_id = get_auto_id(Checkout)
            a_id = get_a_id(Checkout,request)
            #get values from form
            collect_amount = form.cleaned_data['collect_amount']
            date = form.cleaned_data['date']

            instance = Customer.objects.get(pk=checkin.customer.pk,is_deleted=False)
            balance = instance.credit
            
            remaining_balance = balance - collect_amount
            credit = remaining_balance
            debit = instance.debit
            if remaining_balance <= 0:
                credit = 0
                if instance.debit > 0:
                    debit = abs(remaining_balance) + instance.debit
                else:
                    debit = abs(remaining_balance)
                remaining_balance = 0
            
            data1 = form.save(commit=False)
            data1.creator = request.user
            data1.updator = request.user
            data1.auto_id = auto_id
            data1.a_id = a_id
            data1.balance = balance
            data1.remaining_balance = remaining_balance
            data1.shop = current_shop 
            data1.checkin = checkin  
            data1.customer = instance        
            data1.save()
            if not instance.is_system_generated:
                instance.credit = credit
                instance.debit = debit
                instance.save()
            total_expense = 0
            for customer_credit in customer_credits :
                total_expense += customer_credit.amount
                sale = customer_credit.sale
                sale.balance = 0
                sale.save()
                customer_credit.checkin = checkin
                customer_credit.is_paid = True
                customer_credit.save()
            data1.other_expense = total_expense
            data1.rent_balance = checkin.balance
            data1.save()

            if Checkin.objects.filter(is_deleted=False,customer=instance).exists():                
                latest_checkin = Checkin.objects.filter(is_deleted=False,customer=instance,shop=current_shop).latest('date_added')
                balance = latest_checkin.balance - collect_amount
                latest_checkin = Checkin.objects.filter(pk=latest_checkin.pk)
                if remaining_balance < 0:
                    remaining_balance = -debit
                latest_checkin.update(balance=balance,collected_amount=collect_amount,is_checkout=True)
                for i in checkin_items :
                    room = i.room
                    if RoomStatus.objects.filter(checkin=checkin,room=room,is_occupied=True).exists():
                        RoomStatus.objects.filter(checkin=checkin,room=room,is_occupied=True).update(is_deleted=True)

            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']       
            transaction_category = transaction_form.cleaned_data['transaction_category']
            amount = form.cleaned_data['collect_amount']
            transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='customer_payment',category_type="income",is_deleted=False)[:1])
        
            #create income
            data = transaction_form.save(commit=False)
            
            if transaction_mode == "cash":
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                balance = data.cash_account.balance
                balance = balance + amount
                CashAccount.objects.filter(pk=data.cash_account.pk,shop=current_shop).update(balance=balance)
            elif transaction_mode == "bank":
                balance = 0
                balance = data.bank_account.balance
                balance = balance + amount
                BankAccount.objects.filter(pk=data.bank_account.pk,shop=current_shop).update(balance=balance)
                payment_mode = transaction_form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed'] 
                    data.card_details = None

                    if not is_cheque_withdrawed:
                        data.payment_to = None
                        data.bank_account = None
                        data.cash_account = None

                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None
                
            if not transaction_category == "credit":
                data.auto_id = get_auto_id(Transaction) 
                data.a_id = get_a_id(Transaction,request)            
                data.creator = request.user
                data.updator = request.user
                data.transaction_type = "income"
                data.transaction_category = transaction_categories
                data.amount = amount
                data.date = date                
                data.checkout = data1
                data.shop = current_shop
                data.save()    
           
            response_data = {
                "status" : "true",
                "title" : "Succesfully Created",
                "redirect" : "true",
                "redirect_url" : reverse('bookings:checkout',kwargs = {'pk' :data1.pk}),
                "message" : "Checkout Successfully."
            }
        else:
            message = generate_form_errors(form,formset=False)        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }            
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:         
        form = CheckoutForm(initial={"customer" : checkin.customer})
        checkin_rooms = CheckinItem.objects.filter(checkin=checkin)
        transaction_form = TransactionForm()
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "title" : "Checkout",
            "is_create_page" : True,
            "checkin" : checkin,
            "checkin_rooms" : checkin_rooms,
            "customer_credits" : customer_credits,
            'customer' : checkin.customer,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            
        }
        return render(request, 'bookings/entry_checkout.html', context)


@check_mode
@login_required 
@shop_required
@permissions_required(['can_modify_checkout'],allow_self=True,model=Checkout)
def edit_checkout(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Checkout.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    transaction = get_object_or_404(Transaction.objects.filter(checkout=instance,shop=current_shop)) 

    if request.method == "POST":
        response_data = {}  
        old_collected_amount = instance.collect_amount
        balance = 0
        form = CheckoutForm(request.POST,instance=instance)
        transaction_form = TransactionForm(request.POST,instance=transaction)
        if form.is_valid() and transaction_form.is_valid(): 
            
            #initialize customer credit
            
            update_customer_credit_debit(instance.customer.pk,"credit",old_collected_amount)
            amount = form.cleaned_data['collect_amount']
            date = form.cleaned_data['date']
            data1 = form.save(commit=False)
            data1.updator = request.user
            data1.date_updated = datetime.datetime.now()
            data1.save()
            #update customer credit
            update_customer_credit_debit(instance.customer.pk,"debit",amount)

            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']       
            transaction_category = transaction_form.cleaned_data['transaction_category']            
            transaction_categories = get_object_or_404(TransactionCategory.objects.filter(name='customer_payment',category_type="income",is_deleted=False)[:1])
            
            #update account balance
            if Transaction.objects.filter(checkout=pk).exists(): 
                if transaction.cash_account:
                    balance = transaction.cash_account.balance - old_collected_amount
                    CashAccount.objects.filter(pk=transaction.cash_account.pk,shop=current_shop).update(balance=balance)
                else  :
                    balance = transaction.bank_account.balance - old_collected_amount
                    BankAccount.objects.filter(pk=transaction.bank_account.pk,shop=current_shop).update(balance=balance)
            #create transaction
            data = transaction_form.save(commit=False)
            
            if transaction_mode == "cash":
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                balance = balance + amount
                CashAccount.objects.filter(pk=data.cash_account.pk,shop=current_shop).update(balance=balance)
            elif transaction_mode == "bank":
                balance = balance + amount
                BankAccount.objects.filter(pk=data.bank_account.pk,shop=current_shop).update(balance=balance)
                payment_mode = transaction_form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed'] 
                    data.card_details = None

                    if not is_cheque_withdrawed:
                        data.payment_to = None
                        data.bank_account = None
                        data.cash_account = None

                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None
                
            if not transaction_category == "credit":
                data.updator = request.user
                data.transaction_type = "income"
                data.transaction_category = transaction_categories
                data.amount = amount
                data.date = date
                latest_sale = Sale.objects.filter(is_deleted=False,customer=data1.customer,shop=current_shop).latest('date_added')            
                data.sale = latest_sale
                data.checkout = data1
                data.shop = current_shop
                data.save()  

            response_data = {
                "status" : "true",
                "title" : "Succesfully Updated",
                "redirect" : "true",
                "redirect_url" : reverse('bookings:checkout', kwargs = {'pk' :pk}),
                "message" : "Collect Amount Successfully Updated."
            }
        else:
            message = generate_form_errors(form,formset=False)        
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        checkin_rooms = CheckinItem.objects.filter(checkin=instance.checkin) 
        form = CheckoutForm(instance=instance)
        transaction_form = TransactionForm(instance=transaction)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "title" : "Edit Collect Amount : " + str(instance.collect_amount),
            "instance" : instance,
            "checkin_rooms" : checkin_rooms,
            "checkin" : instance.checkin,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'bookings/entry_checkout.html', context)

    

@check_mode
@login_required
@shop_required
@permissions_required(['can_view_checkout'])
def checkouts(request):
    current_shop = get_current_shop(request)
    instances = Checkout.objects.filter(is_deleted=False,shop=current_shop)

    title = "Checkout"
    
    #filter by query
    query = request.GET.get("q")
    if query:
        title = "Checkout (Query - %s)" % query
        instances = instances.filter(Q(collect_amount__icontains=query) | Q(date__icontains=query) | Q(customer__name__icontains=query))
        
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'bookings/checkouts.html',context) 


@check_mode
@login_required
@shop_required
@permissions_required(['can_view_checkout'],allow_self=True,model=Checkout)
def checkout(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Checkout.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    transaction = get_object_or_404(Transaction.objects.filter(checkout=instance,shop=current_shop))
    context = {
        "instance" : instance,
        "transaction" : transaction,
        "title" : "Collect Amount: " + str(instance.collect_amount),
        
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'bookings/checkout.html',context)


def delete_checkout_fun(checkout_obj):
    checkin = checkout_obj.checkin
    checkin_items = CheckinItem.objects.filter(checkin=checkin)
    collect_amount = checkout_obj.collect_amount
    for p in checkin_items:
        room = p.room
        RoomStatus.objects.create(from_time=checkin.from_time,to_time=checkin.to_time,is_occupied=True,room=room,checkin=checkin)

    checkin.is_checkout = False
    checkin.balance = checkin.balance + collect_amount
    checkin.collected_amount = checkin.collected_amount - collect_amount
    checkin.save()
    if Customer.objects.filter(pk=checkout_obj.customer.pk,is_deleted=False):
        credit = Customer.objects.get(pk=checkout_obj.customer.pk,is_deleted=False).credit + checkout_obj.collect_amount
        new_credit = credit + checkout_obj.collect_amount
        Customer.objects.filter(pk=checkout_obj.customer.pk,is_deleted=False).update(credit=credit)


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_checkout'],allow_self=True,model=Checkout)
def delete_checkout(request,pk):
    current_shop = get_current_shop(request)
    checkout_obj = Checkout.objects.get(pk=pk)
    delete_checkin_fun(checkout_obj)
    Checkout.objects.filter(pk=pk).update(is_deleted=True,shop=current_shop)
    
    response_data = {
        "status" : "true",
        "title" : "Succesfully Deleted",
        "redirect" : "true",
        "redirect_url" : reverse('bookings:checkouts'),
        "message" : "Collect Amount Successfully Deleted."
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@shop_required
@permissions_required(['can_delete_checkout'])
def delete_selected_checkouts(request):
    current_shop = get_current_shop(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Checkout.objects.filter(pk=pk,is_deleted=False,shop=current_shop)) 
            delete_checkin_fun(instance)
            Checkout.objects.filter(pk=pk).update(is_deleted=True)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Checkout(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('bookings:checkouts')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some Collect Amount first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def print_checkout(request,pk):
    current_shop = get_current_shop(request)
    instance = get_object_or_404(Checkout.objects.filter(pk=pk,is_deleted=False,shop=current_shop))
    checkin = instance.checkin
    customer_credits = CustomerCredit.objects.filter(checkin=checkin)
    context = {
        "instance" : instance,
        "title" : "Checkout : #" + str(instance.auto_id),
        "single_page" : True,
        "current_shop" : current_shop,
        "customer_credits" : customer_credits,

        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
    }
    return render(request,'bookings/print_checkout.html',context)


def print_checkouts(request):
    current_shop = get_current_shop(request)
    today = datetime.date.today()
    title = "Sale payment"
    instances = Checkout.objects.filter(is_deleted=False,shop=current_shop)
    query = request.GET.get('q')
    if query :
        instances = instances.filter(Q(auto_id__icontains=query) | Q(customer__name__icontains=query))

    date = request.GET.get('date')
    date_error = "no"

    if date :
        try:
            date = datetime.datetime.strptime(date, '%d/%m/%Y').date()          
        except ValueError:
            date_error = "yes" 

    period = request.GET.get('period')

    filter_period = None
    if period:
        if period == "today" or period == "month" or period == "year":
            filter_period = period
    
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date') 
    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%d/%m/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%d/%m/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True


    if filter_period :
        if period == "today":
            title = "Purchases : Today"

            instances = instances.filter(date__year=today.year, date__month=today.month, date__day=today.day)
            total_purchases_created = instances.count()
                
        elif period == "month":
            title = "Purchases : This Month"
            instances = instances.filter(date__year=today.year, date__month=today.month)
            
        elif period == "year":
            title = "Purchases : This Year"
            instances = purchases.filter(date__year=today.year)
    elif filter_date_period:
        title = "Purchases : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(date__range=[from_date, to_date])
            total_purchases_created = instances.count()
    elif date:
        title = "Purchases : Date : %s" %(str(date))
        if date_error == "no":
            instances = instances.filter(date__year=date.year, date__month=date.month, date__day=date.day)
    context = {
        "instances" : instances,
        "title" : title,
        "single_page" : True,
        "current_shop" : current_shop,

        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
    }
    return render(request,'bookings/print_checkouts.html',context)


@check_mode
@login_required
@shop_required
@permissions_required(['can_create_checkin_booking'])
def create_checkin_booking(request,pk):    
    current_shop = get_current_shop(request)
    CheckinCustomerFormset = formset_factory(CheckinCustomerForm,extra=1)
    booking = get_object_or_404(Booking.objects.filter(is_deleted=False,pk=pk))
    booking_items = BookingItem.objects.filter(is_deleted=False,booking=booking)
    bookin_item_code = booking_items.values('room__code')
    if request.method == 'POST':
        form = CheckinForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        customer_form = CustomerForm(request.POST,request.FILES) 
        checkin_customers_formset = CheckinCustomerFormset(request.POST,prefix='checkin_customers_formset')      
        if form.is_valid() and transaction_form.is_valid() and checkin_customers_formset.is_valid() and customer_form.is_valid(): 

            items = {}           

            customer_name = customer_form.cleaned_data['name']
            customer_address = customer_form.cleaned_data['address']
            customer_email = customer_form.cleaned_data['email']
            customer_phone = customer_form.cleaned_data['phone']
            customer = form.cleaned_data['customer']
            customer_state = customer_form.cleaned_data['state']
            customer_age = customer_form.cleaned_data['age']
            customer_document_category = customer_form.cleaned_data['document_category']
            customer_document_no = customer_form.cleaned_data['document_no'] 
            photo = customer_form.cleaned_data['photo']        
            document = customer_form.cleaned_data['document']
            signature = customer_form.cleaned_data['signature']


            if not customer:
                auto_id = get_auto_id(Customer)
                a_id = get_a_id(Customer,request)

                customer = Customer(
                    name = customer_name,
                    email = customer_email,
                    phone = customer_phone,
                    address = customer_address,
                    shop = current_shop,
                    first_time_credit = 0,
                    first_time_debit = 0,
                    credit = 0,
                    debit = 0,
                    creator = request.user,
                    updator = request.user,
                    auto_id = auto_id,
                    a_id = a_id,
                    state = customer_state,
                    photo = photo,
                    document = document,
                    age = customer_age,
                    document_category = customer_document_category,
                    document_no = customer_document_no
                )
                customer.save()
            else :
                if customer_document_no :                    
                    customer.document_no = customer_document_no
                    customer.document_category = customer_document_category
                    customer.save()
                if photo :
                    customer.photo = photo
                    customer.save()
                if document :
                    customer.document = document
                    customer.save()
                if signature :
                    customer.signature = signature
                    customer.save()
            
            auto_id = get_auto_id(Checkin)
            a_id = get_a_id(Checkin,request)            
            rate_type = booking.rate_type
            from_time = form.cleaned_data['from_time']
            to_time = form.cleaned_data['to_time'] 
            total_days = form.cleaned_data['total_days']
            payment = form.cleaned_data['payment']
            discount = form.cleaned_data['discount'] 

            # d1 = from_time.date()
            # d2 = to_time.date()
            # total_days =  abs(d1 - d2).days
            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.shop = current_shop
            data.a_id = a_id
            data.rate_type = rate_type
            data.customer = customer
            data.save()  
            all_subtotal = 0
            # Add Rooms
            rooms = request.POST.getlist('room')
            for b_room in booking_items:
                if not b_room.room.code in rooms:
                    RoomStatus.objects.filter(from_time=booking.from_time,to_time=booking.to_time,is_booked=True,room=b_room.room,booking=booking).update(is_deleted=True)
            for i in range(len(rooms)):        
                code =  rooms[i]
                room = Room.objects.get(code=code)              
                if rate_type == "normal" :
                    rate = room.normal_rate
                elif rate_type == "offtime":
                    rate = room.offtime_rate
                elif rate_type == "peektime":
                    rate = room.peaktime_rate  
                all_subtotal += rate * total_days
                CheckinItem(
                    checkin = data,
                    room = room,
                    rate = rate,                   
                ).save()
                RoomStatus.objects.create(from_time=from_time,to_time=to_time,is_occupied=True,room=room,checkin=data)
                RoomStatus.objects.filter(from_time=booking.from_time,to_time=booking.to_time,is_booked=True,room=room,booking=booking).update(is_deleted=True)
            #Add customers
            if checkin_customers_formset:
                for f in checkin_customers_formset:                
                    name = f.cleaned_data['name'] 
                    age = f.cleaned_data['age']
                    address = f.cleaned_data['address'] 
                    phone = f.cleaned_data['phone']
                    email = f.cleaned_data['email']
                    CheckinCustomer(
                        checkin = data,
                        name = name,
                        age = age,
                        address = address,
                        phone = phone,
                        email = email,                    
                    ).save()

            data.total = all_subtotal
            data.advance = booking.advance
            balance = all_subtotal - payment - discount - booking.advance
            data.balance = balance
            data.save() 
            booking.is_checkin = True
            booking.save()
            if balance > 0 :
                CustomerCredit.objects.create(shop=current_shop,checkin=data,customer=customer,amount=balance)
            if payment > 0 :
                update_customer_credit_debit(customer.pk,"debit",balance)
            add_transaction(request,transaction_form,data,payment,"checkin","income",0,0)   
            
            return HttpResponseRedirect(reverse('bookings:checkins'))                        
        
        else:           
            default_customer = Customer.objects.get(name="default",address="default",shop=current_shop)
            checkin_form = CheckinForm(initial={"customer" : default_customer})
            transaction_form = TransactionForm()    
            customer_form = CustomerForm()  
            transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
            transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)     
            checkin_customers_formset = CheckinCustomerFormset(prefix='checkin_customers_formset')
            context = {
                "title" : "Create Checkin ",
                "form" : checkin_form,
                "url" : reverse('bookings:create_checkin_booking',kwargs={'pk':booking.pk}),
                "redirect" : True,
                "is_create_page" : True,
                "transaction_form" : transaction_form,
                "checkin_customers_formset" : checkin_customers_formset,
                "customer_form" : customer_form,
                
                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,

            }
            return render(request,'bookings/entry_checkin.html',context)
    
    else:             
        initial = []
        for item in booking_items:
            booking_dict = {
                'room': item.room,
                'rate' : int(item.rate),
            }
            initial.append(booking_dict)
        checkin_form = CheckinForm(instance=booking,initial={"from_time" : booking.from_time,"to_time" : booking.to_time})
        if booking.advance>0 :
            transaction = get_object_or_404(Transaction.objects.filter(booking=booking,shop=current_shop,collect_amount=None)) 
            transaction_form = TransactionForm(instance=transaction)  
        else : 
            transaction_form = TransactionForm() 
        customer_form = CustomerForm()  
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(shop=current_shop,is_deleted=False)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(shop=current_shop,is_deleted=False)     
        checkin_customers_formset = CheckinCustomerFormset(prefix='checkin_customers_formset')
        context = {
            "title" : "Create Checkin ",
            "form" : checkin_form,
            "url" : reverse('bookings:create_checkin_booking',kwargs={'pk':booking.pk}),
            "redirect" : True,
            "is_create_page" : True,
            "transaction_form" : transaction_form,
            "checkin_customers_formset" : checkin_customers_formset,
            "customer_form" : customer_form,
            "is_from_booking" : True,
            "booking" : booking,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,

        }
        return render(request,'bookings/entry_checkin.html',context)


