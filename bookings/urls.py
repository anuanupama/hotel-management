from django.conf.urls import url, include
from django.contrib import admin
import views
from sales.views import SaleAutocomplete,ReturnableProductAutocomplete


urlpatterns = [               
    url(r'^booking-autocomplete/$',SaleAutocomplete.as_view(),name='booking_autocomplete'),
    
    url(r'^booking/create/$',views.create_booking,name='create_booking'),
    url(r'^booking/views/$',views.bookings,name='bookings'),
    url(r'^booking/edit/(?P<pk>.*)/$',views.edit_booking,name='edit_booking'),
    url(r'^booking/view/(?P<pk>.*)/$',views.booking,name='booking'),
    url(r'^booking/delete/(?P<pk>.*)/$',views.delete_booking,name='delete_booking'),
    url(r'^booking/delete-selected/$', views.delete_selected_bookings, name='delete_selected_bookings'),
    url(r'^invoice/(?P<pk>.*)/$',views.print_booking,name='print_booking'),
    url(r'^email/(?P<pk>.*)/$',views.email_booking,name='email_booking'),  

    url(r'^checkin-from-boking/create/(?P<pk>.*)$',views.create_checkin_booking,name='create_checkin_booking'),

    url(r'^get-vacant-rooms/$', views.get_vacant_rooms,name='get_vacant_rooms'), 

    url(r'^checkin/create/$',views.create_checkin,name='create_checkin'),
    url(r'^checkin/views/$',views.checkins,name='checkins'),
    url(r'^checkin/edit/(?P<pk>.*)/$',views.edit_checkin,name='edit_checkin'),
    url(r'^checkin/view/(?P<pk>.*)/$',views.checkin,name='checkin'),
    url(r'^checkin/delete/(?P<pk>.*)/$',views.delete_checkin,name='delete_checkin'),
    url(r'^checkin/delete-selected/$', views.delete_selected_checkins, name='delete_selected_checkins'),
    url(r'^print/checkin/(?P<pk>.*)/$',views.print_checkin,name='print_checkin'),
    url(r'^email/(?P<pk>.*)/$',views.email_checkin,name='email_checkin'),  

    url(r'^checkout/create/(?P<pk>.*)/$', views.create_checkout, name='create_checkout'),
    url(r'^checkout/view/(?P<pk>.*)/$', views.checkout, name='checkout'),
    url(r'^checkout/edit/(?P<pk>.*)/$', views.edit_checkout, name='edit_checkout'),
    url(r'^checkouts/$', views.checkouts, name='checkouts'),
    url(r'^checkout/delete/(?P<pk>.*)/$', views.delete_checkout, name='delete_checkout'),
    url(r'^checkout/delete-selected/$', views.delete_selected_checkouts, name='delete_selected_checkouts'),
    url(r'^checkout/print/(?P<pk>.*)/$',views.print_checkout,name='print_checkout'),
    url(r'^checkouts/print/$',views.print_checkouts,name='print_checkouts'), 
]