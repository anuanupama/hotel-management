from __future__ import unicode_literals
from django.db import models
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from main.models import BaseModel
from main.models import STATE
from versatileimagefield.fields import VersatileImageField


RATE_TYPE = (
    ('normal','Normal'),
    ('offtime','Offtime'),
    ('peektime','Peektime'),
)

DOC_CATEGORY = (
    ('license', 'Drivers License'),
    ('voters_card', 'Voters Card'),
    ('passport', 'Passport'),
    ('ration_card', 'Ration Card'),
)

class Booking(BaseModel):
    shop = models.ForeignKey("main.Shop")
    customer = models.ForeignKey("customers.Customer",limit_choices_to={'is_deleted': False},blank=True,null=True)
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    time = models.DateTimeField()
    total = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    advance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total_rooms = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total_customers = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    rate_type = models.CharField(max_length=128,choices=RATE_TYPE,default="normal")
    total_days = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    discount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)  
    is_checkin = models.BooleanField(default=False)   

    class Meta:
        db_table = 'bookings_booking'
        verbose_name = _('booking')
        verbose_name_plural = _('bookings')
        ordering = ('-auto_id',)         
    
    def __unicode__(self): 
        return "%s - %s - %s" %(str(self.a_id), self.customer.name,str(self.total))   


class BookingItem(models.Model):
    booking = models.ForeignKey("bookings.Booking",limit_choices_to={'is_deleted': False})
    room = models.ForeignKey("rooms.Room",blank=True,null=True) 
    rate = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'bookings_booking_item'
        verbose_name = _('booking_item')
        verbose_name_plural = _('booking_items')              
    
    def __unicode__(self): 
        return "%s" %(self.room.name)


class Checkin(BaseModel):
    shop = models.ForeignKey("main.Shop")
    customer = models.ForeignKey("customers.Customer",limit_choices_to={'is_deleted': False},blank=True,null=True)
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    time = models.DateTimeField()
    total = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    advance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total_rooms = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    total_customers = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    rate_type = models.CharField(max_length=128,choices=RATE_TYPE,default="normal")
    total_days = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    payment = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    collected_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    discount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    is_checkout = models.BooleanField(default=False) 

    is_deleted = models.BooleanField(default=False)    

    class Meta:
        db_table = 'bookings_checkin'
        verbose_name = _('checkin')
        verbose_name_plural = _('checkins')

    def get_checkin_items(self):
        return CheckinItem.objects.filter(checkin=self)         
    
    def __unicode__(self): 
        return "%s - %s - %s" %(str(self.a_id), self.customer.name,str(self.total))   


class CheckinItem(models.Model):
    checkin = models.ForeignKey("bookings.Checkin",limit_choices_to={'is_deleted': False})
    room = models.ForeignKey("rooms.Room",blank=True,null=True) 
    rate = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'bookings_checkin_item'
        verbose_name = _('checkin_item')
        verbose_name_plural = _('checkin_items')              
    
    def __unicode__(self): 
        return "%s" %(self.room.name)


class CheckinCustomer(models.Model):
    checkin = models.ForeignKey("bookings.Checkin",limit_choices_to={'is_deleted': False})
    name = models.CharField(max_length=128)
    age = models.CharField(max_length=128)
    address = models.TextField(null=True,blank=True)
    phone = models.CharField(max_length=128,null=True,blank=True)
    email = models.EmailField(null=True,blank=True)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'bookings_checkin_customer'
        verbose_name = _('checkin_customer')
        verbose_name_plural = _('checkin_customers')              
    
    def __unicode__(self): 
        return "%s" %(self.name)


class BookingCustomer(models.Model):
    name = models.CharField(max_length=128,null=True,blank=True)
    age = models.CharField(max_length=128,null=True,blank=True)
    address = models.TextField(null=True,blank=True)
    phone = models.CharField(max_length=128,null=True,blank=True)
    email = models.EmailField(null=True,blank=True)
    state = models.CharField(max_length=128,choices=STATE,default="Kerala")  
    
    document_category = models.CharField(max_length=128, choices=DOC_CATEGORY,default="voters_card")
    document_no = models.CharField(max_length=128,null=True,blank=True)
    photo = VersatileImageField('Photo',upload_to="customer/photo/",blank=True,null=True)
    document = VersatileImageField(blank=True,null=True)
    signature = VersatileImageField(blank=True,null=True)
     
    class Meta:
        db_table = 'bookings_booking_customer'
        verbose_name = _('booking customer')
        verbose_name_plural = _('booking customers') 
  
    def __unicode__(self): 
        return "%s - %s" %(self.name,self.address)


class Checkout(BaseModel):
    shop = models.ForeignKey("main.Shop")
    date = models.DateField()
    collect_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    customer = models.ForeignKey("customers.Customer",blank=True,null=True)
    remaining_balance = models.DecimalField(default=0,decimal_places=2, max_digits=15)
    checkin = models.ForeignKey("bookings.Checkin",blank=True,null=True) 
    other_expense = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    rent_balance = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])   
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'bookings_checkout'
        verbose_name = _('checkout')
        verbose_name_plural = _('checkouts')
        ordering = ('-auto_id',)
    
    
    def __unicode__(self): 
        return "%s" %(self.collect_amount)


