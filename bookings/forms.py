from django import forms
from django.forms.widgets import TextInput, Textarea, Select,HiddenInput
from bookings.models import Booking, BookingItem,Checkin,CheckinItem,CheckinCustomer,BookingCustomer,Checkout
from dal import autocomplete
from django.utils.translation import ugettext_lazy as _
from main.models import STATE
from customers.models import Customer


DOC_CATEGORY = (
    ('license', 'Drivers License'),
    ('voters_card', 'Voters Card'),
    ('passport', 'Passport'),
    ('ration_card', 'Ration Card'),
)


class BookingForm(forms.ModelForm):
    customer_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Customer Name","class" : 'form-control required'}),required=False)
    customer_address = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Customer Address","class" : ' form-control'}),required=False)
    customer_email = forms.EmailField(widget=forms.TextInput(attrs={"placeholder":"Customer Email","class" : ' form-control'}),required=False)
    customer_phone = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Customer Phone","class" : ' form-control'}),required=False)
    customer_state = forms.ChoiceField(widget=forms.Select(attrs={"class" : 'selectpicker form-control'}),required=False,choices=STATE)
    customer_age = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Customer Age","class" : ' form-control'}),required=False)
    
    class Meta:
        model = Booking
        fields = ['customer','time','from_time','to_time','advance','total_days','discount']
        widgets = {
            'customer' : autocomplete.ModelSelect2(url='customers:customer_autocomplete',attrs={'data-placeholder': 'Customer','data-minimum-input-length': 1},),
            'time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'Time'}),
            'from_time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'From Time'}),
            'to_time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'To Time'}),
            'total': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Total'}),
            'total_rooms': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Total Rooms'}),
            'total_customers': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Total members'}),
            'rate_type': Select(attrs={'class': 'required form-control selectpicker'}),
            'advance': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Advance'}),
            'balance': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Balance'}),
            'total_days': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Total Days'}),
            'discount': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Balance'}),
        }
        error_messages = {
            'customer' : {
                'required' : _("Customer field is required."),
            },
            'time' : {
                'required' : _("Time field is required."),
            },
            'to_time' : {
                'required' : _("From Time field is required."),
            },
            'from_time' : {
                'required' : _("To Time field is required."),
            },
            'total' : {
                'required' : _("Total field is required."),
            },
            'total_rooms' : {
                'required' : _("Total Rooms received field is required."),
            },
            'total_customers' : {
                'required' : _("Total members field is required."),
            }
        }
        help_texts = {
            'customer' : 'Click the x button to select an existing customer or create a new one.',
        }

    def clean(self):
        cleaned_data=super(BookingForm, self).clean()
        customer = self.cleaned_data.get('customer')
        customer_name = self.cleaned_data.get('customer_name')
        customer_address = self.cleaned_data.get('customer_address')
        
        if not customer and not customer_name and not customer_address:
            if not customer_name:
                string = "enter customer name"
            if not customer_address:
                string = "enter customer address"
            raise forms.ValidationError(
                "Select One of the customer or" +" " + string
            )
            
        return cleaned_data
        
class BookingItemForm(forms.ModelForm):
    
    class Meta:
        model = BookingItem
        fields = ['room']
        widgets = {
            'room' : autocomplete.ModelSelect2(url='rooms:room_autocomplete',attrs={'data-placeholder': 'Room','data-minimum-input-length': 1},),            
        }
        error_messages = {
            'room' : {
                'required' : _("Room field is required."),
            }
        }



class CheckinForm(forms.ModelForm):    
    class Meta:
        model = Checkin
        fields = ['customer','time','from_time','to_time','total_days','payment','discount']
        widgets = {
            'customer' : autocomplete.ModelSelect2(url='customers:customer_autocomplete',attrs={'data-placeholder': 'Customer','data-minimum-input-length': 1},),
            'time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'Time'}),
            'from_time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'From Time'}),
            'to_time': TextInput(attrs={'class': 'required form-control date-time-picker','placeholder' : 'To Time'}),
            'total': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Total'}),
            'total_rooms': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Total Rooms'}),
            'total_customers': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Total members'}),
            'rate_type': Select(attrs={'class': 'required form-control selectpicker'}),
            'advance': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Advance'}),
            'balance': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Balance'}),
            'total_days': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Total Days'}),
            'payment': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Payment'}),
            'discount': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Discount'}),
        }
        error_messages = {
            'customer' : {
                'required' : _("Customer field is required."),
            },
            'time' : {
                'required' : _("Time field is required."),
            },
            'to_time' : {
                'required' : _("From Time field is required."),
            },
            'from_time' : {
                'required' : _("To Time field is required."),
            },
            'total' : {
                'required' : _("Total field is required."),
            },
            'total_rooms' : {
                'required' : _("Total Rooms received field is required."),
            },
            'total_customers' : {
                'required' : _("Total members field is required."),
            },
            'payment' :{
                'required' : _("Payment field is required.")
            }
        }
        help_texts = {
            'customer' : 'Click the x button to select an existing customer or create a new one.',
        }
    

class CheckinItemForm (forms.ModelForm):
    
    class Meta:
        model = CheckinItem
        fields = ['room']
        widgets = {
            'room' : autocomplete.ModelSelect2(url='rooms:room_autocomplete',attrs={'data-placeholder': 'Room','data-minimum-input-length': 1},),            
        }
        error_messages = {
            'room' : {
                'required' : _("Room field is required."),
            }
        } 

        
class CheckinCustomerForm (forms.ModelForm):    
    class Meta:
        model = CheckinCustomer
        fields = ['name','phone','address','age','email']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}), 
            'email': TextInput(attrs={'class': 'form-control','placeholder' : 'Email'}),
            'phone': TextInput(attrs={'class': 'form-control','placeholder' : 'Phone'}),
            'address': TextInput(attrs={'class': 'required form-control','placeholder' : 'Address'}),
            'age': TextInput(attrs={'class': 'required form-control','placeholder' : 'Age'}), 
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'age' : {
                'required' : _("Age field is required."),
            },
        }        

class CustomerForm(forms.ModelForm):
    
    class Meta:
        model = BookingCustomer
        fields = ['name','email','phone','address','state','document_category','document_no','age','photo','document','signature']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control','placeholder' : 'Name'}), 
            'email': TextInput(attrs={'class': 'form-control','placeholder' : 'Email'}),
            'phone': TextInput(attrs={'class': 'form-control','placeholder' : 'Phone'}),
            'address': TextInput(attrs={'class': 'required form-control','placeholder' : 'Address'}),          
            'state': Select(attrs={'class': 'required selectpicker'}),
            'document_category' : Select(attrs={'class': 'required selectpicker'}),
            'document_no' : TextInput(attrs={'class': 'number required form-control','placeholder' : 'Document Number'}),
            'age' : TextInput(attrs={'class': 'number required form-control','placeholder' : 'Age'}),
        }       


class CheckoutForm(forms.ModelForm):
    
    class Meta:
        model = Checkout
        exclude = ['creator','updator','auto_id','is_deleted','a_id','shop','balance','checkin','remaining_balance','other_expense','rent_balance']
        widgets = {
            'customer' : autocomplete.ModelSelect2(url='customers:customer_autocomplete',attrs={'data-placeholder': 'Customer','data-minimum-input-length': 1},),
            'date' : TextInput(attrs={'class': 'required form-control date-picker','placeholder' : 'Date'}),
            'collect_amount' : TextInput(attrs={'class': 'required form-control','placeholder' : 'Collected Cash'}),
            'balance' : TextInput(attrs={'disabled' : 'disabled','class': 'required form-control','placeholder' : 'Balance'}),
            'remaining_balance' :TextInput(attrs={'class': 'required form-control','placeholder' : 'Remaining Balance'}),            
        }
        error_messages = {
            'date' : {
                'required' : _("Date field is required."),
            },
            'collect_amount' : {
                'required' : _("Collect Amount field is required."),
            },
            'balance' : {
                'required' : _("Balance is required."),
            },
            'customer' : {
                'required' : _("Customer field is required."),
            },
            'remaining_balance' : {
                'required' : _("Remaining Balance field is required."),
            },
        }
