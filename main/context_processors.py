from users.functions import shop_access, get_current_shop
from main.functions import get_account_balance,get_current_role
from users.models import Notification
from main.models import Mode, Shop, ShopAccess
from staffs.models import Staff
import datetime
from finance.forms import SalaryPaymentForm


def main_context(request):
    current_shop = get_current_shop(request)
    today = datetime.date.today()
    salary_payment_form = SalaryPaymentForm(initial={"date":today})
    is_superuser = False
    if "set_user_timezone" in request.session:
        user_session_ok = True
        user_time_zone = request.session['set_user_timezone']
    else:
        user_session_ok = False
        user_time_zone = "Asia/Kolkata"
       
    current_theme = 'cyan-600'
    block_auto_redirect = False
    remove_previous_balance_from_bill = False
    default_paid_value = True
    user_instance =[]
    day_choice = ""
    if current_shop:
        current_theme = current_shop.theme
        block_auto_redirect = current_shop.block_auto_redirect
        remove_previous_balance_from_bill = current_shop.remove_previous_balance_from_bill
        default_paid_value = current_shop.default_paid_value
        day_choice = current_shop.day_choice

    if request.user.is_authenticated():
        if ShopAccess.objects.filter(user=request.user).exists():
            if request.user.is_superuser:
                is_superuser=True
        current_role = get_current_role(request)       
        if current_role == "staff":
            user_instance = Staff.objects.get(user=request.user,shop=current_shop)
        recent_notifications = Notification.objects.filter(user=request.user,is_deleted=False)        
    else:
        recent_notifications = []

    active_parent = request.GET.get('active_parent')
    active = request.GET.get('active')   
        
    return {
        'app_title' : "Arkboss",
        "user_session_ok" : user_session_ok,
        "user_time_zone" : user_time_zone,
        "confirm_delete_message" : "Are you sure want to delete this item. All associated data may be removed.",
        "revoke_access_message" : "Are you sure to revoke this user's login access",
        "confirm_shop_delete_message" : "Your shop will deleted permanantly. All data will lost.",
        "confirm_delete_selected_message" : "Are you sure to delete all selected items.",
        "confirm_read_message" : "Are you sure want to mark as read this item.",
        "confirm_read_selected_message" : "Are you sure to mark as read all selected items.",
        "confirm_cleaning_finished_message" : "Are you sure want to mark as clean this room(s)",
        'domain' : request.META['HTTP_HOST'],
        "shop_access" : shop_access(request),
        'current_shop' : current_shop,
        "account_balance" : get_account_balance(request,request.user),
        "current_theme" : current_theme,
        "user_instance" : user_instance,
        "is_superuser" : is_superuser,
        "active_parent" : active_parent,
        "active_menu" : active,
        "recent_notifications" : recent_notifications,
        "block_auto_redirect" : block_auto_redirect,

        "salary_payment_form":salary_payment_form,
        "remove_previous_balance_from_bill" : remove_previous_balance_from_bill,
        "default_paid_value" : default_paid_value,
        "day_choice" : day_choice
    }
    
    





