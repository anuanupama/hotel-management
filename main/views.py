from django.shortcuts import render
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, shop_required, ajax_required,\
    check_account_balance
from customers.models import Customer
from sales.models import Sale, SaleReturn, SaleItem
from purchases.models import Purchase
from products.models import Product
from main.forms import ShopForm
from main.functions import get_auto_id, generate_form_errors, promo_code_amount,\
    invite_code_amount, get_a_id
import json
from main.models import Shop, ShopAccess, App
from django.views.decorators.http import require_GET
from users.functions import get_current_shop, shop_access
from users.models import NotificationSubject, Notification
from django.db.models import Sum
from django.contrib.auth.models import Group
from products.models import Measurement
from finance.models import CashAccount, TransactionCategory, Transaction
import datetime 
from calendar import monthrange
from rooms.models import Room
from bookings.models import Checkin,Checkout


@check_mode
@login_required
@shop_required
def app(request):
    return HttpResponseRedirect(reverse('dashboard'))


@check_mode
@login_required
@shop_required
def dashboard(request):
    today = datetime.date.today()
    current_shop = get_current_shop(request)
    recent_customers = Customer.objects.filter(shop=current_shop,is_deleted=False,is_system_generated=False,).order_by('-date_added')[:5]
    recent_checkins = Checkin.objects.filter(shop=current_shop,is_deleted=False).order_by('-date_added')[:5]
    recent_checkout = Checkout.objects.filter(shop=current_shop,is_deleted=False).order_by('-date_added')[:5]
    recent_sales = Sale.objects.filter(shop=current_shop,is_deleted=False).order_by('-date_added')[:5]
    recent_purchase = Purchase.objects.filter(shop=current_shop,is_deleted=False).order_by('-date_added')[:5]
    recent_products = Product.objects.filter(shop=current_shop,is_deleted=False).order_by('-date_added')[:5]
    rooms = []
    if Room.objects.filter(shop=current_shop,is_deleted=False).exists():
        rooms = Room.objects.filter(shop=current_shop,is_deleted=False)
                          
    context = {
        "title" : "Dashboard",
        "today" : today,       
        "recent_customers" : recent_customers,
        "recent_sales" : recent_sales,
        "recent_purchase" :recent_purchase,
        "recent_products" : recent_products,
        "recent_checkins" : recent_checkins,
        "recent_checkout" : recent_checkout,
        "rooms" : rooms,

        "is_dashboard" : True,
        
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
        "is_dashboard" :True

    }
    return render(request,"base.html",context)


@check_mode
@login_required
@shop_required
@ajax_required
@require_GET
def reports(request):
    current_shop = get_current_shop(request)
    expenses = Transaction.objects.filter(shop=current_shop,transaction_type="expense",is_deleted=False)
    incomes = Transaction.objects.filter(shop=current_shop,transaction_type="income",is_deleted=False)
    sales = Sale.objects.filter(shop=current_shop,is_deleted=False)
    sale_returns = SaleReturn.objects.filter(shop=current_shop,is_deleted=False)

    expense_category = TransactionCategory.objects.filter(is_deleted=False).values_list('name',flat=True)
    income_category = TransactionCategory.objects.filter(is_deleted=False).values_list('name',flat=True)

    month = request.GET.get('month') 
    year = request.GET.get('year') 
    today = datetime.datetime.today()
    date = request.GET.get('date')
    date_error = "no"
    period = request.GET.get('period')
    filter_period = None
    
    total_expense = 0
    total_income = 0

    this_period_expenses = []
    this_period_incomes = []
    date_list = []
    income_amounts = []
    expense_amounts = []

    #income and expense relation calculation for chart
    this_month = today.month
    this_year = today.year
    this_month = int(this_month)
    this_year = int(this_year)
    if this_month and this_year:
        title = "Report : " +  str(this_month) + " " + str(this_year)
        this_period_expenses = expenses.filter(date__year=this_year,date__month=this_month)
        this_period_incomes = incomes.filter(date__year=this_year,date__month=this_month)
        no_of_days = monthrange(this_year, this_month)[1]
        for i in range(1, no_of_days+1):
            date_obj = datetime.date(this_year, this_month, i)
            date_list.append(date_obj)

    if this_period_incomes:
        total_income = this_period_incomes.aggregate(amount=Sum('amount')).get('amount',0)
        for date_list_obj in date_list:
            income_amount = 0
            if this_period_incomes.filter(date=date_list_obj).exists():
                income_amount = this_period_incomes.filter(date=date_list_obj).aggregate(amount=Sum('amount')).get('amount',0)
            income_amounts.append(str(income_amount))

    if this_period_expenses:
        total_expense = this_period_expenses.aggregate(amount=Sum('amount')).get('amount',0)
        for date_list_obj in date_list:
            expense_amount = 0
            if this_period_expenses.filter(date=date_list_obj).exists():
                expense_amount = this_period_expenses.filter(date=date_list_obj).aggregate(amount=Sum('amount')).get('amount',0)
            expense_amounts.append(str(expense_amount))

    counter = 0
    for date_obj in date_list:
        date_list[counter] = str(date_obj.day)
        counter += 1

    total_amount = total_income + total_expense
    income_percentage = 0
    expense_percentage = 0
    
    if total_amount > 0:
        income_percentage = total_income/total_amount * 100
        expense_percentage = total_expense/total_amount * 100


    #total income and expense calculation
    if period:
        if period == "today" or period == "month" or period == "year":
            filter_period = period
    
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')

    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True
    
    total_sales_created = 0
    total_sales_payment_amount_this_period = 0
    total_sales_amount_this_period = 0
    total_sales_amount = 0
    total_sales_payment_amount = 0
    total_special_discount = 0
    total_profit_amount = 0
    discount = 0
    current_balance = 0
    total_income_amount = 0
    total_profit = 0
    total_discount = 0
    total_sale_return_amount = 0
    this_period_total_expense_amount = 0
    total_other_income_amount = 0
    total_income_amount = 0
    total_expense_amount = 0
    total_balance = 0
    payment_received = 0
    total_return_count = 0

    this_period_sales = []
    this_period_sale_returns = []
    this_period_total_income = []
    this_period_total_expense = []
    this_period_total_other_income = []

    if filter_period:
        if period == "today":
            title = "Today"
            this_period_sales = sales.filter(time__year=today.year, time__month=today.month, time__day=today.day)
            this_period_sale_returns = sale_returns.filter(time__year=today.year, time__month=today.month, time__day=today.day)

            this_period_total_expense = expenses.filter(transaction_category__name__in=expense_category,date__year=today.year, date__month=today.month, date__day=today.day).exclude(transaction_category__name='salereturn_payment')
            this_period_total_income = incomes.filter(transaction_category__name__in=income_category,date__year=today.year, date__month=today.month, date__day=today.day)
            this_period_total_other_income = incomes.filter(transaction_category__name__in=income_category,date__year=today.year, date__month=today.month, date__day=today.day).exclude(transaction_category__name='sale_payment')
                
        elif period == "month":
            title = "This Month"

            title = "month"
            this_period_sales = sales.filter(time__year=today.year,time__month=today.month)
            this_period_sale_returns = sale_returns.filter(time__year=today.year,time__month=today.month)

            this_period_total_expense = expenses.filter(transaction_category__name__in=expense_category,date__year=today.year, date__month=today.month).exclude(transaction_category__name='salereturn_payment')
            this_period_total_income = incomes.filter(transaction_category__name__in=income_category,date__year=today.year, date__month=today.month)
            this_period_total_other_income = incomes.filter(transaction_category__name__in=income_category,date__year=today.year, date__month=today.month).exclude(transaction_category__name='sale_payment')
            print total_sales_created
            
        elif period == "year":
            title = "This Year"
            title = "year"

            this_period_sales = sales.filter(time__year=today.year)
            this_period_sale_returns = sale_returns.filter(time__year=today.year)

            this_period_total_expense = expenses.filter(transaction_category__name__in=expense_category,date__year=today.year).exclude(transaction_category__name='salereturn_payment')
            this_period_total_income = incomes.filter(transaction_category__name__in=income_category,date__year=today.year)
            this_period_total_other_income = incomes.filter(transaction_category__name__in=income_category,date__year=today.year).exclude(transaction_category__name='sale_payment')
                   
    elif filter_date_period:
        if date_error == "yes":
            response_data['message'] = date_error
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')           

        else:         
            this_period_sales = sales.filter(time__range=[from_date, to_date])
            this_period_sale_returns = sale_returns.filter(time__range=[from_date, to_date])

            this_period_total_expense = expenses.filter(transaction_category__name__in=expense_category,date__range=[from_date, to_date]).exclude(transaction_category__name='salereturn_payment')
            this_period_total_income = incomes.filter(transaction_category__name__in=income_category,date__range=[from_date, to_date])
            this_period_total_other_income = incomes.filter(transaction_category__name__in=income_category,date__range=[from_date, to_date]).exclude(transaction_category__name='sale_payment')
            
            to_date = to_date -  datetime.timedelta(days=1)
            title = from_date.strftime('%b %d %Y') + " to " + to_date.strftime('%b %d %Y')
            response_data['from_date'] = from_date
            response_data['to_date'] = to_date

    elif date:
        
        if date:
            try:
                date = datetime.datetime.strptime(date, '%m/%d/%Y').date()          
            except ValueError:
                date_error = "yes"  
        title = str(date)

        this_period_sales = sales.filter(time__date=date)
        this_period_sale_returns = sale_returns.filter(time__date=date)

        this_period_total_expense = expenses.filter(transaction_category__name__in=expense_category,date=date).exclude(transaction_category__name='salereturn_payment')
        this_period_total_income = incomes.filter(transaction_category__name__in=income_category,date=date)
        this_period_total_other_income = incomes.filter(transaction_category__name__in=income_category,date=date).exclude(transaction_category__name='sale_payment')
        
    elif month and year:
        title = str(month)

        this_period_sales = sales.filter(time__year=year,time__month=month)
        this_period_sale_returns = sale_returns.filter(time__year=year,time__month=month)

        this_period_total_expense = expenses.filter(transaction_category__name__in=expense_category,date__year=year,date__month=month).exclude(transaction_category__name='salereturn_payment')
        this_period_total_income = incomes.filter(transaction_category__name__in=income_category,date__year=year,date__month=month)
        this_period_total_other_income = incomes.filter(transaction_category__name__in=income_category,date__year=year,date__month=month).exclude(transaction_category__name='sale_payment')

    elif month:
        title = str(month)

        this_period_sales = sales.filter(time__month=month)
        this_period_sale_returns = sale_returns.filter(time__month=month)

        this_period_total_expense = expenses.filter(transaction_category__name__in=expense_category,date__month=month).exclude(transaction_category__name='salereturn_payment')
        this_period_total_income = incomes.filter(transaction_category__name__in=income_category,date__month=month)
        this_period_total_other_income = incomes.filter(transaction_category__name__in=income_category,date__month=month).exclude(transaction_category__name='sale_payment')
        
    elif year:
        title = str(year)
        this_period_sales = sales.filter(time__year=year)
        this_period_sale_returns = sale_returns.filter(time__year=year)

        this_period_total_expense = expenses.filter(transaction_category__name__in=expense_category,date__year=year).exclude(transaction_category__name='salereturn_payment')
        this_period_total_income = incomes.filter(transaction_category__name__in=income_category,date__year=year)
        this_period_total_other_income = incomes.filter(transaction_category__name__in=income_category,date__year=year).exclude(transaction_category__name='sale_payment')

    #  this two variables are general for above parts.
    if this_period_sales:
        sale_items = SaleItem.objects.filter(sale__in=this_period_sales)
        for sale_item in sale_items:
            price = sale_item.price
            cost = sale_item.cost
            qty = sale_item.qty
            tax_amount = sale_item.tax_amount
            discount = sale_item.discount_amount
            profit = ((price - cost) * qty) - discount
            total_profit += profit

        sales_dict = this_period_sales.aggregate(Sum('total'),Sum('special_discount'),Sum('payment_received'),Sum('balance'),Sum('total_discount_amount'),Sum('total_tax_amount'))
        total_sales_amount  = sales_dict['total__sum']

        total_sales_payment_amount = sales_dict['payment_received__sum']
        total_special_discount = sales_dict['special_discount__sum']
        total_discount = sales_dict['total_discount_amount__sum']
        total_balance = sales_dict['balance__sum']

        total_profits = total_profit - total_special_discount
        total_profit_amount += total_profits
        discount = total_special_discount
        
        total_sales_created = this_period_sales.count()

    if this_period_sale_returns:
        total_sale_return_amount = this_period_sale_returns.aggregate(amount=Sum('amount_returned')).get('amount',0)
        total_return_count = this_period_sale_returns.count()

    if this_period_total_expense:
        this_period_total_expense_amount = this_period_total_expense.aggregate(amount=Sum('amount')).get('amount',0)

    if this_period_total_other_income:
        total_other_income_amount = this_period_total_other_income.aggregate(amount=Sum('amount')).get('amount',0)

    if this_period_total_income:
        total_income_amount = this_period_total_income.aggregate(amount=Sum('amount')).get('amount',0)

    payment_received = total_sales_payment_amount 
    total_income_amount = payment_received + total_other_income_amount 
    total_expense_amount = total_sale_return_amount + this_period_total_expense_amount 
    
    current_balance = total_income_amount - total_expense_amount

    result = {
        "status" : "true",
        "title" :title,
        "expense_amounts" : expense_amounts,
        "income_amounts" : income_amounts,
        "date_list" : date_list,

        "total_expense" : str(total_expense),
        "total_income" : str(total_income),
        "expense_percentage" : str(expense_percentage),
        "income_percentage" : str(income_percentage),

        "total_income_amount" : str(total_income_amount),
        "total_expense_amount" : str(total_expense_amount),
        "total_sales_created" : str(total_sales_created),
        "total_sales_amount" : str(payment_received),
        "total_other_income" : str(total_other_income_amount),
        "total_sale_return_amount"  : str(total_sale_return_amount),
        "total_return_count" : str(total_return_count),
        "this_period_total_expense_amount" : str(this_period_total_expense_amount),
        "total_profit_amount" : str(round(total_profit_amount,2)),
        "current_balance" : str(current_balance)

    }

    return HttpResponse(json.dumps(result),content_type='text/plain')  
    

def create_shop_request(request): 
    context = {
        "title" : "Create Shop",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'users/create_shop_request.html',context)


def download_app(request): 
    app_list = App.objects.all()
    app = None
    file = None
    if app_list:
        app = app_list.latest('date_added')
        file = app.application.url
        if request.is_ajax():
            protocol = "http://"
            if request.is_secure():
                protocol = "https://"

            web_host = request.get_host()
            file_url = protocol + web_host + file

            response_data = {
                "status" : "true",
                "file_url" : file_url
            }
            
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')
        else:
            return HttpResponseRedirect(file)
    else:
        return HttpResponse("File not uploaded.")


@check_mode
@login_required
def create_shop(request):    
    
    if request.method == 'POST':
        form = ShopForm(request.POST,request.FILES)
        
        if form.is_valid(): 
              
            auto_id = get_auto_id(Shop)
            
            #create shop
            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = auto_id
            data.save() 

            Customer(
                auto_id = get_auto_id(Customer),
                a_id = get_a_id(Customer,request),
                shop = data,
                name = "default",
                address = "default",
                creator = request.user,
                updator= request.user,
                is_system_generated= True
            ).save()
            
            current_shop = data

            #create shop access

            group = Group.objects.get(name="administrator")
            is_default= True
            if ShopAccess.objects.filter(user=request.user).exists():
                is_default = False
            ShopAccess(user=request.user,shop=current_shop,group=group,is_accepted=True,is_default=is_default).save()
          
            request.session["current_shop"] = str(data.pk)

            #create initial product units 
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(
                code='CM',
                unit_type='distance',
                unit_name='centimeter',
                is_base=False,
                conversion_factor=0.01,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(
                code='MT',
                unit_type='distance',
                unit_name='meter',
                is_base=True,
                conversion_factor=0,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='MM',
                unit_type='distance',
                unit_name='millimeter',
                is_base=False,
                conversion_factor=0.001,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='YD',
                unit_type='distance',
                unit_name='yard',
                is_base=False,
                conversion_factor=0.9144,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='IN',
                unit_type='distance',
                unit_name='inch',
                is_base=False,
                conversion_factor=0.0254,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='FT',
                unit_type='distance',
                unit_name='foot',
                is_base=False,
                conversion_factor=0.3048,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='SQ',
                unit_type='area',
                is_base=True,
                unit_name='square_feet',
                conversion_factor=0.0,
                shop=current_shop,
                creator=request.user,
                updator=request.user,
                auto_id=auto_id,
                a_id=a_id,
                is_system_generated=True
            ).save()

            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='EA',
                unit_type='quantity',
                unit_name='each',
                is_base=True,
                conversion_factor=0.0,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='DZ',
                unit_type='quantity',
                unit_name='dozen',
                is_base=False,
                conversion_factor=12.0,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='HR',
                unit_type='time',
                unit_name='hour',
                is_base=False,
                conversion_factor=60.0,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='DY',
                unit_type='time',
                unit_name='day',
                is_base=False,
                conversion_factor=1440.0,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='MI',
                unit_type='time',
                unit_name='minute',
                is_base=True,
                conversion_factor=0.0,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='LI',
                unit_type='volume',
                unit_name='liter',
                is_base=True,
                conversion_factor=0.0,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='ML',
                unit_type='volume',
                unit_name='milliliter',
                is_base=False,
                conversion_factor=0.001,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='GR',
                unit_type='weight',
                unit_name='gram',
                is_base=False,
                conversion_factor=.001,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()

            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='LB',
                unit_type='weight',
                unit_name='pound',
                is_base=False,
                conversion_factor=0.453,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()

            auto_id = get_auto_id(Measurement)
            a_id = get_a_id(Measurement,request)
            Measurement(code='KG',
                unit_type='weight',
                unit_name='kilogram',
                is_base=True,
                conversion_factor=1000.0,
                is_system_generated=True,
                shop=current_shop,
                auto_id=auto_id,
                a_id=a_id,
                creator=request.user,
                updator=request.user
            ).save()

            auto_id=get_auto_id(CashAccount)
            a_id = get_a_id(CashAccount,request)
            
            CashAccount.objects.create(
                auto_id=auto_id,
                a_id=a_id,
                shop=current_shop,
                name='General',
                user=request.user,
                creator=request.user,
                updator=request.user,
                first_time_balance=0,
                is_system_generated=True,
            )

            #create initial income categories 

            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="customer_payment",
                shop=current_shop,
                category_type="income",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="sale_payment",
                shop=current_shop,
                category_type="income",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="service_payment",
                shop=current_shop,
                category_type="income",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="service_payment",
                shop=current_shop,
                category_type="expense",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="vendor_payment",
                shop=current_shop,
                category_type="income",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="staff_payment",
                shop=current_shop,
                category_type="income",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="credit_user_payment",
                shop=current_shop,
                category_type="income",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="credit",
                category_type="income",
                shop=current_shop,
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="debit",
                category_type="income",
                shop=current_shop,
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="purchase_payment",
                category_type="income",
                shop=current_shop,
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            #create initital expense categories
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="customer_payment",
                shop=current_shop,
                category_type="expense",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="vendor_payment",
                shop=current_shop,
                category_type="expense",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="purchase_payment",
                shop=current_shop,
                category_type="expense",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="staff_payment",
                shop=current_shop,
                category_type="expense",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="credit_user_payment",
                shop=current_shop,
                category_type="expense",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="debit",
                category_type="expense",
                shop=current_shop,
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="credit",
                category_type="expense",
                shop=current_shop,
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="salereturn_payment",
                shop=current_shop,
                category_type="expense",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()

            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="booking",
                shop=current_shop,
                category_type="income",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()

            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="checkin",
                shop=current_shop,
                category_type="income",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()

            auto_id=get_auto_id(TransactionCategory)
            a_id = get_a_id(TransactionCategory,request)
            TransactionCategory(
                auto_id = auto_id,
                a_id= a_id,
                name="checkout",
                shop=current_shop,
                category_type="income",
                is_system_generated=True,
                creator=request.user,
                updator=request.user
            ).save()
            
            return HttpResponseRedirect(reverse('app'))
        
        else:            
            context = {
                "title" : "Create Shop",
                "form" : form,
                "url" : reverse('create_shop'),

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
            }
            return render(request,'users/create_shop.html',context) 
    else:
        form = ShopForm()
        context = {
            "title" : "Create Shop ",
            "form" : form,
            "url" : reverse('create_shop'),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'users/create_shop.html',context)
    
    
@login_required    
@ajax_required
@require_GET
def switch_shop(request): 
    pk = request.GET.get('pk')
    if Shop.objects.filter(creator=request.user,pk=pk,is_deleted=False).exists():
        request.session["current_shop"] = pk
        response_data = {}
        response_data['status'] = 'true'
        response_data['title'] = "Success"  
        response_data['message'] = 'Shop switched successfully.'        
    else:
        response_data = {}
        response_data['status'] = 'false'
        response_data['title'] = "No Access"  
        response_data['stable'] = "true"  
        response_data['message'] = 'You have no access to this shop.'
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript') 


@login_required    
@ajax_required
@require_GET
def switch_theme(request): 
    current_shop = get_current_shop(request)
    theme = request.GET.get('pk')
    if theme:
        current_shop.theme = theme
        current_shop.save()
        response_data = {}
        response_data['status'] = 'true'
        response_data['title'] = "Success"  
        response_data['message'] = 'Theme switched successfully.'        
    else:
        response_data = {}
        response_data['status'] = 'false'
        response_data['title'] = "Something Wrong!!!"  
        response_data['stable'] = "true"  
        response_data['message'] = 'Sorry Try again later.'
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    
@check_mode
@login_required
def delete_shop(request,pk):
    current_shop = get_current_shop(request)
    
    Shop.objects.filter(pk=current_shop.pk).update(is_deleted=True)
    request.session["current_shop"] = ''
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Shop Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('app')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

from reportlab.lib.units import mm, inch
from reportlab.graphics.barcode import createBarcodeDrawing
from reportlab.graphics.shapes import Drawing, String
from reportlab.graphics.charts.barcharts import HorizontalBarChart


class MyBarcodeDrawing(Drawing):
    def __init__(self, text_value, *args, **kw):
        barcode = createBarcodeDrawing('Code128', value=text_value,  barHeight=.7*inch, barWidth = 1.6, humanReadable=True)
        Drawing.__init__(self,barcode.width,barcode.height,*args,**kw)       
        self.add(barcode, name='barcode')
        
    
@login_required 
def create_barcode(request,code):
    #instantiate a drawing object
    d = MyBarcodeDrawing(code)
    binaryStuff = d.asString('gif')
    return HttpResponse(binaryStuff, 'image/gif')