from django.template import Library
from django.contrib.auth.models import User
register = Library()
from django.template.defaultfilters import stringfilter
from rooms.models import Room,RoomStatus
import datetime


@register.filter    
def check_default(value): 
	result = value
	if value == "default":
		result = "-"
	return result
                                                        
    
@register.filter
@stringfilter
def underscore_smallletter(value):
    value =  value.replace(" ", "_")
    return value
    

@register.filter
def to_fixed_two(value):
    return "{:10.2f}".format(value)


@register.filter
def tax_devide(value):
    return value/2

@register.filter
def is_booked(value):
	room = Room.objects.get(pk=value)
	time = datetime.date.today()
	if RoomStatus.objects.filter(room=room,from_time__date__gte=time,is_booked=True,is_deleted=False).exists():
		return True
	else :
		return False

@register.filter
def is_occupied(value):
	room = Room.objects.get(pk=value)
	time = datetime.date.today()
	if RoomStatus.objects.filter(room=room,from_time__date__gte=time,is_occupied=True,is_deleted=False).exists():
		return True
	else :
		return False

@register.filter
def sub(value,arg):
    return round (value - arg)

@register.filter
def mul(value,arg):
    return round (value * arg)

@register.filter
@stringfilter
def get_display_value(value):
    value =  value.split("_")
    string = ' '.join(value) 
    return string.capitalize()