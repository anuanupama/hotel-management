import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = '8j5q-l89+8&1!0siike)5ukd7o6fu*$*u3ezhwu)ny3#pz(gbp'

DEBUG = True
ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'registration', 
    'el_pagination',
    'mailqueue',
    'dal',
    'dal_select2',
    'django_inlinecss',
    'versatileimagefield',
    
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    
    'users',
    'main',
    'web',
    'customers',
    'products',
    'sales',
    'staffs',
    'vendors',
    'purchases',
    'finance',
    'tasks',
    'rooms',
    'bookings'
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'hotel_management.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                "main.context_processors.main_context",
            ],
        },
    },
]

WSGI_APPLICATION = 'hotel_management.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', 
        'NAME': 'hotel_management',  
        'USER': 'tegain',
        'PASSWORD': 'tegain',
        'HOST': 'localhost',             
        'PORT': '',             
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = (
    'users.backend.EmailOrUsernameModelBackend',
    'django.contrib.auth.backends.ModelBackend'
)

LOGIN_URL = '/app/accounts/login/'
LOGOUT_URL = '/app/accounts/logout/'
LOGIN_REDIRECT_URL = '/'

# CELERY SETTINGS
BROKER_URL = 'redis://localhost:6379/0'

VERSATILEIMAGEFIELD_SETTINGS = {
    'cache_length': 2592000,
    'cache_name': 'versatileimagefield_cache',
    'jpeg_resize_quality': 70,
    'sized_directory_name': '__sized__',
    'filtered_directory_name': '__filtered__',
    'placeholder_directory_name': '__placeholder__',
    'create_images_on_demand': True,
    'image_key_post_processor': None,
    'progressive_jpeg': False
}

MAILQUEUE_LIMIT = 100
MAILQUEUE_QUEUE_UP = True

ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Asia/Kolkata'
USE_I18N = True
USE_L10N = True
USE_TZ = True

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'no_reply_tegain'
EMAIL_HOST_PASSWORD = '**noreplytegain00'
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = 'no-reply@tegain.com'
DEFAULT_BCC_EMAIL = 'allemails@tegain.com'

DEFAULT_REPLY_TO_EMAIL = 'safeer@tegain.com'
SERVER_EMAIL = 'no-reply@tegain.com'
ADMIN_EMAIL = 'safeer@tegain.com'

ENDLESS_PAGINATION_PER_PAGE = 20

MEDIA_URL = '/media/'
MEDIA_ROOT = '/srv/django/hotel_management/src/hotel_management/media/'
STATIC_URL = '/static/'
STATIC_FILE_ROOT = '/srv/django/hotel_management/src/hotel_management/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    '/srv/django/hotel_management/src/hotel_management/static/',
)
